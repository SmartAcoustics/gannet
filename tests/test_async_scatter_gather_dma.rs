#[cfg(all(test, target_arch = "arm", feature = "async"))]
mod async_scatter_gather_dma_tests {

    use rand::{distributions::Standard, rngs::SmallRng, Rng, SeedableRng};
    use std::{cmp::min, ops::Range, path::PathBuf, thread::sleep, time};

    use serial_test::serial;

    use gannet::{
        BusDataWidth,
        operation::{RefreshStaleOperation, ScatterGatherOperation},
        uni_channel::scatter_gather,
        Operation,
    };

    const BUS_DATA_WIDTH: BusDataWidth = BusDataWidth::FourBytes;
    //const BUS_WIDTH: usize = BUS_DATA_WIDTH as usize;

    use tokio;

    // This function generates random values for an MM2S transfer and a
    // corresponding S2MM transfer. It also populates the MM2S memory with data
    // which will work with the MM2S transfer. It returns all of the arguments
    // required to create an MM2S and an S2MM operation along with the expected
    // contents of the S2MM memory after the two transfers have been run.
    fn setup_random_sg_transfers(
        mm2s_device: &mut scatter_gather::Device,
        s2mm_device: &mut scatter_gather::Device,
    ) -> (usize, usize, usize, usize, usize, usize, Vec<u8>) {
        let mut rng = SmallRng::from_entropy();

        let mm2s_dma_data_size = mm2s_device.memory_pool_size();
        let s2mm_dma_data_size = s2mm_device.memory_pool_size();

        // Get the smaller of the two memory sizes
        let dma_data_size = min(mm2s_dma_data_size, s2mm_dma_data_size);

        // Need to check how many descriptors are available on the devices
        let n_mm2s_available_descriptors = mm2s_device.n_available_descriptors().unwrap();
        let n_s2mm_available_descriptors = s2mm_device.n_available_descriptors().unwrap();

        // Get the smaller of the two n_available_descriptors
        let n_available_descriptors =
            min(n_mm2s_available_descriptors, n_s2mm_available_descriptors);

        // Generate a random value for the block_size
        let block_size = 8 * rng.gen_range(1..(1 << 16) / 8);

        // Determine the maximum number of blocks that can fit within the
        // memory
        let n_available_mem_blocks = dma_data_size / block_size;

        // Get the smaller of n_available_mem_blocks and
        // n_available_descriptors as the smaller of these two values
        // determines the maximum number of blocks we can have.
        let physical_constraint_on_n_blocks = min(n_available_mem_blocks, n_available_descriptors);

        // Set an upper limit of 40 on the max_n_blocks
        let max_n_blocks = min(40, physical_constraint_on_n_blocks);

        // Randomly set n_blocks
        let n_blocks = rng.gen_range(1..max_n_blocks + 1);

        // Generate a random value for the block strides
        let mm2s_block_stride =
            8 * rng.gen_range(block_size / 8..(mm2s_dma_data_size / n_blocks) / 8 + 1);
        let s2mm_block_stride =
            8 * rng.gen_range(block_size / 8..(s2mm_dma_data_size / n_blocks) / 8 + 1);

        // Generate a random source and destination offset
        let mm2s_offset =
            8 * rng.gen_range(0..(mm2s_dma_data_size - mm2s_block_stride * n_blocks) / 8);
        let s2mm_offset =
            8 * rng.gen_range(0..(s2mm_dma_data_size - s2mm_block_stride * n_blocks) / 8);

        let expected_s2mm_mem = gen_and_write_random_data(
            mm2s_device,
            s2mm_device,
            &block_size,
            &mm2s_block_stride,
            &n_blocks,
            &mm2s_offset,
            &block_size,
            &s2mm_block_stride,
            &n_blocks,
            &s2mm_offset,
        );

        (
            n_blocks,
            block_size,
            mm2s_block_stride,
            mm2s_offset,
            s2mm_block_stride,
            s2mm_offset,
            expected_s2mm_mem,
        )
    }

    // Given operation arguments this function will set up the MM2S memory with
    // random data that satisfies those arguments and will return the expected
    // S2MM memory.
    //
    // Note: This function does not check that the arguments will be valid. That
    // must be performed in the calling function
    fn gen_and_write_random_data(
        mm2s_device: &mut scatter_gather::Device,
        s2mm_device: &mut scatter_gather::Device,
        mm2s_block_size: &usize,
        mm2s_block_stride: &usize,
        mm2s_n_blocks: &usize,
        mm2s_offset: &usize,
        s2mm_block_size: &usize,
        s2mm_block_stride: &usize,
        s2mm_n_blocks: &usize,
        s2mm_offset: &usize,
    ) -> Vec<u8> {
        let mut rng = SmallRng::from_entropy();

        let s2mm_dma_data_size = s2mm_device.memory_pool_size();

        {
            // Create a vector to overwrite the destination before beginning the
            // transfer
            let destination_setup_data = vec![0u8; s2mm_dma_data_size];

            let mut s2mm_dma_data = s2mm_device.get_memory(0, s2mm_dma_data_size).unwrap();
            s2mm_dma_data.copy_from_slice(&destination_setup_data);
        }

        // Create a vector to take the raw data
        let mut raw_data = Vec::new();

        for block_num in 0..*mm2s_n_blocks {
            // Generate random test data
            let test_data: Vec<u8> = (&mut rng)
                .sample_iter(&Standard)
                .take(*mm2s_block_size)
                .collect();
            raw_data.extend_from_slice(&test_data);

            // Get the offset of the block
            let block_offset = mm2s_offset + block_num * mm2s_block_stride;

            {
                let mut mm2s_dma_data = mm2s_device
                    .get_memory(block_offset, *mm2s_block_size)
                    .unwrap();

                mm2s_dma_data.copy_from_slice(&test_data);
            }
            //            // Need the memory fences here to prevent the cpu from reordering
            //            // the non volatile reads/writes around volatile reads/writes
            //            fence(Ordering::SeqCst);
            //
            //            unsafe {
            //                // Copy the test data into the source memory
            //                copy_nonoverlapping(
            //                    test_data.as_ptr(),
            //                    mm2s_dma_data.offset(block_offset as isize),
            //                    *mm2s_block_size);
            //            }
            //
            //            fence(Ordering::SeqCst);
        }

        // Create a vector to take the expected destination data
        let mut expected_s2mm_mem = vec![0u8; s2mm_dma_data_size];

        for n in 0..*s2mm_n_blocks {
            // Work out the block offset in destination memory
            let block_offset = s2mm_offset + n * s2mm_block_stride;
            // Set up the expected destination data
            expected_s2mm_mem[block_offset..(block_offset + s2mm_block_size)]
                .copy_from_slice(&raw_data[n * s2mm_block_size..(n + 1) * s2mm_block_size]);
        }

        expected_s2mm_mem
    }

    async fn trigger_wait_and_check_sg(
        mut mm2s_device: scatter_gather::Device,
        s2mm_device: scatter_gather::Device,
        mm2s_operation: Operation,
        s2mm_operation: Operation,
        expected_s2mm_mem: &Vec<u8>,
        s2mm_first: bool,
    ) -> (
        scatter_gather::Device,
        scatter_gather::Device,
        Operation,
        Operation,
    ) {
        let s2mm_dma_data_size = s2mm_device.memory_pool_size();

        // Trigger the dma transfers
        let (mut s2mm_device, mm2s_device, s2mm_operation, mm2s_operation, s2mm_mem) = {
            let mm2s_operation_with_mem = mm2s_operation.claim_memory(&mut mm2s_device).unwrap();

            let (s2mm_running_operation, mm2s_running_operation) = if s2mm_first {
                let s2mm_running_operation = s2mm_device.do_dma(s2mm_operation).unwrap();
                // The sleep makes sure the operation has properly begun
                // before the symmetric part runs.
                sleep(time::Duration::from_millis(10));

                let mm2s_running_operation = mm2s_device
                    .do_dma_with_mem(mm2s_operation_with_mem)
                    .unwrap();

                (s2mm_running_operation, mm2s_running_operation)
            } else {
                let mm2s_running_operation = mm2s_device
                    .do_dma_with_mem(mm2s_operation_with_mem)
                    .unwrap();
                // The sleep makes sure the operation has properly begun
                // before the symmetric part runs.
                sleep(time::Duration::from_millis(10));

                let s2mm_running_operation = s2mm_device.do_dma(s2mm_operation).unwrap();

                (s2mm_running_operation, mm2s_running_operation)
            };

            let timeout = time::Duration::from_millis(1000);

            let (mm2s_device, stale_mm2s_operation, _) = mm2s_running_operation
                .completed(timeout.clone())
                .await
                .unwrap();
            let (s2mm_device, stale_s2mm_operation, s2mm_mem) = s2mm_running_operation
                .completed(timeout.clone())
                .await
                .unwrap();

            let s2mm_operation = stale_s2mm_operation.check_and_refresh().unwrap();
            let mm2s_operation = stale_mm2s_operation.check_and_refresh().unwrap();

            (
                s2mm_device,
                mm2s_device,
                s2mm_operation,
                mm2s_operation,
                s2mm_mem,
            )
        };

        let expected_slicer: Range<usize> = s2mm_operation.containing_memory().into();
        // Check the data
        assert_eq!(*s2mm_mem, *expected_s2mm_mem.get(expected_slicer).unwrap());

        drop(s2mm_mem);
        // Also check the whole memory is as expected.
        let s2mm_mem = s2mm_device.get_memory(0, s2mm_dma_data_size).unwrap();
        assert_eq!(&*s2mm_mem, expected_s2mm_mem.as_slice());

        (mm2s_device, s2mm_device, mm2s_operation, s2mm_operation)
    }

    /// The system should be able to transfer data from MM2S memory to S2MM
    /// memory via the PL using scatter gather DMA operations, where the
    /// arguments are defined:
    ///
    ///     block_size
    ///     ----------
    ///     Size (in bytes) of the blocks of data to be transferred. A block is
    ///     is the contiguous memory to be read from or written to by the DMA
    ///     transfer.
    ///
    ///     * 8 bytes <= block size < 2**23 - 1 (number of bytes a single descriptor can transfer)
    ///     * block size is a multiple of 8
    ///
    ///     Note: The combination of these two means max block size is 2**23 - 8
    ///
    ///     block_stride
    ///     ------------
    ///     Difference in offsets between the start of one block and the start of
    ///     the subsequent block.
    ///
    ///     * block size <= block stride < Size of device.dma_data memory
    ///     * block stride is a multiple of 8
    ///
    ///     n_blocks
    ///     --------
    ///     The number of blocks to be read from or written to by the DMA
    ///     transfer.
    ///
    ///     * n_blocks <= Size of device.dma_data memory/block_stride
    ///
    ///     offset
    ///     ------
    ///     The offset of the first block.
    ///
    ///     * 0 <= offset < Size of device.dma_data memory
    ///     * offset is a multiple of 8
    ///
    /// The system should be able to transfer data from any source location in
    /// the MM2S memory to any destination location in the S2MM memory.
    ///
    /// The system should be able to do multiple back to back DMA transfers.
    #[tokio::test]
    #[serial]
    async fn test_sg_dma_transfers() {
        let s2mm_device_path: PathBuf = ["/dev", "axi_dma_sg_s2mm"].iter().collect();
        let mm2s_device_path: PathBuf = ["/dev", "axi_dma_sg_mm2s"].iter().collect();

        // Create the devices. In the loopback PL the MM2S DMA engine stream
        // output is connected to the S2MM DMA engine stream input.
        let mut s2mm_device = scatter_gather::Device::new(&s2mm_device_path, BUS_DATA_WIDTH).unwrap();
        let mut mm2s_device = scatter_gather::Device::new(&mm2s_device_path, BUS_DATA_WIDTH).unwrap();

        let mm2s_dma_data_size = mm2s_device.memory_pool_size();
        let s2mm_dma_data_size = s2mm_device.memory_pool_size();

        assert!(s2mm_dma_data_size == mm2s_dma_data_size);

        let max_mm2s_blocks = mm2s_device.n_available_descriptors().unwrap();
        let max_s2mm_blocks = s2mm_device.n_available_descriptors().unwrap();

        assert!(max_mm2s_blocks == max_s2mm_blocks);

        let mut rng = SmallRng::from_entropy();

        // Generate a random value for block stride
        let rand_stride = 8 * rng.gen_range(1..(1 << 18) / 8);

        // Create argument combinations to test:
        //     Smallest permissible block size
        //     Largest permissible block size
        //     Block stride equals block size
        //     Largest permissible block stride
        //     Smallest permissible n_blocks (1)
        //     Largest permissible n_blocks (2048)
        //     Lowest permissible offset
        //     Highest permissible offset
        let block_sizes = vec![8, (1 << 23) - 8, rand_stride, 8, 8, 8];
        let block_strides = vec![8, (1 << 23) - 8, rand_stride, (1 << 23) - 8, 8, 8];
        let all_n_blocks = vec![1, 1, 2, 2, max_mm2s_blocks, 1];
        let offsets = vec![0, 0, 0, 0, 0, mm2s_dma_data_size - 8];

        for n in 0..block_sizes.len() {
            let block_size = block_sizes[n];
            let block_stride = block_strides[n];
            let n_blocks = all_n_blocks[n];
            let offset = offsets[n];

            // Write random data into the relevant appropriate locations in the
            // MM2S memory.
            let expected_s2mm_mem = gen_and_write_random_data(
                &mut mm2s_device,
                &mut s2mm_device,
                &block_size,
                &block_stride,
                &n_blocks,
                &offset,
                &block_size,
                &block_stride,
                &n_blocks,
                &offset,
            );

            // Set up the transfers into and out of the PL
            let mm2s_operation = mm2s_device
                .new_regular_blocks_operation(block_size, block_stride, n_blocks, offset)
                .unwrap();

            let s2mm_operation = s2mm_device
                .new_regular_blocks_operation(block_size, block_stride, n_blocks, offset)
                .unwrap();

            let s2mm_first = false;

            let (ret_mm2s_device, ret_s2mm_device, _, _) = trigger_wait_and_check_sg(
                mm2s_device,
                s2mm_device,
                mm2s_operation,
                s2mm_operation,
                &expected_s2mm_mem,
                s2mm_first,
            )
            .await;

            mm2s_device = ret_mm2s_device;
            s2mm_device = ret_s2mm_device;
        }
    }

    /// As long as the argument rules are obeyed, the system should be able to
    /// perform any combination of arguments (exluding n_blocks and block_size)
    /// for both MM2S and S2MM transfers. For the loopback PL that these tests
    /// require, n_blocks and block_size must be equal for both MM2S and S2MM
    /// transfers.
    ///
    /// The `check_and_refresh_operation` function should return the number of
    /// bytes that have been transferred.
    ///
    /// The system should be able to set up the MM2S operation first and then the
    /// S2MM operation or vice versa.
    #[tokio::test]
    #[serial]
    async fn test_sg_random_dma_transfers() {
        let s2mm_device_path: PathBuf = ["/dev", "axi_dma_sg_s2mm"].iter().collect();
        let mm2s_device_path: PathBuf = ["/dev", "axi_dma_sg_mm2s"].iter().collect();

        // Create the devices. In the loopback PL the MM2S DMA engine stream
        // output is connected to the S2MM DMA engine stream input.
        let mut s2mm_device = scatter_gather::Device::new(&s2mm_device_path, BUS_DATA_WIDTH).unwrap();
        let mut mm2s_device = scatter_gather::Device::new(&mm2s_device_path, BUS_DATA_WIDTH).unwrap();

        let s2mm_dma_data_size = s2mm_device.memory_pool_size();
        {
            // Create a vector to overwrite the destination before beginning the
            // transfer
            let destination_setup_data = vec![0u8; s2mm_dma_data_size];

            let mut s2mm_dma_data = s2mm_device.get_memory(0, s2mm_dma_data_size).unwrap();
            s2mm_dma_data.copy_from_slice(&destination_setup_data);
        }

        // Do four random transfers
        for _n in 0..4 {
            let (
                n_blocks,
                block_size,
                mm2s_block_stride,
                mm2s_offset,
                s2mm_block_stride,
                s2mm_offset,
                expected_destination_mem,
            ) = setup_random_sg_transfers(&mut mm2s_device, &mut s2mm_device);

            // Set up the transfers into and out of the PL
            let mm2s_operation = mm2s_device
                .new_regular_blocks_operation(block_size, mm2s_block_stride, n_blocks, mm2s_offset)
                .unwrap();
            let s2mm_operation = s2mm_device
                .new_regular_blocks_operation(block_size, s2mm_block_stride, n_blocks, s2mm_offset)
                .unwrap();

            // Randomly set s2mm to trigger first
            let s2mm_first: bool = rand::random();

            let (ret_mm2s_device, ret_s2mm_device, _, _) = trigger_wait_and_check_sg(
                mm2s_device,
                s2mm_device,
                mm2s_operation,
                s2mm_operation,
                &expected_destination_mem,
                s2mm_first,
            )
            .await;

            mm2s_device = ret_mm2s_device;
            s2mm_device = ret_s2mm_device;
        }
    }

    /// It should be possible to create multiple operations on a single device.
    /// It should be possible to run these operations in any order.
    ///
    /// The maximum number of operations it is possible to create on a device is
    /// the number of descriptor chunks on the device.
    #[tokio::test]
    #[serial]
    async fn test_sg_multiple_operations() {
        let s2mm_device_path: PathBuf = ["/dev", "axi_dma_sg_s2mm"].iter().collect();
        let mm2s_device_path: PathBuf = ["/dev", "axi_dma_sg_mm2s"].iter().collect();

        // Create the devices. In the loopback PL the MM2S DMA engine stream
        // output is connected to the S2MM DMA engine stream input.
        let mut s2mm_device = scatter_gather::Device::new(&s2mm_device_path, BUS_DATA_WIDTH).unwrap();
        let mut mm2s_device = scatter_gather::Device::new(&mm2s_device_path, BUS_DATA_WIDTH).unwrap();

        // Get the number of descriptor chunks on the devices. This gives us the
        // maximum number of operations we can create.
        let s2mm_n_descriptors = s2mm_device.n_available_descriptors().unwrap();
        let mm2s_n_descriptors = mm2s_device.n_available_descriptors().unwrap();

        assert!(s2mm_n_descriptors == mm2s_n_descriptors);

        let max_n_operations = s2mm_n_descriptors;

        let mm2s_dma_data_size = mm2s_device.memory_pool_size();
        let s2mm_dma_data_size = s2mm_device.memory_pool_size();

        assert!(s2mm_dma_data_size == mm2s_dma_data_size);

        let dma_data_size = s2mm_dma_data_size;

        let mut rng = SmallRng::from_entropy();

        let mut block_sizes = Vec::new();
        let mut block_strides = Vec::new();
        let mut all_n_blocks = Vec::new();
        let mut offsets = Vec::new();
        let mut mm2s_operations = Vec::new();
        let mut s2mm_operations = Vec::new();

        const N_TEST_OPERATIONS: usize = 8usize;

        let max_split = max_n_operations / N_TEST_OPERATIONS;

        let mut remaining_descriptors = s2mm_device.n_available_descriptors().unwrap();
        let mut n = 0;

        // Create up to the maximum number of operations
        while remaining_descriptors > 0 {
            // Generate random arguments for the operations
            block_sizes.push(8 * rng.gen_range(1..(1 << 16) / 8));
            block_strides.push(8 * rng.gen_range(block_sizes[n] / 8..(1 << 16) / 8));

            let n_blocks = {
                if remaining_descriptors > max_split {
                    rng.gen_range(1..max_split)
                } else {
                    remaining_descriptors
                }
            };

            // We only want to use one descriptor chunk per operation so that we
            // can create the max number of operations
            all_n_blocks.push(n_blocks);
            offsets.push(
                8 * rng.gen_range(0..(dma_data_size - block_strides[n] * all_n_blocks[n]) / 8),
            );

            // Set up the transfers into and out of the PL
            mm2s_operations.push(
                mm2s_device
                    .new_regular_blocks_operation(
                        block_sizes[n],
                        block_strides[n],
                        all_n_blocks[n],
                        offsets[n],
                    )
                    .unwrap(),
            );
            s2mm_operations.push(
                s2mm_device
                    .new_regular_blocks_operation(
                        block_sizes[n],
                        block_strides[n],
                        all_n_blocks[n],
                        offsets[n],
                    )
                    .unwrap(),
            );

            remaining_descriptors = s2mm_device.n_available_descriptors().unwrap();
            n += 1;
        }

        // Check that the descriptors have been consumed in the expected way.
        assert!(mm2s_device.n_available_descriptors().unwrap() == 0);

        let mut rng = SmallRng::from_entropy();

        // Do N_TEST_OPERATIONS random transfers
        for _n in 0..N_TEST_OPERATIONS {
            // Select a random operation
            let op = rng.gen_range(0..mm2s_operations.len());

            // Write random data into the relevant appropriate locations in the
            // MM2S memory.
            let expected_s2mm_mem = gen_and_write_random_data(
                &mut mm2s_device,
                &mut s2mm_device,
                &block_sizes[op],
                &block_strides[op],
                &all_n_blocks[op],
                &offsets[op],
                &block_sizes[op],
                &block_strides[op],
                &all_n_blocks[op],
                &offsets[op],
            );

            let s2mm_operation = s2mm_operations.remove(op);
            let mm2s_operation = mm2s_operations.remove(op);

            let s2mm_first = false;

            let (ret_mm2s_device, ret_s2mm_device, mm2s_operation, s2mm_operation) =
                trigger_wait_and_check_sg(
                    mm2s_device,
                    s2mm_device,
                    mm2s_operation,
                    s2mm_operation,
                    &expected_s2mm_mem,
                    s2mm_first,
                )
                .await;

            mm2s_device = ret_mm2s_device;
            s2mm_device = ret_s2mm_device;

            s2mm_operations.insert(op, s2mm_operation);
            mm2s_operations.insert(op, mm2s_operation);
        }
    }
}
