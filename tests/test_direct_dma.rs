#[cfg(all(test, target_arch = "arm"))]
mod direct_dma_tests {

    use std::{error::Error, path::PathBuf, thread::sleep, time};

    use rand::{distributions::Standard, rngs::SmallRng, Rng, SeedableRng};

    use itertools::izip;

    use serial_test::serial;

    use gannet::{uni_channel::direct, uni_channel::direct::DirectTransferError, BusDataWidth};

    const BUS_DATA_WIDTH: BusDataWidth = BusDataWidth::FourBytes;
    const BUS_WIDTH: usize = BUS_DATA_WIDTH as usize;

    pub fn run_direct_dma_and_check(
        mut mm2s_device: direct::Device,
        mut s2mm_device: direct::Device,
        src_offset: usize,
        dest_offset: usize,
        nbytes: usize,
        test_data: Vec<u8>,
        s2mm_first: bool,
    ) -> Result<(direct::Device, direct::Device), Box<dyn Error>> {
        // Create a vector to overwrite the destination before beginning the
        // transfer
        let destination_setup_data = vec![1u8; nbytes];
        // Create a vector for a sanity check on the destination memory
        let mut test_destination_data = vec![0u8; nbytes];
        // Create a vector into which we can copy the data out of the destination
        // memory
        let mut destination_data = vec![0u8; nbytes];

        {
            let mut s2mm_dma_data = s2mm_device.get_memory(dest_offset, nbytes)?;
            s2mm_dma_data.copy_from_slice(destination_setup_data.as_slice());
            test_destination_data.copy_from_slice(s2mm_dma_data.as_slice());
        }

        let mut mm2s_dma_data = mm2s_device.get_checked_memory(src_offset, nbytes)?;
        mm2s_dma_data.copy_from_slice(test_data.as_slice());

        // Sanity check to make sure the destination memory is known before the
        // transfer. This is so we know any change is cause by the DMA transfer.
        assert!(test_destination_data == destination_setup_data);

        // Trigger the DMA transfers
        let (running_s2mm_device, running_mm2s_device) = {
            if s2mm_first {
                let running_s2mm_device = s2mm_device.do_dma(dest_offset, nbytes)?;
                // The sleep makes sure the operation has properly begun
                // before the symmetric part runs.
                sleep(time::Duration::from_millis(10));

                let running_mm2s_device = mm2s_device.do_dma_with_mem(mm2s_dma_data)?;

                (running_s2mm_device, running_mm2s_device)
            } else {
                let running_mm2s_device = mm2s_device.do_dma_with_mem(mm2s_dma_data)?;
                // The sleep makes sure the operation has properly begun
                // before the symmetric part runs.
                sleep(time::Duration::from_millis(10));
                let running_s2mm_device = s2mm_device.do_dma(dest_offset, nbytes)?;

                (running_s2mm_device, running_mm2s_device)
            }
        };

        let timeout = time::Duration::from_millis(1000);

        // Wait for DMA transfers to complete
        let (mm2s_device, _) = running_mm2s_device.await_completed(timeout.clone())?;
        let (s2mm_device, s2mm_dma_data) = running_s2mm_device.await_completed(timeout.clone())?;

        destination_data.copy_from_slice(s2mm_dma_data.as_slice());

        // Check that we have received the correct data.
        assert!(test_data == destination_data);

        Ok((mm2s_device, s2mm_device))
    }

    /// The system should be able to transfer data from MM2S memory to S2MM
    /// memory via the PL, such that:
    ///
    ///     * 8 bytes <= transfer size < 8 Mbytes
    ///     * Transfer size is a multiple of 8
    ///
    /// The system should be able to transfer data from any source location in
    /// the MM2S memory to any destination location in the S2MM memory, such
    /// that:
    ///
    ///     * 0 <= source location < MM2S memory size
    ///     * Source location is a multiple of 8
    ///     * 0 <= destination location < S2MM memory size
    ///     * Destination location is a multiple of 8
    ///
    /// The system should be able to do multiple back to back DMA
    /// transfers.
    #[test]
    #[serial]
    fn test_direct_dma_transfers() {
        // This test uses DMA to transfer 8 bytes from the highest available
        // source offset to the highest possble destination offset. Then
        // (self.max_transfer_size - 8) bytes from source offset 0 to
        // destination offset 0. Then eight random amounts of  data from a
        // random source offset in the mm2s memory to the PL and then back into
        // a random destination offset in the s2mm memory.

        let s2mm_device_path: PathBuf = ["/dev", "axi_dma_s2mm"].iter().collect();
        let mm2s_device_path: PathBuf = ["/dev", "axi_dma_mm2s"].iter().collect();

        // Create the devices. In the loopback PL the MM2S DMA engine stream
        // output is connected to the S2MM DMA engine stream input.
        let mut s2mm_device = direct::Device::new(
            &s2mm_device_path, BUS_DATA_WIDTH).unwrap();
        let mut mm2s_device = direct::Device::new(
            &mm2s_device_path, BUS_DATA_WIDTH).unwrap();

        let s2mm_dma_data_size = s2mm_device.memory_pool_size();
        let mm2s_dma_data_size = mm2s_device.memory_pool_size();

        let mut rng = SmallRng::from_entropy();

        // Test the smallest transfer size of BUS_WIDTH bytes into the highest memory
        // position and the largest possible transfer size into the offset 0.
        let mut sizes = vec![BUS_WIDTH, (1 << 23) - BUS_WIDTH];
        let mut src_offsets = vec![mm2s_dma_data_size - BUS_WIDTH, 0];
        let mut dest_offsets = vec![s2mm_dma_data_size - BUS_WIDTH, 0];

        let s2mm_first: bool = false;

        // Generate valid random values for the size and source & destination
        // offsets of the transfer. We don't use the full range here to speed the
        // test up.
        for n in 0..3 {
            sizes.push(BUS_WIDTH * rng.gen_range(BUS_WIDTH..(1 << 16) / BUS_WIDTH));
            src_offsets
                .push(BUS_WIDTH * rng.gen_range(0..(mm2s_dma_data_size - sizes[n + 2]) / BUS_WIDTH));
            dest_offsets
                .push(BUS_WIDTH * rng.gen_range(0..(s2mm_dma_data_size - sizes[n + 2]) / BUS_WIDTH));
        }

        for (nbytes, src_offset, dest_offset) in izip!(&sizes, &src_offsets, &dest_offsets) {
            // Generate random data
            let test_data: Vec<u8> = (&mut rng).sample_iter(&Standard).take(*nbytes).collect();

            let (ret_mm2s_device, ret_s2mm_device) = run_direct_dma_and_check(
                mm2s_device,
                s2mm_device,
                *src_offset,
                *dest_offset,
                *nbytes,
                test_data,
                s2mm_first,
            )
            .unwrap();

            mm2s_device = ret_mm2s_device;
            s2mm_device = ret_s2mm_device;
        }
    }

    ///The system should be cache coherent after a transfer.
    #[test]
    #[serial]
    fn test_cache_coherency() {
        // This test uses DMA to transfer data from mm2s memory to the PL and
        // then back into s2mm memory. A second DMA transfer is then performed
        // to overwrite the part of the newly transferred data with another
        // section of the original data. When the data is read from destination
        // s2mm memory, it should match the original data with one section
        // modified.
        //
        // Example:
        // ********
        // Original data in mm2s:               |__1__|__2__|__3__|__4__|
        //
        // Data in s2mm after initial transfer: |__1__|__2__|__3__|__4__|
        //
        // Second transfer just reads out section 4 and places it in section 2.
        //
        // Data in s2mm after second transfer:  |__1__|__4__|__3__|__4__|
        //
        // When the userspace application then reads the full data from the
        // s2mm memory, it should read the data correctly. Ie the cache has
        // been updated as part of the transfer.
        //
        // Note, this doesn't necessarily check things like the accelerated
        // coherency port if the memory is labelled in the driver as being
        // strongly ordered. In that case, the data never touches the cache
        // (which amounts to the same thing as the cache being coherent).

        let s2mm_device_path: PathBuf = ["/dev", "axi_dma_s2mm"].iter().collect();
        let mm2s_device_path: PathBuf = ["/dev", "axi_dma_mm2s"].iter().collect();

        // Create the devices. In the loopback PL the MM2S DMA engine stream
        // output is connected to the S2MM DMA engine stream input.
        let mut s2mm_device = direct::Device::new(&s2mm_device_path, BUS_DATA_WIDTH).unwrap();
        let mut mm2s_device = direct::Device::new(&mm2s_device_path, BUS_DATA_WIDTH).unwrap();

        let s2mm_dma_data_size = s2mm_device.memory_pool_size();
        let mm2s_dma_data_size = mm2s_device.memory_pool_size();

        let mut rng = SmallRng::from_entropy();

        let s2mm_first: bool = false;

        for _n in 0..4 {
            // Randomly set the size of the transfer. We don't use the full range
            // here to speed the test up.
            let transfer_size = BUS_WIDTH * rng.gen_range(BUS_WIDTH..(1 << 16) / BUS_WIDTH);
            // Pick random source and destination offsets
            let src_offset =
                BUS_WIDTH * rng.gen_range(0..(mm2s_dma_data_size - transfer_size) / BUS_WIDTH);
            let dest_offset =
                BUS_WIDTH * rng.gen_range(0..(s2mm_dma_data_size - transfer_size) / BUS_WIDTH);

            // Generate random test data
            let test_data: Vec<u8> = (&mut rng)
                .sample_iter(&Standard)
                .take(transfer_size)
                .collect();

            // Randomly set the size of the overwriting transfer and the offsets
            // for the overwriting transfer
            let overwrite_transfer_size =
                BUS_WIDTH * rng.gen_range(BUS_WIDTH..transfer_size / BUS_WIDTH);
            let overwrite_src_offset =
                BUS_WIDTH * rng.gen_range(
                    src_offset / BUS_WIDTH
                    ..(src_offset + transfer_size - overwrite_transfer_size) / BUS_WIDTH,
                );
            let overwrite_dest_offset = BUS_WIDTH * rng.gen_range(
                dest_offset / BUS_WIDTH
                ..(dest_offset + transfer_size - overwrite_transfer_size) / BUS_WIDTH,
            );

            // Create a vector to take the expected destination data
            let mut expected_destination_data = vec![0; transfer_size];
            expected_destination_data.copy_from_slice(&test_data);

            expected_destination_data[(overwrite_dest_offset - dest_offset)
                ..(overwrite_dest_offset - dest_offset + overwrite_transfer_size)]
                .copy_from_slice(
                    &test_data[(overwrite_src_offset - src_offset)
                        ..(overwrite_src_offset - src_offset + overwrite_transfer_size)],
                );

            let (ret_mm2s_device, ret_s2mm_device) = run_direct_dma_and_check(
                mm2s_device,
                s2mm_device,
                src_offset,
                dest_offset,
                transfer_size,
                test_data,
                s2mm_first,
            )
            .unwrap();

            mm2s_device = ret_mm2s_device;
            s2mm_device = ret_s2mm_device;

            // Trigger the DMA overwriting transfers
            let running_mm2s_device = mm2s_device
                .do_dma(overwrite_src_offset, overwrite_transfer_size)
                .unwrap();
            let running_s2mm_device = s2mm_device
                .do_dma(overwrite_dest_offset, overwrite_transfer_size)
                .unwrap();

            let timeout = time::Duration::from_millis(1000);

            // Wait for DMA transfers to complete
            let (ret_mm2s_device, _) = running_mm2s_device
                .await_completed(timeout.clone())
                .unwrap();
            let (ret_s2mm_device, _) = running_s2mm_device
                .await_completed(timeout.clone())
                .unwrap();

            mm2s_device = ret_mm2s_device;
            s2mm_device = ret_s2mm_device;

            // Create a vector into which we can copy the data out of the
            // destination memory
            let mut destination_data = vec![0u8; transfer_size];

            let s2mm_dma_data = s2mm_device.get_memory(dest_offset, transfer_size).unwrap();
            destination_data.copy_from_slice(s2mm_dma_data.as_slice());

            // Check that we have received the correct data.
            assert!(expected_destination_data == destination_data);
        }
    }

    /// The system should not overwrite any addresses other than those
    /// specified in the DMA transfer.
    #[test]
    #[serial]
    fn test_direct_correct_destination_address() {
        // This test uses DMA to transfer data from mm2s memory to the PL and
        // then back into four different s2mm memory locations. Each write
        // should only affect the memory locations specified.

        let s2mm_device_path: PathBuf = ["/dev", "axi_dma_s2mm"].iter().collect();
        let mm2s_device_path: PathBuf = ["/dev", "axi_dma_mm2s"].iter().collect();

        // Create the devices. In the loopback PL the MM2S DMA engine stream
        // output is connected to the S2MM DMA engine stream input.
        let mut s2mm_device = direct::Device::new(&s2mm_device_path, BUS_DATA_WIDTH).unwrap();
        let mut mm2s_device = direct::Device::new(&mm2s_device_path, BUS_DATA_WIDTH).unwrap();

        // Get a pointer to and the size of the memory for each DMA device
        let s2mm_dma_data_size = s2mm_device.memory_pool_size();
        let mm2s_dma_data_size = mm2s_device.memory_pool_size();

        let mut rng = SmallRng::from_entropy();

        let s2mm_first: bool = false;
        // Create a vector to overwrite the destination before beginning the
        // transfer
        let destination_setup_data = vec![0u8; s2mm_dma_data_size];

        for _n in 0..4 {
            // Randomly set the size of the transfer. We don't use the full range
            // here to speed the test up.
            let nbytes = BUS_WIDTH * rng.gen_range(BUS_WIDTH..(1 << 16) / BUS_WIDTH);
            // Pick random source and destination offsets
            let src_offset =
                BUS_WIDTH * rng.gen_range(0..(mm2s_dma_data_size - nbytes) / BUS_WIDTH);
            let dest_offset =
                BUS_WIDTH * rng.gen_range(0..(s2mm_dma_data_size - nbytes) / BUS_WIDTH);

            // Generate random data
            let test_data: Vec<u8> = (&mut rng).sample_iter(&Standard).take(nbytes).collect();

            // Create a vector to take the expected destination data
            let mut expected_destination_data = vec![0u8; s2mm_dma_data_size];
            expected_destination_data[dest_offset..(dest_offset + nbytes)]
                .copy_from_slice(&test_data);

            {
                let mut s2mm_dma_data = s2mm_device.get_memory(0, s2mm_dma_data_size).unwrap();
                s2mm_dma_data.copy_from_slice(destination_setup_data.as_slice());
            }

            let (ret_mm2s_device, ret_s2mm_device) = run_direct_dma_and_check(
                mm2s_device,
                s2mm_device,
                src_offset,
                dest_offset,
                nbytes,
                test_data,
                s2mm_first,
            )
            .unwrap();

            mm2s_device = ret_mm2s_device;
            s2mm_device = ret_s2mm_device;

            // Create a vector into which we can copy the data out of the
            // destination memory
            let mut destination_data = vec![0u8; s2mm_dma_data_size];

            let s2mm_dma_data = s2mm_device.get_memory(0, s2mm_dma_data_size).unwrap();
            destination_data.copy_from_slice(s2mm_dma_data.as_slice());

            // Check that we have received the correct data.
            assert!(expected_destination_data == destination_data);
        }
    }

    /// Setting a zero length DMA should be possible.
    #[test]
    #[serial]
    fn test_zero_length_dma() {
        // Create the device
        let device_path: PathBuf = ["/dev", "axi_dma_mm2s"].iter().collect();
        let device = direct::Device::new(&device_path, BUS_DATA_WIDTH).unwrap();

        let running_dma = device.do_dma(0usize, 0usize).unwrap();

        // This should return very quickly, since nothing actually happens.
        let (device, mem_block) = running_dma
            .await_completed(std::time::Duration::from_millis(1))
            .unwrap();

        assert!(mem_block.as_slice().len() == 0);

        // We now check the device is still good and ready to roll again
        let s2mm_device_path: PathBuf = ["/dev", "axi_dma_s2mm"].iter().collect();
        let s2mm_device = direct::Device::new(&s2mm_device_path, BUS_DATA_WIDTH).unwrap();

        let nbytes = 100;
        let mut rng = SmallRng::from_entropy();
        let test_data: Vec<u8> = (&mut rng).sample_iter(&Standard).take(nbytes).collect();
        let s2mm_first = false;

        run_direct_dma_and_check(device, s2mm_device, 0, 0, nbytes, test_data, s2mm_first)
            .unwrap();
    }

    /// The system should be able to set up the MM2S operation first and then the
    /// S2MM operation or vice versa.
    #[test]
    #[serial]
    fn test_direct_dma_order() {
        // This test sets up the S2MM transfer and then the MM2S transfer for the
        // first transfer. It then performs up a second transfer by setting up the
        // MM2S channel and then the S2MM channel. It then performs 8 more
        // transfers by randomly selecting which channel to set up first.

        let s2mm_device_path: PathBuf = ["/dev", "axi_dma_s2mm"].iter().collect();
        let mm2s_device_path: PathBuf = ["/dev", "axi_dma_mm2s"].iter().collect();

        // Create the devices. In the loopback PL the MM2S DMA engine stream
        // output is connected to the S2MM DMA engine stream input.
        let mut s2mm_device = direct::Device::new(&s2mm_device_path, BUS_DATA_WIDTH).unwrap();
        let mut mm2s_device = direct::Device::new(&mm2s_device_path, BUS_DATA_WIDTH).unwrap();

        let s2mm_dma_data_size = s2mm_device.memory_pool_size();
        let mm2s_dma_data_size = mm2s_device.memory_pool_size();

        let mut rng = SmallRng::from_entropy();

        for n in 0..4 {
            // Alternate between performing an mm2s transfer and an s2mm transfer
            // first
            let s2mm_first = if n % 2 == 0 { false } else { true };

            // Generate valid random values for the size and source & destination
            // offsets of the transfer. We don't use the full range here to speed
            // the test up.
            let nbytes = BUS_WIDTH * rng.gen_range(BUS_WIDTH..(1 << 16) / BUS_WIDTH);
            let src_offset =
                BUS_WIDTH * rng.gen_range(0..(mm2s_dma_data_size - nbytes) / BUS_WIDTH);
            let dest_offset =
                BUS_WIDTH * rng.gen_range(0..(s2mm_dma_data_size - nbytes) / BUS_WIDTH);

            // Generate random data
            let test_data: Vec<u8> = (&mut rng).sample_iter(&Standard).take(nbytes).collect();

            let (ret_mm2s_device, ret_s2mm_device) = run_direct_dma_and_check(
                mm2s_device,
                s2mm_device,
                src_offset,
                dest_offset,
                nbytes,
                test_data,
                s2mm_first,
            )
            .unwrap();

            mm2s_device = ret_mm2s_device;
            s2mm_device = ret_s2mm_device;
        }
    }

    /// The system should return the number of bytes that was actually
    /// transferred.
    ///
    /// In the event that a program initiates an S2MM transfer requesting more
    /// data than is available in the incoming data packet, the system will
    /// return when the DMA engine has transferred the full incoming data
    /// packet even though this is less than the requested number of bytes.
    ///
    /// The calling function will be expecting the amount requested so the
    /// system should return the number of bytes that were actually
    /// transferred.
    #[test]
    #[serial]
    fn test_direct_dma_mismatched_lengths() {
        // This test uses DMA to transfer a random amounta of data from a
        // random source offset in the mm2s memory to the PL. It then requests
        // a larger amount of data is transferred back into a random
        // destination offset in the s2mm memory. It does this 4 times then it
        // performs one good read to ensure the DMA is still functioning.

        let s2mm_device_path: PathBuf = ["/dev", "axi_dma_s2mm"].iter().collect();
        let mm2s_device_path: PathBuf = ["/dev", "axi_dma_mm2s"].iter().collect();

        // Create the devices. In the loopback PL the MM2S DMA engine stream
        // output is connected to the S2MM DMA engine stream input.
        let mut s2mm_device = direct::Device::new(&s2mm_device_path, BUS_DATA_WIDTH).unwrap();
        let mut mm2s_device = direct::Device::new(&mm2s_device_path, BUS_DATA_WIDTH).unwrap();

        let s2mm_dma_data_size = s2mm_device.memory_pool_size();
        let mm2s_dma_data_size = mm2s_device.memory_pool_size();

        let mut rng = SmallRng::from_entropy();

        let s2mm_first: bool = false;

        for _n in 0..4 {
            // Set the mm2s size. We don't use the full range here to speed the
            // test up.
            let mm2s_nbytes =
                BUS_WIDTH * rng.gen_range(BUS_WIDTH..(1 << 16) / BUS_WIDTH);
            // Set the s2mm size. Make sure the s2mm size is larger than the mm2s.
            // We don't use the full range here to speed the test up.
            let s2mm_nbytes =
                BUS_WIDTH * rng.gen_range(mm2s_nbytes / BUS_WIDTH..(1 << 18) / BUS_WIDTH);
            // Generate valid random values for source & destination offsets of
            // the transfer.
            let src_offset =
                BUS_WIDTH * rng.gen_range(0..(mm2s_dma_data_size - mm2s_nbytes) / BUS_WIDTH);
            let dest_offset =
                BUS_WIDTH * rng.gen_range(0..(s2mm_dma_data_size - s2mm_nbytes) / BUS_WIDTH);

            // Generate random data
            let test_data: Vec<u8> = (&mut rng)
                .sample_iter(&Standard)
                .take(mm2s_nbytes)
                .collect();

            // Set up the expected destination data. As the s2mm transfer size is
            // smaller than the mm2s transfer size we expected the mm2s size of
            // data to contain zeros at the end to make up the difference.
            let mut expected_destination_data = vec![0u8; mm2s_nbytes];
            expected_destination_data[..mm2s_nbytes].copy_from_slice(&test_data);
            expected_destination_data.append(&mut vec![0u8; s2mm_nbytes - mm2s_nbytes]);

            // Create a vector to overwrite the destination before beginning the
            // transfer
            let destination_setup_data = vec![0u8; s2mm_nbytes];

            {
                let mut s2mm_dma_data = s2mm_device.get_memory(dest_offset, s2mm_nbytes).unwrap();
                s2mm_dma_data.copy_from_slice(destination_setup_data.as_slice());
            }

            let mut mm2s_dma_data = mm2s_device
                .get_checked_memory(src_offset, mm2s_nbytes)
                .unwrap();
            mm2s_dma_data.copy_from_slice(test_data.as_slice());

            // Trigger the DMA transfers
            let running_mm2s_device = mm2s_device.do_dma_with_mem(mm2s_dma_data).unwrap();
            let running_s2mm_device = s2mm_device.do_dma(dest_offset, s2mm_nbytes).unwrap();

            let timeout = time::Duration::from_millis(1000);

            // Wait for DMA transfers to complete
            let (ret_mm2s_device, _) = running_mm2s_device
                .await_completed(timeout.clone())
                .unwrap();
            mm2s_device = ret_mm2s_device;

            match running_s2mm_device.await_completed(timeout.clone()) {
                Err(DirectTransferError::TransferSize {
                    transferred,
                    expected: _,
                    errored_device: errored_s2mm_device,
                }) => {
                    assert!(transferred == mm2s_nbytes);
                    s2mm_device = errored_s2mm_device.reset().unwrap();
                }
                _ => panic!("It it expected to get a DirectTransferError::TransferSize error"),
            }

            // We still expect the data that was DMAd to be correct.

            // Create a vector into which we can copy the data out of the
            // destination memory
            let mut destination_data = vec![0u8; s2mm_nbytes];

            // Read the destination memory
            {
                let s2mm_dma_data = s2mm_device.get_memory(dest_offset, s2mm_nbytes).unwrap();

                destination_data.copy_from_slice(s2mm_dma_data.as_slice());
            }

            // Check that we have received the correct data.
            assert!(expected_destination_data == destination_data);
        }

        // Generate valid random values for the size and source & destination
        // offsets of the transfer. We don't use the full range here to speed
        // the test up.
        let nbytes = BUS_WIDTH * rng.gen_range(BUS_WIDTH..(1 << 16) / BUS_WIDTH);
        let src_offset =
            BUS_WIDTH * rng.gen_range(0..(mm2s_dma_data_size - nbytes) / BUS_WIDTH);
        let dest_offset =
            BUS_WIDTH * rng.gen_range(0..(s2mm_dma_data_size - nbytes) / BUS_WIDTH);

        // Generate random data
        let test_data: Vec<u8> = (&mut rng).sample_iter(&Standard).take(nbytes).collect();

        let (_ret_mm2s_device, _ret_s2mm_device) = run_direct_dma_and_check(
            mm2s_device,
            s2mm_device,
            src_offset,
            dest_offset,
            nbytes,
            test_data,
            s2mm_first,
        )
        .unwrap();
    }
}
