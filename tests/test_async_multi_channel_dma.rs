#[cfg(all(test, target_arch = "arm", feature = "async"))]
mod async_multi_channel_dma_tests {

    use std::{cmp::min, collections::HashMap, convert::TryInto, ops::Range, path::PathBuf, time};

    use rand::{distributions::Standard, rngs::SmallRng, Rng, SeedableRng};

    use serial_test::serial;

    use gannet::{
        BusDataWidth,
        multi_channel,
        multi_channel::{Channel, MultiChannelOperation},
        operation::{RefreshStaleOperation, ScatterGatherOperation},
    };

    const BUS_DATA_WIDTH: BusDataWidth = BusDataWidth::FourBytes;
    //const BUS_WIDTH: usize = BUS_DATA_WIDTH as usize;

    use tokio;

    // This can be imported in from multi_channel, but for the purposes
    // of this test, we use 5. This is because it allows us to test on a
    // resource limited instance of the test loopback design.
    const MAX_CHANNELS: u8 = 5u8;

    // This function generates random, valid parameters for an MM2S transfer and a
    // corresponding S2MM transfer.
    fn gen_random_valid_parameters(
        mm2s_device: &multi_channel::Device,
        s2mm_device: &multi_channel::Device,
    ) -> (usize, usize, usize, usize, usize, usize) {
        let mut rng = SmallRng::from_entropy();

        let mm2s_dma_data_size = mm2s_device.memory_pool_size();
        let s2mm_dma_data_size = s2mm_device.memory_pool_size();

        // Get the smaller of the two memory sizes
        let dma_data_size = min(mm2s_dma_data_size, s2mm_dma_data_size);

        // Need to check how many descriptors are available on the devices
        let n_mm2s_available_descriptors = mm2s_device.n_available_descriptors().unwrap();
        let n_s2mm_available_descriptors = s2mm_device.n_available_descriptors().unwrap();

        // Get the smaller of the two n_available_descriptors
        let n_available_descriptors =
            min(n_mm2s_available_descriptors, n_s2mm_available_descriptors);

        // Generate a random value for the block_size
        let block_size = 8 * rng.gen_range(1..(1 << 16) / 8);

        // Determine the maximum number of blocks that can fit within the
        // memory
        let n_available_mem_blocks = dma_data_size / block_size;

        // Get the smaller of n_available_mem_blocks and
        // n_available_descriptors as the smaller of these two values
        // determines the maximum number of blocks we can have.
        let physical_constraint_on_n_blocks = min(n_available_mem_blocks, n_available_descriptors);

        // Set an upper limit of 40 on the max_n_blocks
        let max_n_blocks = min(40, physical_constraint_on_n_blocks);

        // Randomly set n_blocks
        let n_blocks = rng.gen_range(1..max_n_blocks + 1);

        // Generate a random value for the block strides
        let mm2s_block_stride =
            8 * rng.gen_range(block_size / 8..(mm2s_dma_data_size / n_blocks) / 8 + 1);
        let s2mm_block_stride =
            8 * rng.gen_range(block_size / 8..(s2mm_dma_data_size / n_blocks) / 8 + 1);

        // Generate a random source and destination offset
        let mm2s_offset =
            8 * rng.gen_range(0..(mm2s_dma_data_size - mm2s_block_stride * n_blocks) / 8);
        let s2mm_offset =
            8 * rng.gen_range(0..(s2mm_dma_data_size - s2mm_block_stride * n_blocks) / 8);

        (
            n_blocks,
            block_size,
            mm2s_block_stride,
            mm2s_offset,
            s2mm_block_stride,
            s2mm_offset,
        )
    }

    // Given operation arguments this function will set up the MM2S memory with
    // random data that satisfies those arguments and will return the expected
    // S2MM memory.
    //
    // Note: This function does not check that the arguments will be valid. That
    // must be performed in the calling function
    fn gen_and_write_random_data(
        mm2s_device: &mut multi_channel::Device,
        s2mm_device: &mut multi_channel::Device,
        mm2s_block_size: &usize,
        mm2s_block_stride: &usize,
        mm2s_n_blocks: &usize,
        mm2s_offset: &usize,
        s2mm_block_size: &usize,
        s2mm_block_stride: &usize,
        s2mm_n_blocks: &usize,
        s2mm_offset: &usize,
    ) -> (Vec<u8>, Vec<u8>) {
        let mut rng = SmallRng::from_entropy();

        let s2mm_dma_data_size = s2mm_device.memory_pool_size();
        let mm2s_dma_data_size = mm2s_device.memory_pool_size();

        // Create a vector to overwrite the destination before beginning the
        // transfer
        let destination_setup_data = vec![0u8; s2mm_dma_data_size];

        {
            let mut s2mm_dma_data = s2mm_device.get_memory(0, s2mm_dma_data_size).unwrap();
            s2mm_dma_data.copy_from_slice(&destination_setup_data);
        }

        // Create a vector to take the mm2s block
        let mut raw_data = Vec::new();
        let mut set_mm2s_mem = vec![0u8; mm2s_dma_data_size];
        {
            // We zero the mm2s data to clear out old values.
            let mut mm2s_dma_data = mm2s_device.get_memory(0, mm2s_dma_data_size).unwrap();
            mm2s_dma_data.copy_from_slice(&set_mm2s_mem);
        }
        for block_num in 0..*mm2s_n_blocks {
            // Generate random test data
            let test_data: Vec<u8> = (&mut rng)
                .sample_iter(&Standard)
                .take(*mm2s_block_size)
                .collect();

            raw_data.extend_from_slice(&test_data);

            // Get the offset of the block
            let block_offset = mm2s_offset + block_num * mm2s_block_stride;

            {
                let mut mm2s_dma_data = mm2s_device
                    .get_memory(block_offset, *mm2s_block_size)
                    .unwrap();

                mm2s_dma_data.copy_from_slice(&test_data);
                set_mm2s_mem[block_offset..][..*mm2s_block_size].copy_from_slice(&test_data);
            }
        }

        let mut expected_s2mm_mem = vec![0u8; s2mm_dma_data_size];

        for n in 0..*s2mm_n_blocks {
            // Work out the block offset in destination memory
            let block_offset = s2mm_offset + n * s2mm_block_stride;
            // Set up the expected destination data
            expected_s2mm_mem[block_offset..(block_offset + s2mm_block_size)]
                .copy_from_slice(&raw_data[n * s2mm_block_size..(n + 1) * s2mm_block_size]);
        }

        (expected_s2mm_mem, set_mm2s_mem)
    }

    /// The system should be able to transfer data from MM2S memory to S2MM
    /// memory via the PL using multi channel DMA operations, where the
    /// parameters are defined:
    ///
    ///     block_size
    ///     ----------
    ///     Size (in bytes) of the blocks of data to be transferred. A block is
    ///     is the contiguous memory to be read from or written to by the DMA
    ///     transfer.
    ///
    ///     * 8 bytes <= block size < 2**26 - 1 (number of bytes a single descriptor can transfer)
    ///     * block size is a multiple of 8
    ///
    ///     Note: The combination of these two means max block size is 2**26 - 8
    ///
    ///     block_stride
    ///     ------------
    ///     Difference in offsets between the start of one block and the start of
    ///     the subsequent block.
    ///
    ///     * block size <= block stride < Size of device.dma_data memory
    ///     * block stride is a multiple of 8
    ///
    ///     n_blocks
    ///     --------
    ///     The number of blocks to be read from or written to by the DMA
    ///     transfer.
    ///
    ///     * n_blocks <= Size of device.dma_data memory/block_stride
    ///
    ///     offset
    ///     ------
    ///     The offset of the first block.
    ///
    ///     * 0 <= offset < Size of device.dma_data memory
    ///     * offset is a multiple of 8
    ///
    /// The system should be able to transfer data from any source location in
    /// the MM2S memory to any destination location in the S2MM memory.
    ///
    /// The system should be able to do multiple back to back DMA transfers.
    #[tokio::test]
    #[serial]
    async fn test_mc_dma_transfer() {
        let s2mm_device_path: PathBuf = ["/dev", "axi_dma_mc_s2mm"].iter().collect();
        let mm2s_device_path: PathBuf = ["/dev", "axi_dma_mc_mm2s"].iter().collect();

        // Create the devices. In the loopback PL the MM2S DMA engine stream
        // output is connected to the S2MM DMA engine stream input.
        let mut s2mm_device = multi_channel::Device::new(&s2mm_device_path, BUS_DATA_WIDTH).unwrap();
        let mut mm2s_device = multi_channel::Device::new(&mm2s_device_path, BUS_DATA_WIDTH).unwrap();

        let mm2s_dma_data_size = mm2s_device.memory_pool_size();
        let s2mm_dma_data_size = s2mm_device.memory_pool_size();

        let mut rng = SmallRng::from_entropy();

        let max_mm2s_blocks = mm2s_device.n_available_descriptors().unwrap();
        let max_s2mm_blocks = s2mm_device.n_available_descriptors().unwrap();

        assert!(max_mm2s_blocks == max_s2mm_blocks);

        // Generate a random value for block stride
        let rand_stride = 8 * rng.gen_range(1..(1 << 18) / 8);

        // Create argument combinations to test:
        //     Smallest permissible block size
        //     Largest permissible block size (memory size can only accomodate 2**24)
        //     Block stride equals block size
        //     Largest permissible block stride
        //     Smallest permissible n_blocks (1)
        //     Largest permissible n_blocks (2048)
        //     Lowest permissible offset
        //     Highest permissible offset
        let block_sizes = vec![8, (1 << 24) - 8, rand_stride, 8, 8, 8];
        let block_strides = vec![8, (1 << 24) - 8, rand_stride, (1 << 23) - 8, 8, 8];
        let all_n_blocks = vec![1, 1, 2, 2, max_mm2s_blocks, 1];
        let offsets = vec![0, 0, 0, 0, 0, mm2s_dma_data_size - 8];

        // Pick a random channel to test all of the argument combinations on.
        let channel = Channel::new(rng.gen_range(0..MAX_CHANNELS)).unwrap();

        for n in 0..block_sizes.len() {
            let block_size = block_sizes[n];
            let block_stride = block_strides[n];
            let n_blocks = all_n_blocks[n];
            let offset = offsets[n];

            // Write random data into the MM2S memory and based on the parameters
            // get the expected S2MM memory
            let (expected_s2mm_mem, set_mm2s_mem) = gen_and_write_random_data(
                &mut mm2s_device,
                &mut s2mm_device,
                &block_size,
                &block_stride,
                &n_blocks,
                &offset,
                &block_size,
                &block_stride,
                &n_blocks,
                &offset,
            );

            let mm2s_operation = mm2s_device
                .new_regular_blocks_operation(block_size, block_stride, n_blocks, offset)
                .unwrap();
            let s2mm_operation = s2mm_device
                .new_regular_blocks_operation(block_size, block_stride, n_blocks, offset)
                .unwrap();

            // Create the operations hashmaps
            let mut mm2s_operations = HashMap::new();
            let mut s2mm_operations = HashMap::new();

            // Add the operations to the operations hash map
            s2mm_operations.insert(channel, s2mm_operation);
            mm2s_operations.insert(channel, mm2s_operation);

            let s2mm_operations: MultiChannelOperation = s2mm_operations.try_into().unwrap();
            let mm2s_operations: MultiChannelOperation = mm2s_operations.try_into().unwrap();

            let mm2s_slicer: Range<usize> = mm2s_operations.containing_memory().into();

            let mm2s_operations_with_mem = mm2s_operations.claim_memory(&mut mm2s_device).unwrap();

            assert!(*mm2s_operations_with_mem == *set_mm2s_mem.get(mm2s_slicer).unwrap());

            let s2mm_running_operation = s2mm_device.do_dma(s2mm_operations).unwrap();
            let mm2s_running_operation = mm2s_device
                .do_dma_with_mem(mm2s_operations_with_mem)
                .unwrap();

            let timeout = time::Duration::from_millis(1000);

            // Wait for DMA transfers to complete
            let (ret_mm2s_device, mm2s_operations, _) = mm2s_running_operation
                .completed(timeout.clone())
                .await
                .unwrap();
            let (ret_s2mm_device, s2mm_operations, s2mm_dma_data) =
                s2mm_running_operation.completed(timeout).await.unwrap();

            s2mm_device = ret_s2mm_device;
            mm2s_device = ret_mm2s_device;

            let _ = mm2s_operations.check_and_refresh().unwrap();
            let s2mm_operations = s2mm_operations.check_and_refresh().unwrap();

            // We check both the expected data to the correct slice and the
            // full data against the expected data. That way we can check
            // both the returned block and the full dma memory pool.
            let expected_slicer: Range<usize> = s2mm_operations.containing_memory().into();

            // Check the data
            assert_eq!(
                *s2mm_dma_data,
                *expected_s2mm_mem.get(expected_slicer).unwrap()
            );

            drop(s2mm_dma_data);

            // Also check the whole memory is as expected.
            let s2mm_dma_data = s2mm_device.get_memory(0, s2mm_dma_data_size).unwrap();
            assert_eq!(*s2mm_dma_data, *expected_s2mm_mem);
        }
    }

    /// As long as the transfer parameters described above are met, it should be
    /// possible to perform a transfer on any of the 16 channels available in the
    /// multi channel DMA engine with any combination of arguments (exluding
    /// n_blocks and block_size) for both MM2S and S2MM transfers. For the
    /// loopback PL that these tests require, n_blocks and block_size must be
    /// equal for both MM2S and S2MM transfers.
    #[tokio::test]
    #[serial]
    async fn test_mc_each_channel_dma_transfer() {
        let s2mm_device_path: PathBuf = ["/dev", "axi_dma_mc_s2mm"].iter().collect();
        let mm2s_device_path: PathBuf = ["/dev", "axi_dma_mc_mm2s"].iter().collect();

        // Create the devices. In the loopback PL the MM2S DMA engine stream
        // output is connected to the S2MM DMA engine stream input.
        let mut s2mm_device = multi_channel::Device::new(&s2mm_device_path, BUS_DATA_WIDTH).unwrap();
        let mut mm2s_device = multi_channel::Device::new(&mm2s_device_path, BUS_DATA_WIDTH).unwrap();

        let s2mm_dma_data_size = s2mm_device.memory_pool_size();

        for channel in 0..MAX_CHANNELS {
            // Try every channel

            // Generate random parameters for the transfer
            let (
                n_blocks,
                block_size,
                mm2s_block_stride,
                mm2s_offset,
                s2mm_block_stride,
                s2mm_offset,
            ) = gen_random_valid_parameters(&mm2s_device, &s2mm_device);

            // Write random data into the MM2S memory and based on the parameters
            // get the expected S2MM memory
            let (expected_s2mm_mem, set_mm2s_mem) = gen_and_write_random_data(
                &mut mm2s_device,
                &mut s2mm_device,
                &block_size,
                &mm2s_block_stride,
                &n_blocks,
                &mm2s_offset,
                &block_size,
                &s2mm_block_stride,
                &n_blocks,
                &s2mm_offset,
            );

            let mm2s_operation = mm2s_device
                .new_regular_blocks_operation(block_size, mm2s_block_stride, n_blocks, mm2s_offset)
                .unwrap();
            let s2mm_operation = s2mm_device
                .new_regular_blocks_operation(block_size, s2mm_block_stride, n_blocks, s2mm_offset)
                .unwrap();

            // Create the operations hashmaps
            let mut mm2s_operations = HashMap::new();
            let mut s2mm_operations = HashMap::new();

            // Add the operations to the operations hash map
            s2mm_operations.insert(Channel::new(channel).unwrap(), s2mm_operation);
            mm2s_operations.insert(Channel::new(channel).unwrap(), mm2s_operation);

            let s2mm_operations = MultiChannelOperation::new(s2mm_operations).unwrap();
            let mm2s_operations = MultiChannelOperation::new(mm2s_operations).unwrap();

            let mm2s_slicer: Range<usize> = mm2s_operations.containing_memory().into();

            let mm2s_operations_with_mem = mm2s_operations.claim_memory(&mut mm2s_device).unwrap();

            assert!(*mm2s_operations_with_mem == *set_mm2s_mem.get(mm2s_slicer).unwrap());

            // Trigger the transfer
            let s2mm_running_operation = s2mm_device.do_dma(s2mm_operations).unwrap();
            let mm2s_running_operation = mm2s_device
                .do_dma_with_mem(mm2s_operations_with_mem)
                .unwrap();

            let timeout = time::Duration::from_millis(1000);

            // Wait for DMA transfers to complete
            let (ret_mm2s_device, mm2s_operations, _) = mm2s_running_operation
                .completed(timeout.clone())
                .await
                .unwrap();
            let (ret_s2mm_device, s2mm_operations, s2mm_dma_data) =
                s2mm_running_operation.completed(timeout).await.unwrap();

            s2mm_device = ret_s2mm_device;
            mm2s_device = ret_mm2s_device;

            let _ = mm2s_operations.check_and_refresh().unwrap();
            let s2mm_operations = s2mm_operations.check_and_refresh().unwrap();

            // We check both the expected data to the correct slice and the
            // full data against the expected data. That way we can check
            // both the returned block and the full dma memory pool.
            let expected_slicer: Range<usize> = s2mm_operations.containing_memory().into();

            // Check the data
            assert_eq!(
                *s2mm_dma_data,
                *expected_s2mm_mem.get(expected_slicer).unwrap()
            );

            drop(s2mm_dma_data);

            // Also check the whole memory is as expected.
            let s2mm_dma_data = s2mm_device.get_memory(0, s2mm_dma_data_size).unwrap();
            assert_eq!(*s2mm_dma_data, *expected_s2mm_mem);
        }
    }

    /// As long as the transfer parameters described above are met, it should be
    /// possible to perform a transfer on all of the 16 channels available in the
    /// multi channel DMA engine at the same time.
    ///
    /// It should be possible to reuse the same operation multiple times.
    ///
    /// It should be possible to alternate between those operations.
    #[tokio::test]
    #[serial]
    async fn test_mc_all_channels_dma_transfer() {
        // This test creates 2 groups of 16 MM2S operations and 2 groups of 16
        // S2MM operations. It alternates between the groups and performs
        // transfers on them.
        //
        // This test divides the memory in half (one for each group of operations)
        //
        // It then sub divides those half memories into smaller sections so that
        // each channel can write its block to memory in an interleaved manner.

        let n_groups = 2;

        let s2mm_device_path: PathBuf = ["/dev", "axi_dma_mc_s2mm"].iter().collect();
        let mm2s_device_path: PathBuf = ["/dev", "axi_dma_mc_mm2s"].iter().collect();

        // Create the devices. In the loopback PL the MM2S DMA engine stream
        // output is connected to the S2MM DMA engine stream input.
        let mut s2mm_device = multi_channel::Device::new(&s2mm_device_path, BUS_DATA_WIDTH).unwrap();
        let mut mm2s_device = multi_channel::Device::new(&mm2s_device_path, BUS_DATA_WIDTH).unwrap();

        let mm2s_dma_data_size = mm2s_device.memory_pool_size();
        let s2mm_dma_data_size = s2mm_device.memory_pool_size();

        let mut rng = SmallRng::from_entropy();

        // Check that the S2MM device and the MM2S device are similar
        assert!(s2mm_dma_data_size == mm2s_dma_data_size);

        // Set the number of blocks per channel
        let n_blocks = rng.gen_range(1..8);

        // Calculate the maximum block stride. We need two sets of all the
        // channels
        let max_channel_stride = s2mm_dma_data_size / (n_blocks * n_groups * MAX_CHANNELS as usize);

        // Generate a random value for the channel stride
        let channel_stride = 8 * rng.gen_range(1..max_channel_stride / 8 + 1);

        // Calculate the block stride that puts the next block beyond all of
        // channel strides
        let block_stride = channel_stride * MAX_CHANNELS as usize;
        // Generate a random value for the block size
        let block_size = 8 * rng.gen_range(1..channel_stride / 8 + 1);
        let base_offset = 8 * rng.gen_range(0..(max_channel_stride - channel_stride) / 8 + 1);

        // Create the operations hashmaps
        let mut mm2s_operations_0 = HashMap::new();
        let mut s2mm_operations_0 = HashMap::new();
        let mut mm2s_operations_1 = HashMap::new();
        let mut s2mm_operations_1 = HashMap::new();

        for channel_n in 0..MAX_CHANNELS {
            // Calculate the offsets. Each channel needs to be offset relative to
            // the offset of channel 0. Also the second group of operations needs
            // to be offset by half the memory space.
            let offset_0 = base_offset + channel_stride * channel_n as usize;
            let offset_1 =
                base_offset + channel_stride * channel_n as usize + s2mm_dma_data_size / n_groups;

            // Create the first group of operations
            let mm2s_operation_0 = mm2s_device
                .new_regular_blocks_operation(block_size, block_stride, n_blocks, offset_0)
                .unwrap();
            let s2mm_operation_0 = s2mm_device
                .new_regular_blocks_operation(block_size, block_stride, n_blocks, offset_0)
                .unwrap();

            // Create the second group of operations
            let mm2s_operation_1 = mm2s_device
                .new_regular_blocks_operation(block_size, block_stride, n_blocks, offset_1)
                .unwrap();
            let s2mm_operation_1 = s2mm_device
                .new_regular_blocks_operation(block_size, block_stride, n_blocks, offset_1)
                .unwrap();

            let channel = Channel::new(channel_n).unwrap();

            s2mm_operations_0.insert(channel, s2mm_operation_0);
            s2mm_operations_1.insert(channel, s2mm_operation_1);
            mm2s_operations_0.insert(channel, mm2s_operation_0);
            mm2s_operations_1.insert(channel, mm2s_operation_1);
        }

        let mut s2mm_operations_0 = MultiChannelOperation::new(s2mm_operations_0).unwrap();
        let mut mm2s_operations_0 = MultiChannelOperation::new(mm2s_operations_0).unwrap();

        let mut s2mm_operations_1 = MultiChannelOperation::new(s2mm_operations_1).unwrap();
        let mut mm2s_operations_1 = MultiChannelOperation::new(mm2s_operations_1).unwrap();

        let mut expected_data_0 = vec![0; s2mm_dma_data_size];
        let mut expected_data_1 = vec![0; s2mm_dma_data_size];

        for group in 0..n_groups {
            // Iterate over the groups of operations to write the data to the MM2S
            // memory

            let group_offset = group * s2mm_dma_data_size / n_groups;

            for n in 0..MAX_CHANNELS {
                // Iterate over the channels to write the data into the correct
                // locations in the MM2S memory for each channel

                let channel_offset = n as usize * channel_stride;

                for block_num in 0..n_blocks {
                    // Generate the test data
                    let block_data = vec![n + 1 + MAX_CHANNELS * group as u8; block_size];

                    // Get the offset of the block
                    let block_offset =
                        base_offset + block_num * block_stride + channel_offset + group_offset;

                    if group == 0 {
                        // Copy the block data into the correct location in the
                        // expected data vector
                        expected_data_0[block_offset..block_offset + block_size]
                            .copy_from_slice(&block_data);
                    } else {
                        expected_data_1[block_offset..block_offset + block_size]
                            .copy_from_slice(&block_data);
                    }

                    {
                        let mut mm2s_dma_data =
                            mm2s_device.get_memory(block_offset, block_size).unwrap();

                        mm2s_dma_data.copy_from_slice(&block_data);
                    }
                }
            }
        }

        for n in 0..8 {
            // Create a vector into which we can copy the data out of the
            // destination memory
            let destination_data = vec![0u8; s2mm_dma_data_size];

            {
                let mut s2mm_dma_data = s2mm_device.get_memory(0, s2mm_dma_data_size).unwrap();
                s2mm_dma_data.copy_from_slice(&destination_data);
            }

            let timeout = time::Duration::from_millis(1000);

            // Trigger the transfer
            if n % n_groups == 0 {
                let s2mm_running_operation = s2mm_device.do_dma(s2mm_operations_0).unwrap();
                let mm2s_running_operation = mm2s_device.do_dma(mm2s_operations_0).unwrap();

                // Wait for DMA transfers to complete
                let (ret_mm2s_device, stale_mm2s_operations, _) = mm2s_running_operation
                    .completed(timeout.clone())
                    .await
                    .unwrap();
                let (ret_s2mm_device, stale_s2mm_operations, s2mm_dma_data) =
                    s2mm_running_operation
                        .completed(timeout.clone())
                        .await
                        .unwrap();

                s2mm_operations_0 = stale_s2mm_operations.check_and_refresh().unwrap();
                mm2s_operations_0 = stale_mm2s_operations.check_and_refresh().unwrap();
                s2mm_device = ret_s2mm_device;
                mm2s_device = ret_mm2s_device;

                let expected_slicer: Range<usize> = s2mm_operations_0.containing_memory().into();

                assert_eq!(
                    *s2mm_dma_data,
                    *expected_data_0.get(expected_slicer).unwrap()
                );
            } else {
                let s2mm_running_operation = s2mm_device.do_dma(s2mm_operations_1).unwrap();
                let mm2s_running_operation = mm2s_device.do_dma(mm2s_operations_1).unwrap();

                // Wait for DMA transfers to complete
                let (ret_mm2s_device, stale_mm2s_operations, _) = mm2s_running_operation
                    .completed(timeout.clone())
                    .await
                    .unwrap();
                let (ret_s2mm_device, stale_s2mm_operations, s2mm_dma_data) =
                    s2mm_running_operation
                        .completed(timeout.clone())
                        .await
                        .unwrap();

                s2mm_operations_1 = stale_s2mm_operations.check_and_refresh().unwrap();
                mm2s_operations_1 = stale_mm2s_operations.check_and_refresh().unwrap();
                s2mm_device = ret_s2mm_device;
                mm2s_device = ret_mm2s_device;

                let expected_slicer: Range<usize> = s2mm_operations_1.containing_memory().into();

                assert_eq!(
                    *s2mm_dma_data,
                    *expected_data_1.get(expected_slicer).unwrap()
                );
            };

            // Also check the full array
            let s2mm_dma_data = s2mm_device.get_memory(0, s2mm_dma_data_size).unwrap();

            if n % n_groups == 0 {
                assert_eq!(*s2mm_dma_data, *expected_data_0);
            } else {
                assert_eq!(*s2mm_dma_data, *expected_data_1);
            }
        }
    }

    /// As long as the transfer parameters described above are met, it should be
    /// possible to perform a transfer on any subset of the 16 channels available
    /// in the multi channel DMA engine at the same time.
    ///
    /// It should be possible to reuse the same operations multiple times.
    #[tokio::test]
    #[serial]
    async fn test_mc_random_subsets_of_channels() {
        // This test creates random operations for all 16 channels and then
        // selects random sets of them to run.

        let s2mm_device_path: PathBuf = ["/dev", "axi_dma_mc_s2mm"].iter().collect();
        let mm2s_device_path: PathBuf = ["/dev", "axi_dma_mc_mm2s"].iter().collect();

        // Create the devices. In the loopback PL the MM2S DMA engine stream
        // output is connected to the S2MM DMA engine stream input.
        let mut s2mm_device = multi_channel::Device::new(&s2mm_device_path, BUS_DATA_WIDTH).unwrap();
        let mut mm2s_device = multi_channel::Device::new(&mm2s_device_path, BUS_DATA_WIDTH).unwrap();

        let mm2s_dma_data_size = mm2s_device.memory_pool_size();
        let s2mm_dma_data_size = s2mm_device.memory_pool_size();

        // Check that the S2MM device and the MM2S device are similar
        assert!(s2mm_dma_data_size == mm2s_dma_data_size);

        let mut rng = SmallRng::from_entropy();

        // Create vectors to contain all of the operations
        let mut mm2s_operations_store = HashMap::new();
        let mut s2mm_operations_store = HashMap::new();

        // Work out how much memory each channel can have access to
        let memory_per_channel = s2mm_dma_data_size / (MAX_CHANNELS as usize);

        // Make sure the memory per channel is aligned with 8.
        let memory_per_channel = memory_per_channel - memory_per_channel % 8;

        let mut all_channels_expected_data: Vec<u8> = vec![0; s2mm_dma_data_size];

        for channel in 0..MAX_CHANNELS {
            // Set the number of blocks per channel
            let n_blocks = rng.gen_range(1..8);

            // Calculate the maximum block stride.
            let max_block_stride = memory_per_channel / n_blocks;

            // Generate a random value for the block stride
            let block_stride = 8 * rng.gen_range(1..max_block_stride / 8 + 1);

            // Generate a random value for the block size
            let block_size = 8 * rng.gen_range(1..block_stride / 8 + 1);
            // Generate a random offset which is within the channel memory
            let offset = 8 * rng.gen_range(0..(max_block_stride - block_stride) / 8 + 1)
                + memory_per_channel * channel as usize;

            // Store the operations
            dbg!(&memory_per_channel);
            mm2s_operations_store.insert(
                Channel::new(channel).unwrap(),
                mm2s_device
                    .new_regular_blocks_operation(block_size, block_stride, n_blocks, offset)
                    .unwrap(),
            );
            s2mm_operations_store.insert(
                Channel::new(channel).unwrap(),
                s2mm_device
                    .new_regular_blocks_operation(block_size, block_stride, n_blocks, offset)
                    .unwrap(),
            );

            for block_num in 0..n_blocks {
                // Generate the test data
                let block_data: Vec<u8> =
                    vec![block_num as u8 + 1 + channel * MAX_CHANNELS; block_size];

                // Get the offset of the block
                let block_offset = offset + block_num * block_stride;

                // Copy the block data into the correct location in the
                // expected data vector. This is the expected data if all channels
                // are run
                all_channels_expected_data[block_offset..block_offset + block_size]
                    .copy_from_slice(&block_data);

                {
                    let mut mm2s_dma_data =
                        mm2s_device.get_memory(block_offset, block_size).unwrap();
                    mm2s_dma_data.copy_from_slice(&block_data);
                }
            }
        }

        for _n in 0..8 {
            // Create a vector into which we can copy the data out of the
            // destination memory
            let destination_data: Vec<u8> = vec![0; s2mm_dma_data_size];

            {
                let mut s2mm_dma_data = s2mm_device.get_memory(0, s2mm_dma_data_size).unwrap();
                s2mm_dma_data.copy_from_slice(&destination_data);
            }

            // Randomly pick how many channels will be involved in the transfer
            let n_transfer_channels = rng.gen_range(1..MAX_CHANNELS);

            // Randomly select the channels to run.
            let mut channels: Vec<Channel> = Vec::new();

            // Populate the channels vector with random and unique channel numbers
            for _n in 0..n_transfer_channels {
                loop {
                    // Loop and randomly select a channel numbers
                    let channel_n = rng.gen_range(1..MAX_CHANNELS) as u8;
                    let channel = Channel::new(channel_n).unwrap();

                    if !channels.contains(&channel) {
                        // If the randomly selected channel number is not in
                        // channels then add it.
                        channels.push(channel);
                        break;
                    }
                }
            }

            // Create the operations hashmaps
            let mut mm2s_operations = HashMap::new();
            let mut s2mm_operations = HashMap::new();

            // Create a vector to contain the expected data based on the channels
            // which are running
            let mut expected_data: Vec<u8> = vec![0; s2mm_dma_data_size];

            // Populate the operation hashmaps
            for channel in &channels {
                s2mm_operations.insert(*channel, s2mm_operations_store.remove(channel).unwrap());
                mm2s_operations.insert(*channel, mm2s_operations_store.remove(channel).unwrap());

                let channel_num: usize = (*channel).into();

                let lower_index = memory_per_channel * channel_num;
                let upper_index = memory_per_channel * (1 + channel_num);

                // Copy the expected data for the channels that are running into
                // the expected data vector
                expected_data[lower_index..upper_index]
                    .copy_from_slice(&all_channels_expected_data[lower_index..upper_index]);
            }

            let s2mm_operations = MultiChannelOperation::new(s2mm_operations).unwrap();
            let mm2s_operations = MultiChannelOperation::new(mm2s_operations).unwrap();

            // Trigger the transfer
            let s2mm_running_operation = s2mm_device.do_dma(s2mm_operations).unwrap();
            let mm2s_running_operation = mm2s_device.do_dma(mm2s_operations).unwrap();

            let timeout = time::Duration::from_millis(1000);

            // Wait for DMA transfers to complete
            let (ret_mm2s_device, mm2s_operations, _) = mm2s_running_operation
                .completed(timeout.clone())
                .await
                .unwrap();
            let (ret_s2mm_device, s2mm_operations, s2mm_dma_data) = s2mm_running_operation
                .completed(timeout.clone())
                .await
                .unwrap();

            s2mm_device = ret_s2mm_device;
            mm2s_device = ret_mm2s_device;

            let s2mm_operations = s2mm_operations.check_and_refresh().unwrap();
            let mm2s_operations = mm2s_operations.check_and_refresh().unwrap();

            // We check both the expected data to the correct slice and the
            // full data against the expected data. That way we can check
            // both the returned block and the full dma memory pool.
            let expected_slicer: Range<usize> = s2mm_operations.containing_memory().into();

            assert_eq!(*s2mm_dma_data, *expected_data.get(expected_slicer).unwrap());

            drop(s2mm_dma_data);

            // Also check the full array
            let s2mm_dma_data = s2mm_device.get_memory(0, s2mm_dma_data_size).unwrap();

            assert_eq!(*s2mm_dma_data, *expected_data);

            // Now we've completed this test iteration, recover the operations
            // so they can be used again
            s2mm_operations_store.extend(s2mm_operations.into_iter());
            mm2s_operations_store.extend(mm2s_operations.into_iter());
        }
    }
}
