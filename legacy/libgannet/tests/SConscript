Import('test_build_env')

import distutils.sysconfig, os
import numpy
import shutil

dir_path = Dir('.').abspath
source_dir = 'py_axi_dma_wrapper'

cython_exe = 'cython'
if shutil.which(cython_exe) is None:
    cython_exe = 'cython3'
    if shutil.which(cython_exe) is None:
        raise EnvironmentError('No cython executable can be found.')


cython_builder = Builder(
    action='%s $SOURCE -o $TARGET' % cython_exe, suffix='.c',
    src_suffix='.pyx')

vars = distutils.sysconfig.get_config_vars('CC', 'CXX', 'OPT',
        'BASECFLAGS', 'CCSHARED', 'LDSHARED', 'SO')
(cc, cxx, opt, basecflags, ccshared, ldshared, so_ext) = vars

for i in range(len(vars)):
    if vars[i] is None:
        vars[i] = ""

cython_env = test_build_env.Clone(
    CC=cc, SHLINK=ldshared, SHLINKFLAGS=[], SHLIBSUFFIX=so_ext,
    SHLIBPREFIX='', CPPFLAGS=basecflags + " " + opt)

cython_env['BUILDERS']['Cythonize'] = cython_builder

python_include_paths = [distutils.sysconfig.get_python_inc(),
        numpy.get_include()]

cython_env.AppendUnique(CPPPATH=python_include_paths)

pxd_file = os.path.join(source_dir, 'axi_dma.pxd')
pyx_file = os.path.join(source_dir, 'axi_dma.pyx')

target_c_file = cython_env.Cythonize(pyx_file)
Depends(target_c_file, pxd_file)

built_module = cython_env.SharedLibrary(target_c_file)

cython_modules = [built_module]

# Setup the test runner
def test_runner(source, target, env):
    from unittest import TestLoader, TextTestRunner
    import sys

    # We need to generate the test suite given the function
    # names that have been created.
    test_path = os.path.realpath(dir_path)

    if test_path not in sys.path:
        sys.path.append(test_path)

    # pop off the added path
    sys.path.pop()

    tests = TestLoader().discover(os.path.abspath('test'))
    TextTestRunner(verbosity=2).run(tests)

test_alias = Alias('test', action=test_runner)
cython_alias = Alias('build_test', cython_modules)
Depends(test_alias, cython_modules)
AlwaysBuild(test_alias)

# Setup the timer
def timer_runner(source, target, env):

    import sys

    timer_path = os.path.realpath(dir_path)

    if timer_path not in sys.path:
        sys.path.append(timer_path)

    import timer
    from pretty_print_times import pretty_print_times, colour

    lengths, times, rates = timer.time_dma_transfers()

    col_labels = [str(each) for each in lengths]
    print(colour('\nTransfer times\n', 'red'))
    pretty_print_times(times, col_labels, ['Times'],
                       highlight='min')
    print(colour('\nTransfer rates\n', 'red'))
    pretty_print_times(rates, col_labels, ['Rates'],
                       highlight='max')

    # pop off the added path.
    sys.path.pop()

timer_alias = Alias('timer', action=timer_runner)
Depends(timer_alias, cython_modules)
AlwaysBuild(timer_alias)

Return('cython_modules')
