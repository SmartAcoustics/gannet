
import numpy as np
import os

timeit_args = []

def time_dma_transfers():
    import timeit

    def make_setup_script(nbytes):

        global timeit_args

        del timeit_args[:]

        timeit_args += [device_path_mm2s, device_path_s2mm, 
                        offset, nbytes, loops]

        script = 'import ' + __name__ + ' as module;'
        script += ('from py_axi_dma_wrapper import axi_dma;')
        script += 'timeit_args = tuple(module.timeit_args);'
        script += 'function = axi_dma.multiple_dma_transfers(*timeit_args)'

        return script


    lengths = tuple(2**np.arange(3, 23, 2)) + (2**23 - 8,)

    times = np.zeros((1, len(lengths)))
    rates = np.zeros((1, len(lengths)))
    loops = 100

    device_path_mm2s = "/dev/axi_dma_mm2s"
    device_path_s2mm = "/dev/axi_dma_s2mm"

    offset = 0

    for l, each_length in enumerate(lengths):

        time = min(timeit.repeat('function()', 
            setup=make_setup_script(each_length), 
            repeat=3, number=1))

        times[0, l] = time/loops
        rates[0, l] = each_length/times[0, l]

    return lengths, times, rates


