#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <error.h>
#include "gannet.h"

int main() {

    dma_device_t *mm2s_device = gannet_device_init("/dev/axi_dma_mm2s");
    dma_device_t *s2mm_device = gannet_device_init("/dev/axi_dma_s2mm");

    uint8_t* mem_mm2s = gannet_get_memory(mm2s_device);
    uint8_t* mem_s2mm = gannet_get_memory(s2mm_device);

    for(int i = 0; i<(1<<24); ++i){
        mem_mm2s[i] = i % (sizeof(*mem_mm2s) * 8);
        mem_s2mm[i] = i % (sizeof(*mem_s2mm) * 8);
    }

    gannet_device_cleanup(mm2s_device);
    gannet_device_cleanup(s2mm_device);
}

