#ifndef dma_H_
#define dma_H_

#include <stdint.h>
#include <stdbool.h>
#include <pthread.h>

/*
 * In the LogiCORE IP AXI DMA block the configuration registers are 32 bits
 * wide. Each 32 bit configuration register includes **four individually
 * addressable** (8 bit wide) memory locations. In Libgannet, the
 * configuration registers are treated as 32 bits wide therefore:
 * Libgannet address | AXI DMA address      | Register
 * ----------------- | -------------------- | ------------------------------
 * 0                 | 0x00 0               | MM2S Control (MM2S_DMACR)
 * 1                 | 0x04 4               | MM2S Status (MM2S_DMASR)
 * 2                 | 0x08 8               | (SG) MM2S Current descriptor pointer LSW (MM2S_CURDESC)
 * 3                 | 0x0C 12              | (SG) MM2S Current descriptor pointer MSW (MM2S_CURDESC_MSB)
 * 4                 | 0x10 16              | (SG) MM2S Tail descriptor pointer LSW (MM2S_TAILDESC)
 * 5                 | 0x14 20              | (SG) MM2S Tail descriptor pointer MSW (MM2S_TAILDESC_MSB)
 * 6                 | 0x18 24              | (Direct) MM2S Start address LSW (MM2S_SA)
 * 7                 | 0x1C 28              | (Direct) MM2S Start address MSW (MM2S_SA_MSB)
 * 8                 | 0x20 32              | -
 * 9                 | 0x24 36              | -
 * 10                | 0x28 40              | (Direct) MM2S Transfer Length (MM2S_LENGTH)
 * 11                | 0x2C 44              | -
 * 12                | 0x30 48              | S2MM Control (S2MM_DMACR)
 * 13                | 0x34 52              | S2MM Status (S2MM_DMASR)
 * 14                | 0x38 56              | (SG) S2MM Current descriptor pointer LSW (S2MM_CURDESC)
 * 15                | 0x3C 60              | (SG) S2MM Current descriptor pointer MSW (S2MM_CURDESC_MSB)
 * 16                | 0x40 64              | (SG) S2MM Tail descriptor pointer LSW (S2MM_TAILDESC)
 * 17                | 0x44 68              | (SG) S2MM Tail descriptor pointer MSW (S2MM_TAILDESC_MSB)
 * 18                | 0x48 72              | (Direct) S2MM Destination address LSW (S2MM_DA)
 * 19                | 0x4C 76              | (Direct) S2MM Destination address MSW (S2MM_DA_MSB)
 * 20                | 0x50 80              | -
 * 21                | 0x54 84              | -
 * 22                | 0x58 88              | (Direct) S2MM Transfer Length (S2MM_LENGTH)
 */
typedef enum {
    MM2S_CONTROL_REGISTER=0,            // MM2S_DMACR
    MM2S_STATUS_REGISTER=1,             // MM2S_DMASR
    MM2S_SG_CURRENT_DESCRIPTOR_LSW=2,   // MM2S_CURDESC
    MM2S_SG_CURRENT_DESCRIPTOR_MSW=3,   // MM2S_CURDESC_MSB
    MM2S_SG_TAIL_DESCRIPTOR_LSW=4,      // MM2S_TAILDESC
    MM2S_SG_TAIL_DESCRIPTOR_MSW=5,      // MM2S_TAILDESC_MSB
    MM2S_START_ADDRESS_LSW=6,           // MM2S_SA
    MM2S_START_ADDRESS_MSW=7,           // MM2S_SA_MSB
    MM2S_LENGTH=10,                     // MM2S_LENGTH
    S2MM_CONTROL_REGISTER=12,           // S2MM_DMACR
    S2MM_STATUS_REGISTER=13,            // S2MM_DMASR
    S2MM_SG_CURRENT_DESCRIPTOR_LSW=14,  // S2MM_CURDESC
    S2MM_SG_CURRENT_DESCRIPTOR_MSW=15,  // S2MM_CURDESC_MSB
    S2MM_SG_TAIL_DESCRIPTOR_LSW=16,     // S2MM_TAILDESC
    S2MM_SG_TAIL_DESCRIPTOR_MSW=17,     // S2MM_TAILDESC_MSB
    S2MM_DEST_ADDRESS_LSW=18,           // S2MM_SA
    S2MM_DEST_ADDRESS_MSW=19,           // S2MM_SA_MSB
    S2MM_LENGTH=22,                     // S2MM_LENGTH
} dma_register;

typedef enum {
    MM2S_ENABLED=0,
    S2MM_ENABLED=1,
    MM2S_S2MM_ENABLED=2,
} dma_capability;

typedef enum {
    DMACR_RUN_STOP = 1<<0,         // RW Write 1 to trigger a run, 0 to stop.
    DMACR_REGISTER_EXISTS = 1<<1,  // RO Always reads 1 if the register exists
    DMACR_RESET = 1<<2,            // RW 1 to trigger a soft reset.
    DMACR_KEYHOLE = 1<<3,          // RW 1 enables keyhole read
    DMACR_CYCLIC_BD = 1<<4,        // RW 1 enables cyclic BD
    DMACR_IOC_IrqEn = 1<<12,       // RW 1 enables interrupt on complete.
    DMACR_Dly_IrqEn = 1<<13,       // RW 1 enables interrupt on delay timer (SG only)
    DMACR_Err_IrqEn = 1<<14,       // RW 1 enables interrupt on error
} control_register_bitmasks;

typedef enum {
    DMASR_HALTED = 1<<0,           // RO 1 if DMACR_RUN_STOP and operations halted.
    DMASR_IDLE = 1<<1,             // RO 1 if DMA controller is paused
    DMASR_SGIncId = 1<<3,          // RO 1 if scatte gather enabled
    DMASR_DMAIntErr = 1<<4,        // RO 1 if DMA internal error
    DMASR_DMASlvErr = 1<<5,        // RO 1 if DMA slave error
    DMASR_DMADecErr = 1<<6,        // RO 1 if DMA decode error
    DMASR_SGIntErr = 1<<8,         // RO 1 if scatter gather internal error
    DMASR_SGSlvErr = 1<<9,         // RO 1 if scatter gather slave error
    DMASR_SGDecErr = 1<<10,        // RO 1 if scatter gather decode error
    DMASR_IOC_Irq = 1<<12,         // R/WC 1 if IOC interrupt event happened. Write 1 to clear
    DMASR_Dly_Irq = 1<<13,         // R/WC 1 if delay interrupt event happened. Write 1 to clear
    DMASR_Err_Irq = 1<<14,         // R/WC 1 if error interrupt event happened. Write 1 to clear
} status_register_bitmasks;

typedef enum {
    SG_DESC_MM2S_CONTROL_TXEOF = 1<<26,
    SG_DESC_MM2S_CONTROL_TXSOF = 1<<27,
} sg_descriptor_mm2s_control_bitmasks;

typedef enum {
    SG_DESC_S2MM_CONTROL_RXEOF = 1<<26,
    SG_DESC_S2MM_CONTROL_RXSOF = 1<<27,
} sg_descriptor_s2mm_control_bitmasks;


//! An enumeration of the DMA transfer direction.
/*!
 * Enumeration   | Meaning
 * ------------- | -------------------
 * MM2S_TRANSFER | From memory into PL
 * S2MM_TRANSFER | From PL into memory
 */
typedef enum {
    MM2S_TRANSFER=0,
    S2MM_TRANSFER=1,
} dma_direction;

//! Each DMA device is represented by an instantiation of this structure.
/*!
 * This structure includes pointers to and sizes of the configuration register
 * space, and the device memory (into which it can DMA). It provides the
 * physical address of the device memory as the DMA controller must use this
 * rather than the virtual address used by libgannet. It includes the
 * file descriptor pointing to the device and its capability (whether it can
 * do MM2S, S2MM or both).
 * The mutexes are included to enforce a memory fence (defined architecturally
 * to be the case on a mutex on ARM) before register read/writes.
 */
struct dma_device_s{
    volatile uint32_t *cfg;
    size_t cfg_size;
    void *cma; // Relying on ACP port to handle cache coherency
    size_t cma_size;
    uintptr_t cma_phys_addr;
    dma_capability dma_device_capability;
    int fd;
    pthread_mutex_t mm2s_cfg_mutex;
    pthread_mutex_t s2mm_cfg_mutex;
};
typedef struct dma_device_s dma_device_t;

struct dma_sg_descriptor_s {
    uint32_t next_descriptor_ptr;     // NXTDESC
    uint32_t next_descriptor_ptr_msb; // NXTDESC_MSB
    uint32_t buffer_address;          // BUFFER_ADDRESS
    uint32_t buffer_address_msb;      // BUFFER_ADDRESS_MSB
    uint32_t reserved_1;
    uint32_t reserved_2;
    uint32_t control;                 // CONTROL
    uint32_t status;                  // STATUS
    uint32_t user_application_1;      // APP1
    uint32_t user_application_2;      // APP1
    uint32_t user_application_3;      // APP1
    uint32_t user_application_4;      // APP1
};

struct dma_scatter_gather_op_s {
    bool valid;
    dma_direction direction;
    struct dma_sg_descriptor_s *sg_descriptors;
    size_t n_sg_descriptors;
    void *mem_ptr; // Pointer to the memory base address
    void *buffer_start; // Pointer to the first location the DMA writes to.
    uintptr_t mem_lower_bound; // Inclusive lower bound of the DMA memory space (including the descriptors)
    uintptr_t mem_upper_bound; // Exclusive upper bound of the DMA memory space.
};

typedef struct dma_scatter_gather_op_s dma_scatter_gather_op_t;

#endif //Header guard
