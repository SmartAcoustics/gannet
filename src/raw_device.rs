use memmap2::MmapMut;
use std::marker::PhantomData;
use std::ptr::{read_volatile, write_volatile};
use std::sync::atomic::{fence, Ordering};

use crate::errors::DeviceError;
use crate::mem_setup::RegisterLayout;

/// A simple wrapper around a memory map, protecting the reads and writes with
/// address constraints and memory fences.
#[derive(Debug)]
pub(crate) struct RawDevice<R: RegisterLayout> {
    registers_mem: MmapMut,
    r_marker: PhantomData<R>,
}

impl<R> RawDevice<R>
where
    R: RegisterLayout,
{
    pub(crate) fn new(registers_mem: MmapMut) -> Result<RawDevice<R>, DeviceError> {
        Ok(RawDevice {
            registers_mem,
            r_marker: PhantomData,
        })
    }

    /// Unwraps the MmapMut, taking ownership of it. This destructs
    /// RawDevice.
    pub(crate) fn unwrap(self) -> MmapMut {
        let RawDevice {
            registers_mem,
            r_marker: _,
        } = self;

        registers_mem
    }

    #[inline]
    pub(crate) fn read_reg(&self, register: R) -> u32 {
        let offset = register.offset();
        let register_ptr = self.registers_mem.as_ptr() as *mut u32;

        // Need the memory fences here to prevent the cpu from reordering
        // the non volatile reads/writes around volatile reads/writes
        //
        // Critically, ordering is important independently of actual address
        // since in the memory mapped instance writing to one memory location
        // could have behind the scenes side effects on a different memory
        // location. For this reason we use SeqCst to enforce a strong ordering
        // constraint.
        fence(Ordering::SeqCst);

        let reg_val: u32 = unsafe { read_volatile(register_ptr.add(offset)) };

        fence(Ordering::SeqCst);

        reg_val
    }

    #[inline]
    pub(crate) fn write_reg(&self, register: R, val: u32) {
        let offset = register.offset();
        let register_ptr = self.registers_mem.as_ptr() as *mut u32;

        // Need the memory fences here to prevent the cpu from reordering
        // the non volatile reads/writes around volatile reads/writes
        //
        // Critically, ordering is important independently of actual address
        // since in the memory mapped instance writing to one memory location
        // could have behind the scenes side effects on a different memory
        // location. For this reason we use SeqCst to enforce a strong ordering
        // constraint.
        fence(Ordering::SeqCst);

        unsafe {
            // Write to the register
            write_volatile(register_ptr.add(offset), val);
        }

        fence(Ordering::SeqCst);
    }
}
