pub mod errors;
pub mod mem_pool;
pub mod multi_channel;
pub mod operation;
pub mod uni_channel;

mod descriptors;
mod device_core;
mod mem_setup;
mod raw_device;
mod scatter_gather;
mod utils;

pub use descriptors::DescriptorError;
pub use errors::*;
pub use mem_pool::{MemBlock, MemBlockLayout, MemPool, MemPoolError};
pub use mem_setup::MemSetupError;
pub use scatter_gather::ScatterGatherError;

use mem_setup::{RegisterCheck, RegisterLayout};
pub use operation::Operation;

#[derive(Debug, Clone, Copy, PartialEq)]
#[repr(usize)]
pub enum BusDataWidth {
    OneByte = 1,
    TwoBytes = 2,
    FourBytes = 4,
    EightBytes = 8,
    SixteenBytes = 16,
    ThirtyTwoBytes = 32,
    SixtyFourBytes = 64,
    OneTwentyEightBytes = 128,
}

impl From<BusDataWidth> for usize {

    fn from(value: BusDataWidth) -> Self {
        value as usize
    }
}

#[derive(Debug, Copy, Clone)]
enum TransferType {
    MM2S,
    S2MM,
}
