use std::time;

use crate::errors::{DeviceError, DmaCoreError};

/// Encapsulates the core functionality of a DMA device.
pub(crate) trait DeviceSpecifics {
    /// Performs a DMA reset by writing to the DMA reset bit.
    fn write_reset_bit(&self);

    fn halted(&self) -> bool;
    fn idle(&self) -> bool;
    fn check_error(&self) -> Result<(), DmaCoreError>;

    fn get_and_consume_interrupts(
        &mut self,
        timeout: &time::Duration,
    ) -> Result<usize, DeviceError>;

    /// This function waits for the halted bit to be set high. It will wait up to
    /// timeout. If halted is not set high within timeout it will return a
    /// DeviceError::HaltTimedOut error.
    ///
    /// This method will spin freely until the bit is set or the timeout
    /// occurs. This is because the resolution of the timer is too coarse
    /// for the time periods being considered
    fn wait_for_halted(&self, timeout: &time::Duration) -> Result<(), DeviceError> {
        // Set the time period for the loop sleeps in microseconds
        //let delay_duration = time::Duration::from_micros(1);

        // Get the time now
        let wait_for_halted_time_check = time::Instant::now();

        loop {
            // If the device is halted then break.
            if self.halted() {
                break;
            }

            // Check if we have exceeded the acceptable length of time for
            // halting
            if wait_for_halted_time_check.elapsed() > *timeout {
                // Halting took too long so return an error
                return Err(DeviceError::HaltTimedOut);
            }

            // Pause so that we don't waste lots of energy looping
            //thread::sleep(delay_duration);
        }

        Ok(())
    }

    /// Resets the DMA engine.
    fn reset(&mut self) -> Result<(), DeviceError> {
        // Write to the device registers to reset the device
        self.write_reset_bit();

        // Wait for the status register to show the device is halted. At that
        // point, the reset has completed. If wait for halted timedout return
        // DeviceError::ResetFailed.
        match self.wait_for_halted(&time::Duration::from_millis(100)) {
            Err(DeviceError::HaltTimedOut) => return Err(DeviceError::ResetFailed),
            _ => (),
        }

        // The DMA engine could send an interrupt at the same time as we reset
        // it hence creating a race condition. If the interrupt was sent it
        // needs to be consumed so that subsequent operations work correctly.
        // By running get_and_consume_interrupts here we handle this
        // condition.
        self.get_and_consume_interrupts(&time::Duration::from_secs(0))?;

        Ok(())
    }

    /// This function waits for the idle bit to be set high. It will wait up
    /// to timeout. If idle is not set high within timeout it will return a
    /// DeviceError::TimedOut error.
    ///
    /// This method will spin freely until the bit is set or the timeout
    /// occurs. This is because the resolution of the timer is too coarse
    /// for the time periods being considered.
    fn wait_for_idle(&mut self, timeout: &time::Duration) -> Result<(), DeviceError> {
        // Set the time period for the sleeps in microseconds
        //let delay_duration = time::Duration::from_micros(4);
        // Get the time now
        let wait_for_idle_time_check = time::Instant::now();

        // We loop and keep checking if the device is idle.
        loop {
            // Check if the idle bit is set
            if self.idle() {
                break;
            }

            // Check if we have exceeded the acceptable length of time waiting
            // for idle
            if wait_for_idle_time_check.elapsed() > *timeout {
                return Err(DeviceError::TimedOut);
            }

            // Pause so that we don't waste lots of energy looping
            //thread::sleep(delay_duration);
        }

        Ok(())
    }
}
