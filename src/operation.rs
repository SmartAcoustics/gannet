use crate::descriptors::{DescriptorBlock, DescriptorError, DescriptorPool};
use crate::errors::{DeviceError, DmaCoreError};
use crate::scatter_gather::{walk_descriptor_chain_check_and_refresh, DescriptorFunctions};
use crate::{MemBlock, MemBlockLayout};
use std::sync::atomic::{AtomicU64, Ordering};
use std::{error, fmt, ops};
use thiserror::Error;

/// This provides a globally unique ID for each operation that is created.
/// The index is incremented on each read. This is fine - if a billion
/// operations are created every second, the program could run for nearly
/// 600 years before the counter wraps around.
static NEXT_OPERATION_ID: AtomicU64 = AtomicU64::new(0);

/// Error in creating or handling a Gannet operation.
#[derive(Error, Debug)]
pub enum OperationError {
    /// Error with handling the descriptors.
    #[error("Descriptor error: {source}")]
    Descriptors {
        #[from]
        source: DescriptorError,
    },
    /// An error was recorded during the scatter gather operation.
    #[error("A DMA error was recorded during the operation: {source}")]
    TransferError { source: DmaCoreError },
    /// Insufficient bytes were transferred
    #[error("Insufficient bytes were transferred: {actual} (expected {expected})")]
    InsufficientBytesTransferred { expected: usize, actual: usize },
}

/// Error whilst refreshing an Operation, which is most likely derived from
/// inspecting the desriptor chain. This error type encapsulates
/// the device that is recovered which will have been refreshed and is ready
/// for use again.
#[derive(Debug)]
pub struct OperationRefreshError {
    pub source: OperationError,
    pub refreshed_operation: Operation,
}

impl error::Error for OperationRefreshError {
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        Some(&self.source)
    }
}

impl fmt::Display for OperationRefreshError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Operation error: {}", self.source)
    }
}

pub trait MemoryFromOperation {
    /// Returns the memory associated with an operation from the device memory
    /// pool.
    ///
    /// The same rules about non-overlapping blocks apply as for [get_memory].
    ///
    /// If the operation is not from this device, a `DeviceError::WrongDevice`
    /// will be raised.
    fn get_memory_from_operation<T: ScatterGatherOperation>(
        &mut self,
        operation: &T,
    ) -> Result<MemBlock, DeviceError>;
}

pub trait RefreshStaleOperation {
    type Output;
    type Error;

    /// Checks an operation and refreshes it, returning any errors encountered.
    fn check_and_refresh(self) -> Result<Self::Output, Self::Error>;
}

pub trait ScatterGatherOperation: Sized {
    /// Returns whether the operation descriptors are from the given pool.
    fn descriptors_from(&self, pool: &DescriptorPool) -> bool;

    /// Returns the containing memory layout of this operation. That is, as a
    /// `MemBlockLayout` that can be used to claim the block of memory that
    /// contains within all accesses and reads for this operation.
    ///
    /// Precisely what the layout means is down to how the descriptors are
    /// defined, and knowledge of how the operation is created is necessary
    /// for making proper use of a claimed memory block.
    fn containing_memory(&self) -> MemBlockLayout;

    fn claim_memory<T: MemoryFromOperation>(
        self,
        device: &mut T,
    ) -> Result<OperationWithMemory<Self>, DeviceError> {
        OperationWithMemory::new(self, device)
    }
}

/// The operation structure encapsulates the information required to perform a
/// scatter gather operation (either multi or uni channel). There is an
/// overhead in setting up a scatter gather operation so it can be useful to
/// keep an operation for multiple runs.
#[derive(Debug)]
pub struct Operation {
    descriptor_block: DescriptorBlock,
    descriptor_functions: DescriptorFunctions,
    transfer_size: usize,
    containing_memory: MemBlockLayout,
    pub(crate) n_descriptors: usize,
    _operation_id: u64,
    pub(crate) first_descriptor_lsw: u32,
    #[allow(dead_code)]
    pub(crate) first_descriptor_msw: u32,
    pub(crate) last_descriptor_lsw: u32,
    #[allow(dead_code)]
    pub(crate) last_descriptor_msw: u32,
}

impl Operation {
    /// Creates a new `Operation` representing a scatter gather operation.
    ///
    /// This is marked unsafe as `transfer_size` and `containing_memory` must
    /// be set correctly for the provided descriptors.
    pub(crate) unsafe fn new(
        descriptor_block: DescriptorBlock,
        descriptor_functions: DescriptorFunctions,
        transfer_size: usize,
        containing_memory: MemBlockLayout,
    ) -> Result<Operation, OperationError> {
        let n_descriptors = descriptor_block.len();

        let first_descriptor_phys_addr = descriptor_block[0].physical_address();
        let last_descriptor_phys_addr = descriptor_block[n_descriptors - 1].physical_address();

        let operation_id = NEXT_OPERATION_ID.fetch_add(1, Ordering::Relaxed);

        let first_descriptor_lsw = first_descriptor_phys_addr as u32;

        let first_descriptor_msw = ((first_descriptor_phys_addr as u64) >> 32) as u32;

        let last_descriptor_lsw = last_descriptor_phys_addr as u32;

        let last_descriptor_msw = ((last_descriptor_phys_addr as u64) >> 32) as u32;

        // Create the operation structure.
        Ok(Operation {
            descriptor_block,
            descriptor_functions,
            transfer_size,
            containing_memory,
            n_descriptors,
            _operation_id: operation_id,
            first_descriptor_lsw,
            first_descriptor_msw,
            last_descriptor_lsw,
            last_descriptor_msw,
        })
    }

    #[inline]
    pub(crate) fn descriptor_block(&self) -> &DescriptorBlock {
        &self.descriptor_block
    }

    #[inline]
    pub(crate) fn descriptor_block_mut(&mut self) -> &mut DescriptorBlock {
        &mut self.descriptor_block
    }

    pub(crate) fn descriptor_functions(&self) -> DescriptorFunctions {
        self.descriptor_functions
    }

    /// Returns the containing memory layout of this operation. That is, as a
    /// `MemBlockLayout` that can be used to claim the block of memory that
    /// contains within all accesses and reads for this operation.
    ///
    /// Precisely what the layout means is down to how the descriptors are
    /// defined, and knowledge of how the operation is created is necessary
    /// for making proper use of a claimed memory block.
    pub fn containing_memory(&self) -> MemBlockLayout {
        ScatterGatherOperation::containing_memory(self)
    }
}

impl ScatterGatherOperation for Operation {
    /// Returns whether the descriptors associated with this operation are
    /// from the passed in DescriptorPool.
    #[inline]
    fn descriptors_from(&self, pool: &DescriptorPool) -> bool {
        pool.contains(&self.descriptor_block)
    }

    fn containing_memory(&self) -> MemBlockLayout {
        self.containing_memory
    }
}

impl RefreshStaleOperation for Operation {
    type Output = Operation;
    type Error = OperationRefreshError;

    fn check_and_refresh(mut self) -> Result<Self::Output, Self::Error> {
        match (&mut self).check_and_refresh() {
            Ok(_) => Ok(self),
            Err(e) => Err(OperationRefreshError {
                source: e,
                refreshed_operation: self,
            }),
        }
    }
}

impl RefreshStaleOperation for &mut Operation {
    type Output = ();
    type Error = OperationError;

    fn check_and_refresh(self) -> Result<Self::Output, Self::Error> {
        match walk_descriptor_chain_check_and_refresh(self) {
            Ok(val) => {
                if val == self.transfer_size {
                    Ok(())
                } else {
                    let expected = self.transfer_size;
                    Err(OperationError::InsufficientBytesTransferred {
                        expected,
                        actual: val,
                    })
                }
            }
            Err(e) => Err(OperationError::TransferError { source: e }),
        }
    }
}

#[derive(Debug)]
pub struct StaleOperation {
    operation: Operation,
}

impl StaleOperation {
    /// Unwraps without refreshing a StaleOperation into the [Operation] from
    /// which it which it was derived.
    ///
    /// Strictly speaking, the process of unwrapping is not unsafe, but the
    /// returned [Operation] may not be in a usable state.
    pub unsafe fn unwrap(self) -> Operation {
        let StaleOperation { operation } = self;

        operation
    }
}

impl RefreshStaleOperation for StaleOperation {
    type Output = Operation;
    type Error = OperationRefreshError;

    fn check_and_refresh(self) -> Result<Self::Output, Self::Error> {
        let StaleOperation { operation } = self;
        operation.check_and_refresh()
    }
}

impl From<Operation> for StaleOperation {
    fn from(op: Operation) -> Self {
        StaleOperation { operation: op }
    }
}

/// An operation with its associated memory which is claimed from the
/// [Device] during construction.
pub struct OperationWithMemory<T> {
    operation: T,
    memory: MemBlock,
}

impl<T: ScatterGatherOperation> OperationWithMemory<T> {
    pub fn new(
        operation: T,
        device: &mut impl MemoryFromOperation,
    ) -> Result<OperationWithMemory<T>, DeviceError> {
        let memory = device.get_memory_from_operation(&operation)?;
        Ok(OperationWithMemory { operation, memory })
    }

    /// Returns the block memory as a slice.
    pub fn as_slice(&self) -> &[u8] {
        self
    }

    /// Returns the block memory as a mutable slice.
    pub fn as_mut_slice(&mut self) -> &mut [u8] {
        self
    }
}

impl<T: ScatterGatherOperation> From<OperationWithMemory<T>> for (T, MemBlock) {
    fn from(source: OperationWithMemory<T>) -> Self {
        let OperationWithMemory { operation, memory } = source;

        (operation, memory)
    }
}

impl<T> ops::Deref for OperationWithMemory<T> {
    type Target = [u8];

    fn deref(&self) -> &[u8] {
        &*self.memory
    }
}

impl<T> ops::DerefMut for OperationWithMemory<T> {
    fn deref_mut(&mut self) -> &mut [u8] {
        &mut *self.memory
    }
}

#[cfg(test)]
mod tests_operation_config {

    use super::*;
    use crate::descriptors::{Descriptor, DESCRIPTOR_ALIGNMENT};
    use crate::errors::DmaCoreError;
    use crate::MemBlockLayout;
    use memmap2::MmapMut;

    const SIZE: usize = 1000;

    fn make_mem() -> MmapMut {
        let mut mem = MmapMut::map_anon(SIZE).unwrap();

        // Hackily put some data in
        for n in 0..SIZE {
            mem[n] = (n % (u8::MAX as usize + 1)) as u8;
        }

        mem
    }

    fn dummy_descriptor_status(_descriptor: &Descriptor) -> Result<usize, DmaCoreError> {
        Ok(0)
    }

    fn dummy_refresh_descriptor(_descriptor: &mut Descriptor) {}

    #[test]
    /// The operation should have its fields correctly populated.
    fn test_operation_fields() {
        // Some page aligned address
        // This can be arbitrary as it only needs to be correct when used by
        // the DMA engine.
        let aligned_addr = 0xFFF000;

        for phys_offset in 0..DESCRIPTOR_ALIGNMENT {
            let descriptor_functions = DescriptorFunctions {
                descriptor_status: dummy_descriptor_status,
                refresh_descriptor: dummy_refresh_descriptor,
            };

            let mem = make_mem();
            let mut pool = unsafe { DescriptorPool::new(mem, aligned_addr - phys_offset) };
            let descriptor_block = pool
                .request_descriptors(pool.descriptors_available())
                .unwrap();
            let n_descriptors = descriptor_block.len();
            let first_phys_addr = descriptor_block[0].physical_address();
            let last_phys_addr = descriptor_block[n_descriptors - 1].physical_address();

            let transfer_bytes = 0usize; // We don't actually use this
            let containing_memory = MemBlockLayout { offset: 0, size: 0 };
            let operation = unsafe {
                Operation::new(
                    descriptor_block,
                    descriptor_functions,
                    transfer_bytes,
                    containing_memory,
                )
            }
            .unwrap();

            assert!(operation.n_descriptors == n_descriptors);

            let first_descriptor_lsw = first_phys_addr as u32;
            let first_descriptor_msw = ((first_phys_addr as u64) >> 32) as u32;

            let last_descriptor_lsw = last_phys_addr as u32;
            let last_descriptor_msw = ((last_phys_addr as u64) >> 32) as u32;
            assert!(operation.first_descriptor_lsw == first_descriptor_lsw);
            assert!(operation.first_descriptor_msw == first_descriptor_msw);
            assert!(operation.last_descriptor_lsw == last_descriptor_lsw);
            assert!(operation.last_descriptor_msw == last_descriptor_msw);
        }
    }
}
