use std::io;
use thiserror::Error;

use crate::descriptors::DescriptorError;
use crate::mem_pool::MemPoolError;
use crate::mem_setup::MemSetupError;
use crate::operation::OperationError;
use crate::scatter_gather::ScatterGatherError;

/// Errors from the FPGA DMA core
#[derive(Error, Debug)]
pub enum DmaCoreError {
    /// DMA internal error
    #[error("DMA internal error")]
    Internal,
    /// DMA slave error
    #[error("DMA slave error")]
    Slave,
    /// DMA decode error
    #[error("DMA decode error")]
    Decode,
    /// Scatter Gather internal error
    #[error("Scatter Gather internal error")]
    SGInternal,
    /// Scatter Gather slave error
    #[error("Scatter Gather slave error")]
    SGSlave,
    /// Scatter Gather decode error
    #[error("Scatter Gather decode error")]
    SGDecode,
    /// Scatter Gather descriptor not completed
    #[error("Scatter Gather Descriptor not completed")]
    DescriptorNotCompleted,
    /// Insufficient descriptors for the transfer
    #[error("Insufficient descriptors for the transfer")]
    BDShortfall,
}

/// Errors during direct DMA configuration
#[derive(Error, Debug)]
pub enum DirectDmaError {
    /// Invalid size requested
    #[error("Invalid size requested")]
    InvalidSize,
    /// Invalid offset for the transfer
    #[error("Invalid offset for the transfer")]
    InvalidOffset,
    /// The combination of nbytes and offset would cause a memory overflow
    #[error("The combination of nbytes and offset would cause a memory overflow")]
    MemoryOverflow,
}

/// Errors during interacting with the DMA device. This is the main user
/// facing error, with more specific sub-errors wrapped appropriately.
#[derive(Error, Debug)]
pub enum DeviceError {
    /// IO error.
    #[error("IO error: {source}")]
    Io {
        #[from]
        source: io::Error,
    },
    /// Error making a system call.
    #[error("System call error: {source}")]
    Nix {
        #[from]
        source: nix::Error,
    },
    /// Library cannot handle MM2S and S2MM in the same device
    #[error("Library cannot handle MM2S and S2MM in the same device")]
    UnsupportedCapability,
    /// Function requires a direct device and does not support scatter gather dma
    #[error("Function requires a direct device and does not support scatter gather dma")]
    UnsupportedScatterGather,
    /// Function requires a scatter gather device and does not support direct dma
    #[error("Function require s a scatter gather device and does not support direct dma")]
    UnsupportedDirect,
    /// Function requires a non multi channel device and does not support multi channel dma
    #[error("Function requires a non multi channel device and does not support multi channel dma")]
    UnsupportedMultiChannel,
    /// Function requires a multi channel device and does not support non multi channel dma
    #[error("Function requires a multi channel device and does not support non multi channel dma")]
    UnsupportedUniChannel,
    /// Device took too long to reset
    #[error("Device took too long to reset")]
    ResetFailed,
    /// Device took too long to halt
    #[error("Device took too long to halt")]
    HaltTimedOut,
    /// Transfer in progress. Call wait_transfer_complete
    #[error("Transfer in progress. Call wait_transfer_complete")]
    TransferInProgress,
    /// operation was not created by this device
    #[error("operation was not created by this device")]
    WrongDevice,
    /// Device is not halted. This operation can only run on a halted device
    #[error("Device is not halted. This operation can only run on a halted device")]
    DeviceNotHalted,
    /// Requested channel is outside the range [0, MAX_CHANNELS)
    #[error("Requested channel is outside the range [0, MAX_CHANNELS)")]
    InvalidChannel,
    /// The DMA transfer timed out
    #[error("The DMA transfer timed out")]
    TimedOut,
    /// DMA core error
    #[error("DMA core error: {source}")]
    DmaCore {
        #[from]
        source: DmaCoreError,
    },
    /// Descriptor handling error.
    #[error("Descriptor error: {source}")]
    Descriptor {
        #[from]
        source: DescriptorError,
    },
    /// Error setting up the device memory maps.
    #[error("Error setting up the device memory maps: {source}")]
    MemSetup {
        #[from]
        source: MemSetupError,
    },
    /// Scatter-gather error.
    #[error("Scatter-gather error: {source}")]
    ScatterGather {
        #[from]
        source: ScatterGatherError,
    },
    /// Direct DMA error.
    #[error("Direct DMA error: {source}")]
    DirectDma {
        #[from]
        source: DirectDmaError,
    },
    /// Operation error.
    #[error("Operation error: {source}")]
    Operation {
        #[from]
        source: OperationError,
    },
    /// Mem pool error.
    #[error("Mem pool error: {source}")]
    MemPool {
        #[from]
        source: MemPoolError,
    },
}
