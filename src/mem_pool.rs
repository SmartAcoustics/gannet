use std::cmp;
use std::fmt;
use std::ops;
use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::Arc;

use memmap2::MmapMut;
use thiserror::Error;

#[derive(Error, Debug, PartialEq)]
/// Errors returned by [MemPool].
pub enum MemPoolError {
    /// The requested block does not fit inside the memory pool.
    #[error("The requested block does not fit inside the memory pool.")]
    MemBlockRequestedInvalid,
    /// MemBlock to be checked out overlaps with an already checked out block.
    #[error("MemBlock to be checked out overlaps with an already checked out block.")]
    MemBlockCheckoutOverlap,
    /// The block to be checked out has not been created or has been removed.
    #[error("The block to be checked out has not been created or has been removed.")]
    MemBlockDoesNotExist,
}

#[derive(Error, Debug, PartialEq)]
/// Errors returned whilst trying to coalesce a series of `MemBlock`s
pub enum MemBlockCoalesceError {
    /// The `MemBlocks` are not contiguous and can't be coalesced
    #[error("The MemBlocks are not contiguous and can't be coalesced.")]
    NonContiguousMemBlocks,
    /// The `MemBlocks` are not all from the same memory pool.
    #[error("The MemBlocks are not all from the same memory pool.")]
    MemBlocksFromDifferentPools,
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub struct MemBlockLayout {
    pub offset: usize,
    pub size: usize,
}

impl MemBlockLayout {
    fn overlaps(&self, other: &MemBlockLayout) -> bool {
        let self_excl_upper = self.offset + self.size;
        let other_excl_upper = other.offset + other.size;

        // [---####------] (self)
        // [-----####----] (other)
        // self.offset < other_excl_upper checks the following range is non zero:
        // [---[-----)---]
        // other.offset < self_excl_upper checks the following range is non zero:
        // [-----[-)-----]
        // Since it's symmetrical, this holds for either self.offset or
        // other.offset being smaller.
        if (self.offset < other_excl_upper) && (other.offset < self_excl_upper) {
            true
        } else {
            false
        }
    }
}

impl From<ops::Range<usize>> for MemBlockLayout {
    fn from(range: ops::Range<usize>) -> MemBlockLayout {
        MemBlockLayout {
            offset: range.start,
            size: range.end.saturating_sub(range.start),
        }
    }
}

impl From<MemBlockLayout> for ops::Range<usize> {
    fn from(layout: MemBlockLayout) -> ops::Range<usize> {
        layout.offset..layout.offset.saturating_add(layout.size)
    }
}

impl ops::Add for MemBlockLayout {
    type Output = Self;
    fn add(self, other: Self) -> Self {
        let self_range: ops::Range<usize> = self.into();
        let other_range: ops::Range<usize> = other.into();

        let combined_range = cmp::min(self_range.start, other_range.start)
            ..cmp::max(self_range.end, other_range.end);

        combined_range.into()
    }
}

/// A structure to allow the pool to keep track of what blocks layouts have
/// been checked out.
#[derive(Debug)]
struct MemBlockShadow {
    layout: MemBlockLayout,
    block_alive: Arc<AtomicBool>,
}

/// Represents a block of memory that has been checked out from a [MemPool]
/// The memory it claims will not be accessible through an overlapping block
/// until this block is dropped.
pub struct MemBlock {
    inner_mem: Arc<MmapMut>,
    owned_layout: MemBlockLayout,
    block_alive: Arc<AtomicBool>,
}

unsafe impl Send for MemBlock {}
unsafe impl Sync for MemBlock {}

impl MemBlock {
    unsafe fn new(mem: Arc<MmapMut>, owned_layout: MemBlockLayout) -> MemBlock {
        let block_alive = Arc::new(AtomicBool::new(true));

        // Note we need to store the Mmap on this object because we need the
        // Mmap to live as long as this object.
        MemBlock {
            inner_mem: mem,
            owned_layout,
            block_alive,
        }
    }

    /// Returns the block memory as a slice.
    pub fn as_slice(&self) -> &[u8] {
        self
    }

    /// Returns the block memory as a mutable slice.
    pub fn as_mut_slice(&mut self) -> &mut [u8] {
        self
    }

    fn shadow(&self) -> MemBlockShadow {
        MemBlockShadow {
            layout: self.owned_layout,
            block_alive: self.block_alive.clone(),
        }
    }
}

impl ops::Deref for MemBlock {
    type Target = [u8];

    fn deref(&self) -> &[u8] {
        // The following unsafe relies heavily on the rest of the checking
        // out semantics working properly. In essence, there should never be
        // more than one block anywhere that controls the chunk of memory
        // defined by owned_layout.
        unsafe {
            let ptr = (self.inner_mem.as_ptr() as *mut u8).add(self.owned_layout.offset);

            std::slice::from_raw_parts(ptr, self.owned_layout.size)
        }
    }
}

impl AsRef<[u8]> for MemBlock {
    fn as_ref(&self) -> &[u8] {
        self
    }
}

impl ops::DerefMut for MemBlock {
    fn deref_mut(&mut self) -> &mut [u8] {
        // The following unsafe relies heavily on the rest of the checking
        // out semantics working properly. In essence, there should never be
        // more than one block anywhere that controls the chunk of memory
        // defined by owned_layout.
        unsafe {
            let ptr = (self.inner_mem.as_ptr() as *mut u8).add(self.owned_layout.offset);

            std::slice::from_raw_parts_mut(ptr, self.owned_layout.size)
        }
    }
}

impl fmt::Debug for MemBlock {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{:?}", self.as_slice())
    }
}

impl Drop for MemBlock {
    fn drop(&mut self) {
        // This should be the _only_ place that block_alive is cleared.
        self.block_alive.store(false, Ordering::SeqCst);
    }
}

/// Attempts to coalesce a structure of memory blocks into a single contiguous
/// memory slice.
pub trait TryCoalesce {
    type Output: AsRef<[u8]>;
    type Error: std::error::Error;

    fn try_coalesce(self) -> Result<Self::Output, Self::Error>;
}

pub trait TryCoalesceFrom<C>: AsRef<[u8]> + Sized {
    type Error: std::error::Error;

    fn try_coalesce_from(values: C) -> Result<Self, Self::Error>;
}

#[derive(Debug)]
pub struct CoalescedMemBlocks<T> {
    _inner: T,
    ptr: Option<*const u8>,
    size: usize,
}

impl<T> TryCoalesceFrom<T> for CoalescedMemBlocks<T>
where
    T: AsRef<[MemBlock]>,
{
    type Error = MemBlockCoalesceError;

    fn try_coalesce_from(values: T) -> Result<Self, Self::Error> {
        let u8_ref = values.as_ref();

        if u8_ref.len() == 0 {
            return Ok(CoalescedMemBlocks {
                _inner: values,
                ptr: None,
                size: 0,
            });
        }

        let mut running_size = 0usize;
        let offset = u8_ref[0].owned_layout.offset;
        let inner_mem_ptr = u8_ref[0].inner_mem.as_ptr();

        for mem_block in u8_ref {
            if mem_block.inner_mem.as_ptr() != inner_mem_ptr {
                return Err(MemBlockCoalesceError::MemBlocksFromDifferentPools);
            }

            if mem_block.owned_layout.offset != (offset + running_size) {
                return Err(MemBlockCoalesceError::NonContiguousMemBlocks);
            }

            running_size += mem_block.owned_layout.size;
        }

        let ptr = unsafe { (inner_mem_ptr as *mut u8).add(offset) as *const u8 };

        Ok(CoalescedMemBlocks {
            _inner: values,
            ptr: Some(ptr),
            size: running_size,
        })
    }
}

impl<T> Default for CoalescedMemBlocks<T>
where
    T: AsRef<[MemBlock]> + Default,
{
    fn default() -> Self {
        CoalescedMemBlocks {
            _inner: Default::default(),
            ptr: None,
            size: 0,
        }
    }
}

impl<T> AsRef<[u8]> for CoalescedMemBlocks<T>
where
    T: AsRef<[MemBlock]>,
{
    fn as_ref(&self) -> &[u8] {
        if let Some(ptr) = self.ptr {
            unsafe { std::slice::from_raw_parts(ptr, self.size) }
        } else {
            &[]
        }
    }
}

impl<T> ops::Deref for CoalescedMemBlocks<T>
where
    T: AsRef<[MemBlock]>,
{
    type Target = [u8];

    fn deref(&self) -> &Self::Target {
        self.as_ref()
    }
}

impl<T> TryCoalesce for T
where
    T: AsRef<[MemBlock]>,
{
    type Output = CoalescedMemBlocks<T>;
    type Error = MemBlockCoalesceError;

    fn try_coalesce(self) -> Result<Self::Output, Self::Error> {
        CoalescedMemBlocks::try_coalesce_from(self)
    }
}

/// Constructs a new memory pool out of a memory map, over which it takes
/// ownership. A [MemBlock] can then be created and then checked out. A block is
/// a piece of the pool that allows both read and write operations on it.
///
/// Rust aliasing rules
/// are enforced on the block through the checkout mechanism: although it is
/// possible to have multiple blocks created, each byte of memory can only
/// by contained within a single checked out block.
///
/// That is, if an attempt is made to check out a block that overlaps with
/// an already checked out block, an error is raised.
#[derive(Debug)]
pub struct MemPool {
    mem: Arc<MmapMut>,
    mem_size: usize,
    blocks_checked_out: Vec<MemBlockShadow>,
}

impl MemPool {
    pub fn new(mem: MmapMut) -> Self {
        let mem_size = mem.len();
        let mem = Arc::new(mem);
        let blocks_checked_out = Vec::new();

        MemPool {
            mem,
            mem_size,
            blocks_checked_out,
        }
    }

    /// Check out a block described by the memory layout
    pub fn check_out_block(&mut self, layout: MemBlockLayout) -> Result<MemBlock, MemPoolError> {
        if layout.offset + layout.size > self.mem_size {
            return Err(MemPoolError::MemBlockRequestedInvalid);
        }

        self.blocks_checked_out
            .retain(|block_shadow| block_shadow.block_alive.load(Ordering::SeqCst));

        for each_shadow in &self.blocks_checked_out {
            if each_shadow.layout.overlaps(&layout) {
                return Err(MemPoolError::MemBlockCheckoutOverlap);
            }
        }

        let new_block = unsafe { MemBlock::new(self.mem.clone(), layout) };

        self.blocks_checked_out.push(new_block.shadow());

        Ok(new_block)
    }

    /// Get the size of the memory pool in bytes.
    #[inline]
    pub fn size(&self) -> usize {
        self.mem_size
    }
}

#[cfg(test)]
mod tests {

    use super::*;
    const SIZE: usize = 1000;

    fn make_mem() -> MmapMut {
        let mut mem = MmapMut::map_anon(SIZE).unwrap();

        // Hackily put some data in
        for n in 0..SIZE {
            mem[n] = (n % (u8::MAX as usize + 1)) as u8;
        }

        mem
    }

    #[test]
    fn test_mem_layout_from_range() {
        let test_layout: MemBlockLayout = (0..10).into();
        assert_eq!(
            test_layout,
            MemBlockLayout {
                offset: 0,
                size: 10
            }
        );

        let test_layout: MemBlockLayout = (10..20).into();
        assert_eq!(
            test_layout,
            MemBlockLayout {
                offset: 10,
                size: 10
            }
        );

        let test_layout: MemBlockLayout = (15..20).into();
        assert_eq!(
            test_layout,
            MemBlockLayout {
                offset: 15,
                size: 5
            }
        );

        let test_layout: MemBlockLayout = (15..16).into();
        assert_eq!(
            test_layout,
            MemBlockLayout {
                offset: 15,
                size: 1
            }
        );

        let test_layout: MemBlockLayout = (15..15).into();
        assert_eq!(
            test_layout,
            MemBlockLayout {
                offset: 15,
                size: 0
            }
        );

        let test_layout: MemBlockLayout = (15..5).into();
        assert_eq!(
            test_layout,
            MemBlockLayout {
                offset: 15,
                size: 0
            }
        );
    }

    #[test]
    fn test_mem_layout_into_range() {
        let test_layout: MemBlockLayout = (0..10).into();
        assert_eq!(0..10, test_layout.into());

        let test_layout: MemBlockLayout = (10..20).into();
        assert_eq!(10..20, test_layout.into());

        let test_layout: MemBlockLayout = (15..20).into();
        assert_eq!(15..20, test_layout.into());

        let test_layout: MemBlockLayout = (15..16).into();
        assert_eq!(15..16, test_layout.into());

        let test_layout: MemBlockLayout = (15..15).into();
        assert_eq!(15..15, test_layout.into());

        let test_layout: MemBlockLayout = (15..5).into();
        assert_eq!(15..15, test_layout.into());
    }

    #[test]
    /// It should be possible to get the size of the memory pool.
    fn test_pool_size() {
        let mem = make_mem();
        let pool = MemPool::new(mem);

        assert_eq!(pool.size(), SIZE);
    }

    #[test]
    fn test_create_and_check_out_blocks() {
        let mem = make_mem();
        let mut pool = MemPool::new(mem);

        let block = pool
            .check_out_block(MemBlockLayout {
                offset: 0,
                size: 100,
            })
            .unwrap();
        let block2 = pool
            .check_out_block(MemBlockLayout {
                offset: 110,
                size: 100,
            })
            .unwrap();

        assert_eq!(block.as_slice().len(), 100);
        assert_eq!(block2.as_slice().len(), 100);

        // Only true of the offset is not too big
        assert!(block.as_slice() != block2.as_slice());
    }

    #[test]
    /// If two blocks are overlapping, it should not be possible to check
    /// out the second block whilst the first still exists. Once the first
    /// drops out of scope, it should be possible to check out the second.
    fn test_overlapping_blocks() {
        let mem = make_mem();
        let mut pool = MemPool::new(mem);

        {
            let _block = pool
                .check_out_block(MemBlockLayout { offset: 0, size: 5 })
                .unwrap();
            let block2 = pool.check_out_block(MemBlockLayout { offset: 4, size: 5 });
            assert_eq!(block2.unwrap_err(), MemPoolError::MemBlockCheckoutOverlap);
        }

        // Out of scope, so we can check it out now.
        let rwlock_block2_result = pool.check_out_block(MemBlockLayout { offset: 4, size: 5 });
        assert!(rwlock_block2_result.is_ok());
    }

    #[test]
    /// If the offset is out of range, checking out a block should error.
    fn test_offset_out_of_range() {
        let mem = make_mem();
        let mut pool = MemPool::new(mem);

        // Should be fine
        {
            let _block = pool
                .check_out_block(MemBlockLayout {
                    offset: SIZE - 1,
                    size: 1,
                })
                .unwrap();
        }

        // Should raise
        {
            let block = pool.check_out_block(MemBlockLayout {
                offset: SIZE,
                size: 1,
            });
            assert_eq!(block.unwrap_err(), MemPoolError::MemBlockRequestedInvalid);
        }

        // Should be fine
        {
            let _block = pool
                .check_out_block(MemBlockLayout {
                    offset: SIZE,
                    size: 0,
                })
                .unwrap();
        }

        // Should raise
        {
            let block = pool.check_out_block(MemBlockLayout {
                offset: SIZE + 1,
                size: 0,
            });
            assert_eq!(block.unwrap_err(), MemPoolError::MemBlockRequestedInvalid);
        }
    }

    #[test]
    /// If the size pushes the block out of range, checking out a block should
    /// error.
    fn test_size_and_offset_out_of_range() {
        let mem = make_mem();
        let mut pool = MemPool::new(mem);

        // Should be fine
        {
            let _block = pool
                .check_out_block(MemBlockLayout {
                    offset: 0,
                    size: SIZE,
                })
                .unwrap();
        }

        // Should raise
        {
            let block = pool.check_out_block(MemBlockLayout {
                offset: 0,
                size: SIZE + 1,
            });
            assert_eq!(block.unwrap_err(), MemPoolError::MemBlockRequestedInvalid);
        }

        // Should be fine
        {
            let _block = pool
                .check_out_block(MemBlockLayout {
                    offset: SIZE - 1,
                    size: 1,
                })
                .unwrap();
        }

        // Should raise
        {
            let block = pool.check_out_block(MemBlockLayout {
                offset: SIZE - 1,
                size: 2,
            });
            assert_eq!(block.unwrap_err(), MemPoolError::MemBlockRequestedInvalid);
        }

        // Should be fine
        {
            let _block = pool
                .check_out_block(MemBlockLayout {
                    offset: SIZE,
                    size: 0,
                })
                .unwrap();
        }
    }

    #[test]
    /// It should be possible to write to the block.
    fn test_writing_to_block() {
        let mem = make_mem();
        let mut pool = MemPool::new(mem);

        let mut expected_output = vec![0; 10];

        {
            let mut wr_block = pool
                .check_out_block(MemBlockLayout {
                    offset: 20,
                    size: 10,
                })
                .unwrap();

            {
                let slice = wr_block.as_mut_slice();

                for n in 0..slice.len() {
                    slice[n] = n as u8;
                    expected_output[n] = n as u8;
                }
            }

            let read_slice = wr_block.as_slice();
            assert_eq!(expected_output, read_slice);
        }

        // Now the writer is out of scope, we can create a new block to read
        let block2 = pool
            .check_out_block(MemBlockLayout {
                offset: 20,
                size: 10,
            })
            .unwrap();
        let read_slice = block2.as_slice();
        assert_eq!(expected_output, read_slice);
    }

    #[test]
    /// It should be possible to create a zero size block.
    fn test_zero_size_block() {
        let mem = make_mem();
        let mut pool = MemPool::new(mem);

        // We can check out two zero length blocks at the same offset.
        let block = pool
            .check_out_block(MemBlockLayout { offset: 0, size: 0 })
            .unwrap();
        let mut block2 = pool
            .check_out_block(MemBlockLayout { offset: 0, size: 0 })
            .unwrap();
        assert!(block.as_slice().is_empty());
        assert!(block2.as_mut_slice().is_empty());

        // Also at the end of the array
        let block = pool
            .check_out_block(MemBlockLayout {
                offset: SIZE,
                size: 0,
            })
            .unwrap();
        let mut block2 = pool
            .check_out_block(MemBlockLayout {
                offset: SIZE,
                size: 0,
            })
            .unwrap();
        assert!(block.as_slice().is_empty());
        assert!(block2.as_mut_slice().is_empty());
    }
}

#[cfg(test)]
mod test_coalesce_memblock_vec {

    use super::*;
    const SIZE: usize = 1000;

    fn make_mem() -> MmapMut {
        let mut mem = MmapMut::map_anon(SIZE).unwrap();

        // Hackily put some data in
        for n in 0..SIZE {
            mem[n] = (n % (u8::MAX as usize + 1)) as u8;
        }

        mem
    }

    #[test]
    /// A vec of contiguous memblocks should be coalesceable.
    fn test_coalesced_block() {
        let mem = make_mem();
        let mut pool = MemPool::new(mem);

        let mut expected_output = Vec::new();
        let mut memblock_vec = Vec::new();
        let mut offset = 20;

        for n in 0..10 {
            let size = 10 + n;
            let mut wr_block = pool
                .check_out_block(MemBlockLayout { offset, size })
                .unwrap();

            offset += size;

            let slice = wr_block.as_mut_slice();

            for n in 0..slice.len() {
                slice[n] = n as u8;
                expected_output.push(n as u8);
            }

            memblock_vec.push(wr_block);
        }

        assert_eq!(&expected_output, &*(memblock_vec.try_coalesce().unwrap()));
    }

    #[test]
    /// Non-contiguous blocks should fail
    fn test_non_contiguous_blocks() {
        let mem = make_mem();
        let mut pool = MemPool::new(mem);

        let mut memblock_vec = Vec::new();
        let mut offset = 20;

        for n in 0..10 {
            let size = 10 + n;
            let mut wr_block = pool
                .check_out_block(MemBlockLayout { offset, size })
                .unwrap();

            offset += size + 1;

            let slice = wr_block.as_mut_slice();

            for n in 0..slice.len() {
                slice[n] = n as u8;
            }

            memblock_vec.push(wr_block);
        }

        match memblock_vec.try_coalesce() {
            Ok(_) => panic!(),
            Err(MemBlockCoalesceError::NonContiguousMemBlocks) => (),
            Err(_) => panic!(),
        }
    }

    #[test]
    /// The memblocks should be ordered otherwise they are deemed to be
    /// non-contiguous
    fn test_non_ordered_blocks() {
        let mem = make_mem();
        let mut pool = MemPool::new(mem);

        let mut expected_output = Vec::new();
        let mut memblock_vec = Vec::new();
        let mut offset = 20;

        for n in 0..10 {
            let size = 10 + n;
            let mut wr_block = pool
                .check_out_block(MemBlockLayout { offset, size })
                .unwrap();

            offset += size;

            let slice = wr_block.as_mut_slice();

            for n in 0..slice.len() {
                slice[n] = n as u8;
                expected_output.push(n as u8);
            }

            memblock_vec.push(wr_block);
        }

        // It should work initially
        assert_eq!(
            &expected_output,
            (&memblock_vec).try_coalesce().unwrap().as_ref()
        );

        // Swap elements 4 and 5.
        memblock_vec.swap(4, 5);

        // now it errors.
        match memblock_vec.try_coalesce() {
            Ok(_) => panic!(),
            Err(MemBlockCoalesceError::NonContiguousMemBlocks) => (),
            Err(_) => panic!(),
        }
    }

    #[test]
    /// An empty vec of memblocks should return an empty slice
    fn test_empty_memblock_vec() {
        let memblock_vec: Vec<MemBlock> = Vec::new();
        let expected_output: &[u8] = &[];
        assert_eq!(
            expected_output,
            memblock_vec.try_coalesce().unwrap().as_ref()
        );
    }

    #[test]
    /// Blocks from different pools should not coalesce.
    fn test_blocks_from_different_pools_fail() {
        let mem1 = make_mem();
        let mem2 = make_mem();

        let mut pool1 = MemPool::new(mem1);
        let mut pool2 = MemPool::new(mem2);

        let mut memblock_vec = Vec::new();
        let mut offset = 20;

        let size = 10;
        let wr_block1 = pool1
            .check_out_block(MemBlockLayout { offset, size })
            .unwrap();

        memblock_vec.push(wr_block1);
        offset += size;

        let wr_block2 = pool2
            .check_out_block(MemBlockLayout { offset, size })
            .unwrap();

        memblock_vec.push(wr_block2);

        match memblock_vec.try_coalesce() {
            Ok(_) => panic!(),
            Err(MemBlockCoalesceError::MemBlocksFromDifferentPools) => (),
            Err(_) => panic!(),
        }
    }
}
