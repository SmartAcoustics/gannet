mod descriptors;
mod running;

pub use descriptors::MAX_BYTES_TRANSFER_PER_DESCRIPTOR;
pub use running::{ErroredRunningDevice, RunningDevice};

use std::{error::Error, fmt, fs::File, mem::size_of, path::PathBuf, time};

use descriptors::{DescriptorControlField, DescriptorFields, DescriptorStatusField};

use crate::descriptors::{Descriptor, DescriptorPool};
use crate::operation::{
    MemoryFromOperation, Operation, OperationWithMemory, ScatterGatherOperation,
};

use crate::device_core::DeviceSpecifics;
use crate::errors::{DeviceError, DmaCoreError};
use crate::mem_setup::{setup_cfg_and_data_mem, setup_descriptors_mem};
use crate::scatter_gather::{
    write_regular_blocks_descriptor_chain, DescriptorFunctions, ScatterGatherConfig,
    ScatterGatherDevice, ScatterGatherDeviceConfig,
};
use crate::uni_channel::UniChannelDeviceCommon;
use crate::utils::{enable_interrupts, get_and_consume_interrupts, open_device};
use crate::{BusDataWidth, MemBlock, MemBlockLayout, MemPool};

use crate::uni_channel::registers::{ControlRegister, Registers};

use crate::TransferType;

#[cfg(feature = "async")]
use tokio::io::unix::AsyncFd;

const U32_SIZE: usize = size_of::<u32>();

/// Error during a scatter-gather DMA transfer. This error type encapsulates
/// the device that is recovered
#[derive(Debug)]
pub struct ScatterGatherTransferError {
    pub source: DeviceError,
    pub errored_device: ErroredRunningDevice,
}

impl Error for ScatterGatherTransferError {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        Some(&self.source)
    }
}

impl fmt::Display for ScatterGatherTransferError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Error during transfer: {}", self.source)
    }
}

// This function reads the descriptor status returns the correct error if
// any have been reported. If there are no errors it returns the number of
// bytes transferred by the descriptor
pub(crate) fn descriptor_status(descriptor: &Descriptor) -> Result<usize, DmaCoreError> {
    let descriptor_data = descriptor.as_slice();

    // Set the offset of the status word within the current
    // descriptor. We divide the status_offset by 4 as this offset is
    // counted in words rather than bytes.
    let descriptor_status = descriptor_data[DescriptorFields::Status.offset() / U32_SIZE];

    // Check the error status of the descriptor and return the
    // relevant error.
    if (DescriptorStatusField::InternalError.bitmask() & descriptor_status) != 0 {
        Err(DmaCoreError::Internal)
    } else if (DescriptorStatusField::SlaveError.bitmask() & descriptor_status) != 0 {
        Err(DmaCoreError::Slave)
    } else if (DescriptorStatusField::DecodeError.bitmask() & descriptor_status) != 0 {
        Err(DmaCoreError::Decode)
    } else if (DescriptorStatusField::Complete.bitmask() & descriptor_status)
        != DescriptorStatusField::Complete.bitmask()
    {
        Err(DmaCoreError::DescriptorNotCompleted)
    } else {
        // Return the nbytes transferred by this descriptor
        Ok((DescriptorStatusField::TransferredBytes.bitmask() & descriptor_status) as usize)
    }
}

// This function overwrites the complete bit in the descriptor status to
// refresh it for the next run
fn refresh_descriptor(descriptor: &mut Descriptor) {
    let descriptor_data = descriptor.as_mut_slice();

    // The offset is in bytes, not words, so we need to divide
    // by U32_SIZE.
    descriptor_data[DescriptorFields::Status.offset() / U32_SIZE] = 0u32;
}

fn start_of_frame_bitmask() -> u32 {
    DescriptorControlField::StartOfFrame.bitmask()
}

fn end_of_frame_bitmask() -> u32 {
    DescriptorControlField::EndOfFrame.bitmask()
}

fn desc_field_nxt_desc_lsw_offset() -> usize {
    DescriptorFields::NextDescriptorLsw.offset()
}

fn desc_field_nxt_desc_msw_offset() -> usize {
    DescriptorFields::NextDescriptorMsw.offset()
}

fn desc_field_buffer_addr_lsw_offset() -> usize {
    DescriptorFields::BufferAddrLsw.offset()
}

fn desc_field_buffer_addr_msw_offset() -> usize {
    DescriptorFields::BufferAddrMsw.offset()
}

fn desc_field_control_offset() -> usize {
    DescriptorFields::Control.offset()
}

/// The Device structure encapsulates all the information required for the
/// system to perform scatter gather DMA operations. There are methods on the
/// Device structure which enable these operations.
#[derive(Debug)]
pub struct Device {
    unichannel_common: UniChannelDeviceCommon,
    dma_descriptor_pool: DescriptorPool,
    dma_mem_pool: MemPool,
    dma_data_phys_addr: usize,
    file: File,
    bus_width: BusDataWidth,
    #[cfg(feature = "async")]
    async_file: Option<AsyncFd<File>>,
}

impl Drop for Device {
    fn drop(&mut self) {
        // Reset the underlying device on drop. Ignore any errors as the
        // device is being dropped anyway.
        let _ = self.reset();
    }
}

// Implement the DeviceSpecifics trait
impl DeviceSpecifics for Device {
    fn write_reset_bit(&self) {
        self.unichannel_common.write_reset_bit();
    }

    fn halted(&self) -> bool {
        self.unichannel_common.halted()
    }

    fn idle(&self) -> bool {
        self.unichannel_common.idle()
    }

    fn check_error(&self) -> Result<(), DmaCoreError> {
        self.unichannel_common.check_error()
    }

    fn get_and_consume_interrupts(
        &mut self,
        timeout: &time::Duration,
    ) -> Result<usize, DeviceError> {
        get_and_consume_interrupts(&mut self.file, timeout)
    }
}

impl ScatterGatherDevice for Device {
    fn borrow_descriptor_pool(&mut self) -> &mut DescriptorPool {
        &mut self.dma_descriptor_pool
    }
}

impl ScatterGatherConfig for Device {
    fn scatter_gather_config(&self) -> ScatterGatherDeviceConfig {
        ScatterGatherDeviceConfig {
            dma_data_size: self.memory_pool_size(),
            dma_data_phys_addr: self.dma_data_phys_addr,
            max_bytes_transfer_per_descriptor: MAX_BYTES_TRANSFER_PER_DESCRIPTOR,
            start_of_frame_bitmask: start_of_frame_bitmask(),
            end_of_frame_bitmask: end_of_frame_bitmask(),
            desc_field_nxt_desc_lsw_offset: desc_field_nxt_desc_lsw_offset(),
            desc_field_nxt_desc_msw_offset: desc_field_nxt_desc_msw_offset(),
            desc_field_buffer_addr_lsw_offset: desc_field_buffer_addr_lsw_offset(),
            desc_field_buffer_addr_msw_offset: desc_field_buffer_addr_msw_offset(),
            desc_field_control_offset: desc_field_control_offset(),
        }
    }
}

impl Device {
    /// Represents a scatter-gather single-channel device which it interacts
    /// with through the device file.
    ///
    /// Once created, DMA operations can be created and run on the device.
    pub fn new(device_path: &PathBuf, bus_width: BusDataWidth)
        -> Result<Device, DeviceError>
    {
        // Initialise the device
        let mut device = Device::init(&device_path, bus_width)?;

        // Reset the device
        device.reset()?;

        Ok(device)
    }

    /// A function to initialise the DMA device.
    ///
    /// Given a system file, this function extracts all the required
    /// information and returns a DMA device.
    fn init(device_path: &PathBuf, bus_width: BusDataWidth)
        -> Result<Device, DeviceError>
    {
        // Open the device file with the right settings
        let file = open_device(device_path)?;

        let (cfg_mem, dma_data_mem, dma_data_phys_addr) =
            setup_cfg_and_data_mem::<Registers>(device_path, &file)?;

        let unichannel_common = UniChannelDeviceCommon::new(cfg_mem)?;

        if !unichannel_common.scatter_gather_enabled {
            // Error if the device is not scatter gather enabled
            return Err(DeviceError::UnsupportedDirect);
        }

        let (dma_descriptors_mem, dma_descriptors_phys_addr) =
            setup_descriptors_mem(device_path, &file)?;

        let dma_descriptor_pool =
            unsafe { DescriptorPool::new(dma_descriptors_mem, dma_descriptors_phys_addr) };

        let dma_mem_pool = MemPool::new(dma_data_mem);

        #[cfg(target_arch = "arm")]
        {
            // We never need the upper 32 bits of the configuration words on
            // 32-bit arm, so there is no point setting them at run time.
            // We zero them here though just to make sure nothing strange
            // happens.
            unichannel_common
                .raw_device
                .write_reg(Registers::S2mmSGCurrentDescriptorMsw, 0u32);
            unichannel_common
                .raw_device
                .write_reg(Registers::Mm2sSGCurrentDescriptorMsw, 0u32);
        }

        Ok(Device {
            unichannel_common,
            dma_descriptor_pool,
            dma_mem_pool,
            dma_data_phys_addr,
            file,
            bus_width,
            #[cfg(feature = "async")]
            async_file: None,
        })
    }

    /// A method to create a new regular blocks operation.
    ///
    /// * `block_size` - The size of each block in the memory.
    /// * `block_stride` - The offset difference between index 0 of block n and
    /// index 0 of block n+1 in the memory.
    /// * `n_blocks` - The number of blocks required.
    /// * `offset` - The offset in memory of index 0 of block 0.
    ///
    /// Memory claimed using this operation will be positioned at offset and
    /// span up to the end of the last block.
    ///
    /// Note: This method creates one descriptor per block so `n_blocks`
    /// should equal the number of AXIS packets you want to read from/write to
    /// memory.
    ///
    /// Note: For MM2S, `block_size` should equal the size of the AXIS packets
    /// you want to read from memory.
    ///
    /// Note: For S2MM, `block_size` should be greater than or equal to the size
    /// of the largest AXIS packet you want to write to memory.
    pub fn new_regular_blocks_operation(
        &mut self,
        block_size: usize,
        block_stride: usize,
        n_blocks: usize,
        offset: usize,
    ) -> Result<Operation, DeviceError> {
        let (descriptor_block, transfer_bytes, containing_memory) =
            write_regular_blocks_descriptor_chain(
                self,
                block_size,
                block_stride,
                n_blocks,
                offset,
                self.bus_width,
            )?;

        let descriptor_functions = DescriptorFunctions {
            descriptor_status,
            refresh_descriptor,
        };

        Ok(unsafe {
            Operation::new(
                descriptor_block,
                descriptor_functions,
                transfer_bytes,
                containing_memory,
            )?
        })
    }

    /// Implements the s2mm do_dma
    fn do_dma_s2mm(&mut self, operation: &Operation) -> Result<(), DeviceError> {
        // SETUP THE DMA CONFIG REGISTER FOR THE RUN
        // Set:
        //     run bit
        //     interrupt on complete bit
        //     interrupt on error bit
        //     Set Interrupt on Complete threshold to n_descriptors so the
        //         DMA engine waits for all descriptors to complete before
        //         sending the interrupt
        // All other config bits are set to 0, except IRQThreshold, which
        // implicitly acquires the default value of 1.
        let control_word = ControlRegister::RunStop.bitmask()
            | ControlRegister::IocIrqEn.bitmask()
            | ControlRegister::ErrIrqEn.bitmask()
            | (operation.n_descriptors << ControlRegister::IrqThreshold.offset()) as u32;

        enable_interrupts(&mut self.file)?;

        // write the current descriptor (i.e. the start descriptor)
        self.unichannel_common.write_reg(
            Registers::S2mmSGCurrentDescriptorLsw,
            operation.first_descriptor_lsw,
        );

        // Only need to write the upper 32 bits on 64-bit arch
        #[cfg(not(target_arch = "arm"))]
        {
            self.unichannel_common.write_reg(
                Registers::S2mmSGCurrentDescriptorMsw,
                operation.first_descriptor_msw,
            );
        }

        // This must be set _after_ the current descriptor is set. Once the
        // run-stop bit is set, the current descriptor registers are
        // read-only.
        self.unichannel_common
            .write_reg(Registers::S2mmControlRegister, control_word);

        // BEGIN THE OPERATION
        // writing the address of the last descriptor triggers the transfer.
        self.unichannel_common.write_reg(
            Registers::S2mmSGTailDescriptorLsw,
            operation.last_descriptor_lsw,
        );

        // Only need to write the upper 32 bits on 64-bit arch
        #[cfg(not(target_arch = "arm"))]
        {
            self.unichannel_common.write_reg(
                Registers::S2mmSGTailDescriptorMsw,
                operation.last_descriptor_msw,
            );
        }

        Ok(())
    }

    /// Implements the mm2s do_dma
    fn do_dma_mm2s(&mut self, operation: &Operation) -> Result<(), DeviceError> {
        // SETUP THE DMA CONFIG REGISTER FOR THE RUN
        // Set:
        //     run bit
        //     interrupt on complete bit
        //     interrupt on error bit
        //     Set Interrupt on Complete threshold to n_descriptors so the
        //         DMA engine waits for all descriptors to complete before
        //         sending the interrupt
        // All other config bits are set to 0, except IRQThreshold, which
        // implicitly acquires the default value of 1.
        let control_word = ControlRegister::RunStop.bitmask()
            | ControlRegister::IocIrqEn.bitmask()
            | ControlRegister::ErrIrqEn.bitmask()
            | (operation.n_descriptors << ControlRegister::IrqThreshold.offset()) as u32;

        enable_interrupts(&mut self.file)?;

        // write the current descriptor (i.e. the start descriptor)
        self.unichannel_common.write_reg(
            Registers::Mm2sSGCurrentDescriptorLsw,
            operation.first_descriptor_lsw,
        );

        // Only need to write the upper 32 bits on 64-bit arch
        #[cfg(not(target_arch = "arm"))]
        {
            self.unichannel_common.write_reg(
                Registers::Mm2sSGCurrentDescriptorMsw,
                operation.first_descriptor_msw,
            );
        }

        // This must be set _after_ the current descriptor is set. Once the
        // run-stop bit is set, the current descriptor registers are
        // read-only.
        self.unichannel_common
            .write_reg(Registers::Mm2sControlRegister, control_word);

        // BEGIN THE OPERATION
        // writing the address of the last descriptor triggers the transfer.
        self.unichannel_common.write_reg(
            Registers::Mm2sSGTailDescriptorLsw,
            operation.last_descriptor_lsw,
        );

        // Only need to write the upper 32 bits on 64-bit arch
        #[cfg(not(target_arch = "arm"))]
        {
            self.unichannel_common.write_reg(
                Registers::Mm2sSGTailDescriptorMsw,
                operation.last_descriptor_msw,
            );
        }

        Ok(())
    }

    /// Perform a scatter-gather DMA transfer.
    ///
    /// If successful this function returns a [RunningDevice], which can be
    /// waited on for completion of the DMA.
    pub fn do_dma(
        mut self,
        mut operation: Operation,
    ) -> Result<RunningDevice, ScatterGatherTransferError> {
        if !operation.descriptors_from(&self.dma_descriptor_pool) {
            Err(ScatterGatherTransferError {
                source: DeviceError::WrongDevice,
                errored_device: (self, operation).into(),
            })
        } else {
            let mut do_dma_inner = |operation: &mut Operation| {
                let mem_block = self.get_memory_from_operation(operation)?;

                match self.unichannel_common.transfer_type {
                    TransferType::MM2S => self.do_dma_mm2s(operation)?,
                    TransferType::S2MM => self.do_dma_s2mm(operation)?,
                };

                Ok(mem_block)
            };

            match do_dma_inner(&mut operation) {
                Ok(mem_block) => Ok(RunningDevice {
                    device: self,
                    operation,
                    mem_block,
                }),
                Err(e) => Err(ScatterGatherTransferError {
                    source: e,
                    errored_device: (self, operation).into(),
                }),
            }
        }
    }

    /// Performs a multi-channel scatter-gather DMA transfer in which the
    /// operation has pre-claimed the memory block. This is useful for
    /// MM2S transfers in which you want an unbroken ownership chain of the
    /// memory.
    ///
    /// Beyond the signature, it behaves exactly like [do_dma].
    pub fn do_dma_with_mem(
        mut self,
        operation_with_mem: OperationWithMemory<Operation>,
    ) -> Result<RunningDevice, ScatterGatherTransferError> {
        let (mut operation, mem_block): (Operation, MemBlock) = operation_with_mem.into();

        let mut do_dma_inner = |operation: &mut Operation| {
            if !operation.descriptors_from(&self.dma_descriptor_pool) {
                Err(DeviceError::WrongDevice)
            } else {
                match self.unichannel_common.transfer_type {
                    TransferType::MM2S => self.do_dma_mm2s(operation)?,
                    TransferType::S2MM => self.do_dma_s2mm(operation)?,
                }
                Ok(())
            }
        };

        match do_dma_inner(&mut operation) {
            Ok(_) => Ok(RunningDevice {
                device: self,
                operation,
                mem_block,
            }),
            Err(e) => Err(ScatterGatherTransferError {
                source: e,
                errored_device: (self, operation).into(),
            }),
        }
    }

    /// Return a memory block from the device memory pool.
    ///
    /// It is not possible to have live overlapping memory blocks. Until
    /// a memory block is freed, this method will error if an overlapping
    /// block is requested.
    ///
    /// Once the memory block is dropped, it relinquishes control of that
    /// region of memory.
    ///
    /// The returned memory block is both [Send] and [Sync] so can be passed
    /// around as needed.
    ///
    /// Note that when [do_dma] is called, it will first attempt to get hold
    /// of the necessary bit of memory, which will cause an error if the
    /// user owns an overlapping block.
    pub fn get_memory(&mut self, offset: usize, size: usize) -> Result<MemBlock, DeviceError> {
        let layout = MemBlockLayout { offset, size };
        Ok(self.dma_mem_pool.check_out_block(layout)?)
    }

    /// Returns the total size of the device memory pool.
    ///
    /// This number has nothing to do with the size of the blocks that can be
    /// checked out at this moment in time, which depends on what other blocks
    /// have been checked out. It is literally the total size of the memory
    /// pool.
    pub fn memory_pool_size(&self) -> usize {
        self.dma_mem_pool.size()
    }

    /// Returns the number of unused descriptors available on this device.
    pub fn n_available_descriptors(&self) -> Result<usize, DeviceError> {
        Ok(self.dma_descriptor_pool.descriptors_available())
    }

    /// This function prints the status of the device in human readable form.
    pub fn print_status(&self) {
        self.unichannel_common.print_status();
    }
}

impl MemoryFromOperation for Device {
    fn get_memory_from_operation<T: ScatterGatherOperation>(
        &mut self,
        operation: &T,
    ) -> Result<MemBlock, DeviceError> {
        if !operation.descriptors_from(&self.dma_descriptor_pool) {
            Err(DeviceError::WrongDevice)
        } else {
            Ok(self
                .dma_mem_pool
                .check_out_block(operation.containing_memory())?)
        }
    }
}

#[cfg(all(test, target_arch = "arm"))]
mod tests {
    use rand::{distributions::Standard, rngs::SmallRng, Rng, SeedableRng};
    use serial_test::serial;
    use std::{ops::Range, path::PathBuf};

    use super::{
        Device, DeviceError, ScatterGatherTransferError, MAX_BYTES_TRANSFER_PER_DESCRIPTOR,
    };

    use crate::BusDataWidth;
    use crate::descriptors::DescriptorError;
    use crate::operation::{MemoryFromOperation, ScatterGatherOperation};
    use crate::scatter_gather::ScatterGatherError;

    /// The `dma::scatter_gather::device::new` function should return an
    /// UnsupportedDirect error if the underlying device is a direct DMA
    /// device.
    #[test]
    #[serial]
    fn test_unsupported_direct() {
        let device_path: PathBuf = ["/dev", "axi_dma_mm2s"].iter().collect();

        let device = Device::new(&device_path, BusDataWidth::FourBytes);

        match device {
            Err(DeviceError::UnsupportedDirect) => (),
            _ => panic!("Did not get DeviceError::UnsupportedDirect error."),
        }
    }

    /// The `dma::scatter_gather::device::new` function should return an
    /// UnsupportedCapability error if the underlying device is capable of
    /// performing both MM2S and S2MM.
    #[test]
    #[serial]
    fn test_unsupported_capability() {
        let device_path: PathBuf = ["/dev", "axi_dma_sg_combined"].iter().collect();

        let device = Device::new(&device_path, BusDataWidth::FourBytes);

        match device {
            Err(DeviceError::UnsupportedCapability) => (),
            _ => panic!("Did not get DeviceError::UnsupportedCapability error."),
        }
    }

    /// The `dma::scatter_gather::device::new` function should return an
    /// UnsupportedMultiChannel error if the underlying device is a multi
    /// channel device.
    #[test]
    #[serial]
    fn test_unsupported_multi_channel() {
        let device_path: PathBuf = ["/dev", "axi_dma_mc_mm2s"].iter().collect();

        let device = Device::new(&device_path, BusDataWidth::FourBytes);

        match device {
            Err(DeviceError::UnsupportedMultiChannel) => (),
            _ => panic!("Did not get DeviceError::UnsupportedMultiChannel error."),
        }
    }

    /// The `dma::scatter_gather::device::new_regular_blocks_operation`
    /// function should check that the `block_size` argument is in the range
    /// 8 -> 2**23 and that it is a multiple of 8. If either of these is not
    /// true then it should return a `InvalidSize` error.
    #[test]
    #[serial]
    fn test_invalid_size() {
        // Create the device
        let device_path: PathBuf = ["/dev", "axi_dma_sg_mm2s"].iter().collect();
        let mut device = Device::new(&device_path, BusDataWidth::FourBytes).unwrap();

        let mut rng = SmallRng::from_entropy();

        let block_size: usize = loop {
            // Generate a random block_size
            let block_size: usize = rng.gen();

            if block_size < 8 || block_size >= 1 << 23 || block_size % 8 != 0 {
                // Check that block_size is invalid, if it is then return it
                break block_size;
            }
        };

        let block_stride = block_size;
        let n_blocks = 1;
        let offset: usize = 0;

        let operation =
            device.new_regular_blocks_operation(block_size, block_stride, n_blocks, offset);

        match operation {
            Err(DeviceError::ScatterGather {
                source: ScatterGatherError::InvalidSize,
            }) => (),
            _ => panic!("Did not get InvalidSize error."),
        }
    }

    /// The `dma::scatter_gather::device::new_regular_blocks_operation`
    /// function should check that the `offset` argument is in the range
    /// 0 -> `memory_pool_size()` and that it is a multiple of 8. If either of
    /// these is not true then it should return a `InvalidOffset`
    /// error.
    #[test]
    #[serial]
    fn test_invalid_offset() {
        // Create the device
        let device_path: PathBuf = ["/dev", "axi_dma_sg_mm2s"].iter().collect();
        let mut device = Device::new(&device_path, BusDataWidth::FourBytes).unwrap();

        let block_size: usize = 16;
        let block_stride = block_size;
        let n_blocks = 1;
        let mut rng = SmallRng::from_entropy();

        let offset: usize = loop {
            // Generate a random offset
            let offset: usize = rng.gen();

            if offset >= device.memory_pool_size() || offset % 8 != 0 {
                // Check that offset is invalid, if it is then return it
                break offset;
            }
        };

        let operation =
            device.new_regular_blocks_operation(block_size, block_stride, n_blocks, offset);

        match operation {
            Err(DeviceError::ScatterGather {
                source: ScatterGatherError::InvalidOffset,
            }) => (),
            _ => panic!("Did not get InvalidOffset error."),
        }
    }

    /// The `dma::scatter_gather::device::new_regular_blocks_operation`
    /// function should check that the `block_stride` argument is in the range
    /// block_size -> `memory_pool_size()` and that it is a multiple of 8. If
    /// either of these is not true then it should return a
    /// `InvalidStride` error.
    #[test]
    #[serial]
    fn test_invalid_block_stride() {
        // Create the device
        let device_path: PathBuf = ["/dev", "axi_dma_sg_mm2s"].iter().collect();
        let mut device = Device::new(&device_path, BusDataWidth::FourBytes).unwrap();

        let block_size: usize = 1024;
        let offset: usize = 0;
        let n_blocks = 1;
        let mut rng = SmallRng::from_entropy();

        let block_stride: usize = loop {
            // Generate a random block stride
            let block_stride: usize = rng.gen();

            if block_stride < block_size
                || block_stride >= device.memory_pool_size()
                || block_stride % 8 != 0
            {
                // Check that block_stride is invalid, if it is then
                // return it
                break block_stride;
            }
        };

        let operation =
            device.new_regular_blocks_operation(block_size, block_stride, n_blocks, offset);

        match operation {
            Err(DeviceError::ScatterGather {
                source: ScatterGatherError::InvalidStride,
            }) => (),
            _ => panic!("Did not get InvalidStride error."),
        }
    }

    /// The `dma::scatter_gather::device::new_regular_blocks_operation`
    /// function should check that the `n_blocks` argument does not exceed the
    /// maximum possible number of blocks given the block_stride. If it does
    /// then the function should return a `InvalidNBlocks` error.
    #[test]
    #[serial]
    fn test_invalid_n_blocks_too_large() {
        // Create the device
        let device_path: PathBuf = ["/dev", "axi_dma_sg_mm2s"].iter().collect();
        let mut device = Device::new(&device_path, BusDataWidth::FourBytes).unwrap();

        let block_size: usize = 1024;
        let offset: usize = 0;

        // Generate a random block stride
        let mut rng = SmallRng::from_entropy();

        let block_stride: usize = 8 * rng.gen_range(block_size / 8..device.memory_pool_size() / 8);

        let min_n_blocks = device.memory_pool_size() / block_stride + 1;

        let n_blocks = rng.gen_range(min_n_blocks..min_n_blocks * 2);

        let operation =
            device.new_regular_blocks_operation(block_size, block_stride, n_blocks, offset);

        match operation {
            Err(DeviceError::ScatterGather {
                source: ScatterGatherError::InvalidNBlocks,
            }) => (),
            _ => panic!("Did not get InvalidNBlocks error."),
        }
    }

    /// The `dma::scatter_gather::device::new_regular_blocks_operation`
    /// function should check that the `n_blocks` argument is not zero.
    /// If it is then the function should return a `InvalidNBlocks` error.
    #[test]
    #[serial]
    fn test_invalid_n_blocks_zero() {
        // Create the device
        let device_path: PathBuf = ["/dev", "axi_dma_sg_mm2s"].iter().collect();
        let mut device = Device::new(&device_path, BusDataWidth::FourBytes).unwrap();

        let block_size: usize = 1024;
        let offset: usize = 0;

        // Generate a random block stride
        let mut rng = SmallRng::from_entropy();

        let block_stride: usize = 8 * rng.gen_range(block_size / 8..device.memory_pool_size() / 8);

        let n_blocks = 0;

        let operation =
            device.new_regular_blocks_operation(block_size, block_stride, n_blocks, offset);

        match operation {
            Err(DeviceError::ScatterGather {
                source: ScatterGatherError::InvalidNBlocks,
            }) => (),
            _ => panic!("Did not get InvalidNBlocks error."),
        }
    }

    /// The `dma::uni_channel::device::new_regular_blocks_operation`
    /// function should return a DescriptorError() error if the
    /// device does not have sufficient descriptor chunks available to set up
    /// the requested operation.
    #[test]
    #[serial]
    fn test_insufficient_descriptors() {
        let device_path: PathBuf = ["/dev", "axi_dma_sg_mm2s"].iter().collect();

        let mut device = Device::new(&device_path, BusDataWidth::FourBytes).unwrap();
        let pool = &mut device.dma_descriptor_pool;

        // Set the block stride so that the blocks won't overlap
        let block_size = 1024;
        let block_stride = block_size;
        let offset = 0;

        let descriptors_available = pool.descriptors_available();

        // We check out every descriptor so we don't have enough.
        {
            // It's one descriptor per block, so this will use all the
            // descriptors and should not panic
            let _ops = device
                .new_regular_blocks_operation(
                    block_size,
                    block_stride,
                    descriptors_available,
                    offset,
                )
                .unwrap();

            let operation_res =
                device.new_regular_blocks_operation(block_size, block_stride, 1, offset);

            match operation_res {
                Err(DeviceError::ScatterGather {
                    source:
                        ScatterGatherError::Descriptor {
                            source: DescriptorError::InsufficientDescriptors,
                        },
                }) => (),
                _ => panic!("Did not get InsufficientDescriptors error."),
            }
        }

        // Now we should be able to check some out again because the handle has
        // been dropped.
        let _ops = device.new_regular_blocks_operation(block_size, block_stride, 1, offset);

        let _more_ops = device.new_regular_blocks_operation(
            block_size,
            block_stride,
            descriptors_available - 1,
            offset,
        );

        // And again, this should fail.
        let operation_res =
            device.new_regular_blocks_operation(block_size, block_stride, 1, offset);

        match operation_res {
            Err(DeviceError::ScatterGather {
                source:
                    ScatterGatherError::Descriptor {
                        source: DescriptorError::InsufficientDescriptors,
                    },
            }) => (),
            _ => panic!("Did not get InsufficientDescriptors error."),
        }
    }
    /// The `dma::scatter_gather::device::new_regular_blocks_operation`
    /// function should check that the `block_size`, `block_stride`,
    /// `n_blocks` and `offset` arguments will not cause a transfer to memory
    /// outside `dma_data`. If they do then it would cause a memory overflow.
    /// The `dma::device::new_regular_blocks_operation` function should
    /// prevent this by returning a `DeviceError::MemoryOverflow` error.
    #[test]
    #[serial]
    fn test_memory_overflow() {
        // Create the device
        let device_path: PathBuf = ["/dev", "axi_dma_sg_mm2s"].iter().collect();
        let mut device = Device::new(&device_path, BusDataWidth::FourBytes).unwrap();
        let mut rng = SmallRng::from_entropy();

        let (block_size, block_stride, n_blocks, offset) = loop {
            // Randomly select arguments
            let block_size = 8 * rng.gen_range(1..MAX_BYTES_TRANSFER_PER_DESCRIPTOR / 8 - 1);
            let block_stride: usize =
                8 * rng.gen_range(block_size / 8..device.memory_pool_size() / 8);

            let max_n_blocks = device.memory_pool_size() / block_stride;
            let n_blocks = rng.gen_range(1..max_n_blocks + 1);

            let offset: usize = 8 * rng.gen_range(0..device.memory_pool_size() / 8);

            if (offset + block_stride * (n_blocks - 1) + block_size) > device.memory_pool_size() {
                // Check that nbytes and offset would result in a memory
                // overflow, if it would then return it
                break (block_size, block_stride, n_blocks, offset);
            }
        };

        let operation =
            device.new_regular_blocks_operation(block_size, block_stride, n_blocks, offset);

        match operation {
            Err(DeviceError::ScatterGather {
                source: ScatterGatherError::MemoryOverflow,
            }) => (),
            _ => panic!("Did not get MemoryOverflow error."),
        }
    }

    /// The `dma::scatter_gather::device::do_dma` function
    /// should check that the `operation` argument was created by the device
    /// which is being called on to run the DMA. If it was not then it should
    /// return a `DeviceError::WrongDevice` error.
    #[test]
    #[serial]
    fn test_do_dma_wrong_device() {
        let block_size: usize = 1024;
        let block_stride = block_size;
        let offset: usize = 0;
        let n_blocks = 1;

        // Create device 0
        let device_0_path: PathBuf = ["/dev", "axi_dma_sg_mm2s"].iter().collect();
        let device_0 = Device::new(&device_0_path, BusDataWidth::FourBytes).unwrap();

        // Create device 1
        let device_1_path: PathBuf = ["/dev", "axi_dma_sg_s2mm"].iter().collect();
        let mut device_1 = Device::new(&device_1_path, BusDataWidth::FourBytes).unwrap();

        let operation_1 = device_1
            .new_regular_blocks_operation(block_size, block_stride, n_blocks, offset)
            .unwrap();

        // Try to run do dma on an operation which was not created by the
        // device being called
        let ScatterGatherTransferError {
            source: do_dma_err,
            errored_device: _,
        } = device_0.do_dma(operation_1).unwrap_err();

        match do_dma_err {
            DeviceError::WrongDevice => (),
            _ => panic!(
                "Did not get DeviceError::WrongDevice error. Got: {:?}",
                do_dma_err
            ),
        }
    }

    /// When an operation is combined with memory it should be accessible as
    /// though it was the separate memory.
    #[test]
    #[serial]
    fn test_operation_with_memory() {
        // Create device 0
        let device_path: PathBuf = ["/dev", "axi_dma_sg_mm2s"].iter().collect();
        let mut device = Device::new(&device_path, BusDataWidth::FourBytes).unwrap();

        let operation = device
            .new_regular_blocks_operation(1024, 2048, 2, 0)
            .unwrap();

        let mut rng = SmallRng::from_entropy();
        let mut ref_data: Vec<u8> = (&mut rng)
            .sample_iter(&Standard)
            .take(device.memory_pool_size())
            .collect();

        let ref_slicer: Range<usize> = operation.containing_memory().into();
        {
            let mut mem = device.get_memory(0, device.memory_pool_size()).unwrap();
            mem.copy_from_slice(&ref_data);
        }

        {
            let mem = device.get_memory_from_operation(&operation).unwrap();
            assert_eq!(*mem, *ref_data.get(ref_slicer.clone()).unwrap());
        }

        let operation = {
            let op_mem = operation.claim_memory(&mut device).unwrap();
            assert_eq!(*op_mem, *ref_data.get(ref_slicer.clone()).unwrap());

            let (operation, memory) = op_mem.into();

            assert_eq!(*memory, *ref_data.get(ref_slicer.clone()).unwrap());
            operation
        };

        // Write through one way of getting the memory
        {
            let mut mem = device.get_memory_from_operation(&operation).unwrap();
            let mem_len = mem.len();
            mem.copy_from_slice(&vec![0u8; mem_len]);
        }

        // And then through another way.
        {
            let mut op_mem = operation.claim_memory(&mut device).unwrap();
            let mem_len = op_mem.len();
            op_mem.copy_from_slice(&vec![1u8; mem_len]);

            // Update the reference
            let sliced_ref_data = ref_data.get_mut(ref_slicer.clone()).unwrap();
            sliced_ref_data.copy_from_slice(&vec![1u8; mem_len]);
        }

        // Finally check the memory is still consistent
        let mem = device.get_memory(0, device.memory_pool_size()).unwrap();
        assert_eq!(*mem, *ref_data);
    }
}
