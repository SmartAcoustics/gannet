use crate::multi_channel::Channel;
use crate::{RegisterCheck, RegisterLayout};
use strum_macros::EnumIter;

pub(crate) struct ChannelRegister {
    offset: usize,
}

impl RegisterLayout for ChannelRegister {
    type RegisterType = u32;

    #[inline]
    fn offset(&self) -> usize {
        self.offset
    }
}

// Register descriptions for the Xilinx AXI MCDMA LogiCORE IP.
// See Xilinx product guide pg288 for more information on the register space.
//
// In the LogiCORE IP AXI multichannel DMA block the configuration registers
// are 32 bits wide. Each 32 bit configuration register includes **four
// individually addressable** (8 bit wide) memory locations. In Libgannet, the
// configuration registers are treated as 32 bits wide therefore:
//
// Libgannet addr (32 bit words) | Axi DMA addr (8 bit words) | Register name
// ----------------------------- | -------------------------- | -------------
// 0	                         | 0	  0x000	              | Mm2sCommonControl
// 1	                         | 4	  0x004	              | Mm2sCommonStatus
// 2	                         | 8	  0x008	              | Mm2sChannelEnable
// 3	                         | 12	  0x00C	              | Mm2sChannelInProgress
// 4	                         | 16	  0x010	              | Mm2sError
// 5	                         | 20	  0x014	              | Mm2sChannelSchedulerType
// 6	                         | 24	  0x018	              | Mm2sLowerChannelsWeight
// 7	                         | 28	  0x01C	              | Mm2sUpperChannelWeight
// 8	                         | 32	  0x020	              | Mm2sChannelsComplete
// 9	                         | 36	  0x024	              | Mm2sARCacheARUser
// 10	                         | 40	  0x028	              | Mm2sInterruptStatus
// 11	                         | 44	  0x02C	              | Reserved
// 12	                         | 48	  0x030	              | Reserved
// 13	                         | 52	  0x034	              | Reserved
// 14	                         | 56	  0x038	              | Reserved
// 15	                         | 60	  0x03C	              | Reserved
// 16	                         | 64	  0x040	              | Mm2sCh0Control
// 17	                         | 68	  0x044	              | Mm2sCh0Status
// 18	                         | 72	  0x048	              | Mm2sCh0CurrentDescriptorLsw
// 19	                         | 76	  0x04C	              | Mm2sCh0CurrentDescriptorMsw
// 20	                         | 80	  0x050	              | Mm2sCh0TailDescriptorLsw
// 21	                         | 84	  0x054	              | Mm2sCh0TailDescriptorMsw
// 22	                         | 88	  0x058	              | Mm2sCh0PacketCount
// 23	                         | 92	  0x05C	              | Reserved
// 24	                         | 96	  0x060	              | Reserved
// 25	                         | 100	  0x064	              | Reserved
// 26	                         | 104	  0x068	              | Reserved
// 27	                         | 108	  0x06C	              | Reserved
// 28	                         | 112	  0x070	              | Reserved
// 29	                         | 116	  0x074	              | Reserved
// 30	                         | 120	  0x078	              | Reserved
// 31	                         | 124	  0x07C	              | Reserved
// 32	                         | 128	  0x080	              | Mm2sCh1Control
// 33	                         | 132	  0x084	              | Mm2sCh1Status
// 34	                         | 136	  0x088	              | Mm2sCh1CurrentDescriptorLsw
// 35	                         | 140	  0x08C	              | Mm2sCh1CurrentDescriptorMsw
// 36	                         | 144	  0x090	              | Mm2sCh1TailDescriptorLsw
// 37	                         | 148	  0x094	              | Mm2sCh1TailDescriptorMsw
// 38	                         | 152	  0x098	              | Mm2sCh1PacketCount
// 39	                         | 156	  0x09C	              | Reserved
// 40	                         | 160	  0x0A0	              | Reserved
// 41	                         | 164	  0x0A4	              | Reserved
// 42	                         | 168	  0x0A8	              | Reserved
// 43	                         | 172	  0x0AC	              | Reserved
// 44	                         | 176	  0x0B0	              | Reserved
// 45	                         | 180	  0x0B4	              | Reserved
// 46	                         | 184	  0x0B8	              | Reserved
// 47	                         | 188	  0x0BC	              | Reserved
// 48	                         | 192	  0x0C0	              | Mm2sCh2Control
// 49	                         | 196	  0x0C4	              | Mm2sCh2Status
// 50	                         | 200	  0x0C8	              | Mm2sCh2CurrentDescriptorLsw
// 51	                         | 204	  0x0CC	              | Mm2sCh2CurrentDescriptorMsw
// 52	                         | 208	  0x0D0	              | Mm2sCh2TailDescriptorLsw
// 53	                         | 212	  0x0D4	              | Mm2sCh2TailDescriptorMsw
// 54	                         | 216	  0x0D8	              | Mm2sCh2PacketCount
// 55	                         | 220	  0x0DC	              | Reserved
// 56	                         | 224	  0x0E0	              | Reserved
// 57	                         | 228	  0x0E4	              | Reserved
// 58	                         | 232	  0x0E8	              | Reserved
// 59	                         | 236	  0x0EC	              | Reserved
// 60	                         | 240	  0x0F0	              | Reserved
// 61	                         | 244	  0x0F4	              | Reserved
// 62	                         | 248	  0x0F8	              | Reserved
// 63	                         | 252	  0x0FC	              | Reserved
// 64	                         | 256	  0x100	              | Mm2sCh3Control
// 65	                         | 260	  0x104	              | Mm2sCh3Status
// 66	                         | 264	  0x108	              | Mm2sCh3CurrentDescriptorLsw
// 67	                         | 268	  0x10C	              | Mm2sCh3CurrentDescriptorMsw
// 68	                         | 272	  0x110	              | Mm2sCh3TailDescriptorLsw
// 69	                         | 276	  0x114	              | Mm2sCh3TailDescriptorMsw
// 70	                         | 280	  0x118	              | Mm2sCh3PacketCount
// 71	                         | 284	  0x11C	              | Reserved
// 72	                         | 288	  0x120	              | Reserved
// 73	                         | 292	  0x124	              | Reserved
// 74	                         | 296	  0x128	              | Reserved
// 75	                         | 300	  0x12C	              | Reserved
// 76	                         | 304	  0x130	              | Reserved
// 77	                         | 308	  0x134	              | Reserved
// 78	                         | 312	  0x138	              | Reserved
// 79	                         | 316	  0x13C	              | Reserved
// 80	                         | 320	  0x140	              | Mm2sCh4Control
// 81	                         | 324	  0x144	              | Mm2sCh4Status
// 82	                         | 328	  0x148	              | Mm2sCh4CurrentDescriptorLsw
// 83	                         | 332	  0x14C	              | Mm2sCh4CurrentDescriptorMsw
// 84	                         | 336	  0x150	              | Mm2sCh4TailDescriptorLsw
// 85	                         | 340	  0x154	              | Mm2sCh4TailDescriptorMsw
// 86	                         | 344	  0x158	              | Mm2sCh4PacketCount
// 87	                         | 348	  0x15C	              | Reserved
// 88	                         | 352	  0x160	              | Reserved
// 89	                         | 356	  0x164	              | Reserved
// 90	                         | 360	  0x168	              | Reserved
// 91	                         | 364	  0x16C	              | Reserved
// 92	                         | 368	  0x170	              | Reserved
// 93	                         | 372	  0x174	              | Reserved
// 94	                         | 376	  0x178	              | Reserved
// 95	                         | 380	  0x17C	              | Reserved
// 96	                         | 384	  0x180	              | Mm2sCh5Control
// 97	                         | 388	  0x184	              | Mm2sCh5Status
// 98	                         | 392	  0x188	              | Mm2sCh5CurrentDescriptorLsw
// 99	                         | 396	  0x18C	              | Mm2sCh5CurrentDescriptorMsw
// 100	                         | 400	  0x190	              | Mm2sCh5TailDescriptorLsw
// 101	                         | 404	  0x194	              | Mm2sCh5TailDescriptorMsw
// 102	                         | 408	  0x198	              | Mm2sCh5PacketCount
// 103	                         | 412	  0x19C	              | Reserved
// 104	                         | 416	  0x1A0	              | Reserved
// 105	                         | 420	  0x1A4	              | Reserved
// 106	                         | 424	  0x1A8	              | Reserved
// 107	                         | 428	  0x1AC	              | Reserved
// 108	                         | 432	  0x1B0	              | Reserved
// 109	                         | 436	  0x1B4	              | Reserved
// 110	                         | 440	  0x1B8	              | Reserved
// 111	                         | 444	  0x1BC	              | Reserved
// 112	                         | 448	  0x1C0	              | Mm2sCh6Control
// 113	                         | 452	  0x1C4	              | Mm2sCh6Status
// 114	                         | 456	  0x1C8	              | Mm2sCh6CurrentDescriptorLsw
// 115	                         | 460	  0x1CC	              | Mm2sCh6CurrentDescriptorMsw
// 116	                         | 464	  0x1D0	              | Mm2sCh6TailDescriptorLsw
// 117	                         | 468	  0x1D4	              | Mm2sCh6TailDescriptorMsw
// 118	                         | 472	  0x1D8	              | Mm2sCh6PacketCount
// 119	                         | 476	  0x1DC	              | Reserved
// 120	                         | 480	  0x1E0	              | Reserved
// 121	                         | 484	  0x1E4	              | Reserved
// 122	                         | 488	  0x1E8	              | Reserved
// 123	                         | 492	  0x1EC	              | Reserved
// 124	                         | 496	  0x1F0	              | Reserved
// 125	                         | 500	  0x1F4	              | Reserved
// 126	                         | 504	  0x1F8	              | Reserved
// 127	                         | 508	  0x1FC	              | Reserved
// 128	                         | 512	  0x200	              | Mm2sCh7Control
// 129	                         | 516	  0x204	              | Mm2sCh7Status
// 130	                         | 520	  0x208	              | Mm2sCh7CurrentDescriptorLsw
// 131	                         | 524	  0x20C	              | Mm2sCh7CurrentDescriptorMsw
// 132	                         | 528	  0x210	              | Mm2sCh7TailDescriptorLsw
// 133	                         | 532	  0x214	              | Mm2sCh7TailDescriptorMsw
// 134	                         | 536	  0x218	              | Mm2sCh7PacketCount
// 135	                         | 540	  0x21C	              | Reserved
// 136	                         | 544	  0x220	              | Reserved
// 137	                         | 548	  0x224	              | Reserved
// 138	                         | 552	  0x228	              | Reserved
// 139	                         | 556	  0x22C	              | Reserved
// 140	                         | 560	  0x230	              | Reserved
// 141	                         | 564	  0x234	              | Reserved
// 142	                         | 568	  0x238	              | Reserved
// 143	                         | 572	  0x23C	              | Reserved
// 144	                         | 576	  0x240	              | Mm2sCh8Control
// 145	                         | 580	  0x244	              | Mm2sCh8Status
// 146	                         | 584	  0x248	              | Mm2sCh8CurrentDescriptorLsw
// 147	                         | 588	  0x24C	              | Mm2sCh8CurrentDescriptorMsw
// 148	                         | 592	  0x250	              | Mm2sCh8TailDescriptorLsw
// 149	                         | 596	  0x254	              | Mm2sCh8TailDescriptorMsw
// 150	                         | 600	  0x258	              | Mm2sCh8PacketCount
// 151	                         | 604	  0x25C	              | Reserved
// 152	                         | 608	  0x260	              | Reserved
// 153	                         | 612	  0x264	              | Reserved
// 154	                         | 616	  0x268	              | Reserved
// 155	                         | 620	  0x26C	              | Reserved
// 156	                         | 624	  0x270	              | Reserved
// 157	                         | 628	  0x274	              | Reserved
// 158	                         | 632	  0x278	              | Reserved
// 159	                         | 636	  0x27C	              | Reserved
// 160	                         | 640	  0x280	              | Mm2sCh9Control
// 161	                         | 644	  0x284	              | Mm2sCh9Status
// 162	                         | 648	  0x288	              | Mm2sCh9CurrentDescriptorLsw
// 163	                         | 652	  0x28C	              | Mm2sCh9CurrentDescriptorMsw
// 164	                         | 656	  0x290	              | Mm2sCh9TailDescriptorLsw
// 165	                         | 660	  0x294	              | Mm2sCh9TailDescriptorMsw
// 166	                         | 664	  0x298	              | Mm2sCh9PacketCount
// 167	                         | 668	  0x29C	              | Reserved
// 168	                         | 672	  0x2A0	              | Reserved
// 169	                         | 676	  0x2A4	              | Reserved
// 170	                         | 680	  0x2A8	              | Reserved
// 171	                         | 684	  0x2AC	              | Reserved
// 172	                         | 688	  0x2B0	              | Reserved
// 173	                         | 692	  0x2B4	              | Reserved
// 174	                         | 696	  0x2B8	              | Reserved
// 175	                         | 700	  0x2BC	              | Reserved
// 176	                         | 704	  0x2C0	              | Mm2sCh10Control
// 177	                         | 708	  0x2C4	              | Mm2sCh10Status
// 178	                         | 712	  0x2C8	              | Mm2sCh10CurrentDescriptorLsw
// 179	                         | 716	  0x2CC	              | Mm2sCh10CurrentDescriptorMsw
// 180	                         | 720	  0x2D0	              | Mm2sCh10TailDescriptorLsw
// 181	                         | 724	  0x2D4	              | Mm2sCh10TailDescriptorMsw
// 182	                         | 728	  0x2D8	              | Mm2sCh10PacketCount
// 183	                         | 732	  0x2DC	              | Reserved
// 184	                         | 736	  0x2E0	              | Reserved
// 185	                         | 740	  0x2E4	              | Reserved
// 186	                         | 744	  0x2E8	              | Reserved
// 187	                         | 748	  0x2EC	              | Reserved
// 188	                         | 752	  0x2F0	              | Reserved
// 189	                         | 756	  0x2F4	              | Reserved
// 190	                         | 760	  0x2F8	              | Reserved
// 191	                         | 764	  0x2FC	              | Reserved
// 192	                         | 768	  0x300	              | Mm2sCh11Control
// 193	                         | 772	  0x304	              | Mm2sCh11Status
// 194	                         | 776	  0x308	              | Mm2sCh11CurrentDescriptorLsw
// 195	                         | 780	  0x30C	              | Mm2sCh11CurrentDescriptorMsw
// 196	                         | 784	  0x310	              | Mm2sCh11TailDescriptorLsw
// 197	                         | 788	  0x314	              | Mm2sCh11TailDescriptorMsw
// 198	                         | 792	  0x318	              | Mm2sCh11PacketCount
// 199	                         | 796	  0x31C	              | Reserved
// 200	                         | 800	  0x320	              | Reserved
// 201	                         | 804	  0x324	              | Reserved
// 202	                         | 808	  0x328	              | Reserved
// 203	                         | 812	  0x32C	              | Reserved
// 204	                         | 816	  0x330	              | Reserved
// 205	                         | 820	  0x334	              | Reserved
// 206	                         | 824	  0x338	              | Reserved
// 207	                         | 828	  0x33C	              | Reserved
// 208	                         | 832	  0x340	              | Mm2sCh12Control
// 209	                         | 836	  0x344	              | Mm2sCh12Status
// 210	                         | 840	  0x348	              | Mm2sCh12CurrentDescriptorLsw
// 211	                         | 844	  0x34C	              | Mm2sCh12CurrentDescriptorMsw
// 212	                         | 848	  0x350	              | Mm2sCh12TailDescriptorLsw
// 213	                         | 852	  0x354	              | Mm2sCh12TailDescriptorMsw
// 214	                         | 856	  0x358	              | Mm2sCh12PacketCount
// 215	                         | 860	  0x35C	              | Reserved
// 216	                         | 864	  0x360	              | Reserved
// 217	                         | 868	  0x364	              | Reserved
// 218	                         | 872	  0x368	              | Reserved
// 219	                         | 876	  0x36C	              | Reserved
// 220	                         | 880	  0x370	              | Reserved
// 221	                         | 884	  0x374	              | Reserved
// 222	                         | 888	  0x378	              | Reserved
// 223	                         | 892	  0x37C	              | Reserved
// 224	                         | 896	  0x380	              | Mm2sCh13Control
// 225	                         | 900	  0x384	              | Mm2sCh13Status
// 226	                         | 904	  0x388	              | Mm2sCh13CurrentDescriptorLsw
// 227	                         | 908	  0x38C	              | Mm2sCh13CurrentDescriptorMsw
// 228	                         | 912	  0x390	              | Mm2sCh13TailDescriptorLsw
// 229	                         | 916	  0x394	              | Mm2sCh13TailDescriptorMsw
// 230	                         | 920	  0x398	              | Mm2sCh13PacketCount
// 231	                         | 924	  0x39C	              | Reserved
// 232	                         | 928	  0x3A0	              | Reserved
// 233	                         | 932	  0x3A4	              | Reserved
// 234	                         | 936	  0x3A8	              | Reserved
// 235	                         | 940	  0x3AC	              | Reserved
// 236	                         | 944	  0x3B0	              | Reserved
// 237	                         | 948	  0x3B4	              | Reserved
// 238	                         | 952	  0x3B8	              | Reserved
// 239	                         | 956	  0x3BC	              | Reserved
// 240	                         | 960	  0x3C0	              | Mm2sCh14Control
// 241	                         | 964	  0x3C4	              | Mm2sCh14Status
// 242	                         | 968	  0x3C8	              | Mm2sCh14CurrentDescriptorLsw
// 243	                         | 972	  0x3CC	              | Mm2sCh14CurrentDescriptorMsw
// 244	                         | 976	  0x3D0	              | Mm2sCh14TailDescriptorLsw
// 245	                         | 980	  0x3D4	              | Mm2sCh14TailDescriptorMsw
// 246	                         | 984	  0x3D8	              | Mm2sCh14PacketCount
// 247	                         | 988	  0x3DC	              | Reserved
// 248	                         | 992	  0x3E0	              | Reserved
// 249	                         | 996	  0x3E4	              | Reserved
// 250	                         | 1000  0x3E8	              | Reserved
// 251	                         | 1004  0x3EC	              | Reserved
// 252	                         | 1008  0x3F0	              | Reserved
// 253	                         | 1012  0x3F4	              | Reserved
// 254	                         | 1016  0x3F8	              | Reserved
// 255	                         | 1020  0x3FC	              | Reserved
// 256	                         | 1024  0x400	              | Mm2sCh15Control
// 257	                         | 1028  0x404	              | Mm2sCh15Status
// 258	                         | 1032  0x408	              | Mm2sCh15CurrentDescriptorLsw
// 259	                         | 1036  0x40C	              | Mm2sCh15CurrentDescriptorMsw
// 260	                         | 1040  0x410	              | Mm2sCh15TailDescriptorLsw
// 261	                         | 1044  0x414	              | Mm2sCh15TailDescriptorMsw
// 262	                         | 1048  0x418	              | Mm2sCh15PacketCount
// 263	                         | 1052  0x41C	              | Reserved
// 264	                         | 1056  0x420	              | Reserved
// 265	                         | 1060  0x424	              | Reserved
// 266	                         | 1064  0x428	              | Reserved
// 267	                         | 1068  0x42C	              | Reserved
// 268	                         | 1072  0x430	              | Reserved
// 269	                         | 1076  0x434	              | Reserved
// 270	                         | 1080  0x438	              | Reserved
// 271	                         | 1084  0x43C	              | Reserved
// 272	                         | 1088  0x440	              | Mm2sChannelObserver1
// 273	                         | 1092  0x444	              | Mm2sChannelObserver2
// 274	                         | 1096  0x448	              | Mm2sChannelObserver3
// 275	                         | 1100  0x44C	              | Mm2sChannelObserver4
// 276	                         | 1104  0x450	              | Mm2sChannelObserver5
// 277	                         | 1108  0x454	              | Mm2sChannelObserver6
// 278	                         | 1112  0x458	              | Reserved
// 279	                         | 1116  0x45C	              | Reserved
// 280	                         | 1120  0x460	              | Reserved
// 281	                         | 1124  0x464	              | Reserved
// 282	                         | 1128  0x468	              | Reserved
// 283	                         | 1132  0x46C	              | Reserved
// 284	                         | 1136  0x470	              | Reserved
// 285	                         | 1140  0x474	              | Reserved
// 286	                         | 1144  0x478	              | Reserved
// 287	                         | 1148  0x47C	              | Reserved
// 288	                         | 1152  0x480	              | Reserved
// 289	                         | 1156  0x484	              | Reserved
// 290	                         | 1160  0x488	              | Reserved
// 291	                         | 1164  0x48C	              | Reserved
// 292	                         | 1168  0x490	              | Reserved
// 293	                         | 1172  0x494	              | Reserved
// 294	                         | 1176  0x498	              | Reserved
// 295	                         | 1180  0x49C	              | Reserved
// 296	                         | 1184  0x4A0	              | Reserved
// 297	                         | 1188  0x4A4	              | Reserved
// 298	                         | 1192  0x4A8	              | Reserved
// 299	                         | 1196  0x4AC	              | Reserved
// 300	                         | 1200  0x4B0	              | Reserved
// 301	                         | 1204  0x4B4	              | Reserved
// 302	                         | 1208  0x4B8	              | Reserved
// 303	                         | 1212  0x4BC	              | Reserved
// 304	                         | 1216  0x4C0	              | Reserved
// 305	                         | 1220  0x4C4	              | Reserved
// 306	                         | 1224  0x4C8	              | Reserved
// 307	                         | 1228  0x4CC	              | Reserved
// 308	                         | 1232  0x4D0	              | Reserved
// 309	                         | 1236  0x4D4	              | Reserved
// 310	                         | 1240  0x4D8	              | Reserved
// 311	                         | 1244  0x4DC	              | Reserved
// 312	                         | 1248  0x4E0	              | Reserved
// 313	                         | 1252  0x4E4	              | Reserved
// 314	                         | 1256  0x4E8	              | Reserved
// 315	                         | 1260  0x4EC	              | Reserved
// 316	                         | 1264  0x4F0	              | Reserved
// 317	                         | 1268  0x4F4	              | Reserved
// 318	                         | 1272  0x4F8	              | Reserved
// 319	                         | 1276  0x4FC	              | Reserved
// 320	                         | 1280  0x500	              | S2mmCommonControl
// 321	                         | 1284  0x504	              | S2mmCommonStatus
// 322	                         | 1288  0x508	              | S2mmChannelEnable
// 323	                         | 1292  0x50C	              | S2mmChannelInProgress
// 324	                         | 1296  0x510	              | S2mmError
// 325	                         | 1300  0x514	              | S2mmPacketsDropped
// 326	                         | 1304  0x518	              | S2mmChannelsComplete
// 327	                         | 1308  0x51C	              | S2mmAWCacheAWUser
// 328	                         | 1312  0x520	              | S2mmInterruptStatus
// 329	                         | 1316  0x524	              | Reserved
// 330	                         | 1320  0x528	              | Reserved
// 331	                         | 1324  0x52C	              | Reserved
// 332	                         | 1328  0x530	              | Reserved
// 333	                         | 1332  0x534	              | Reserved
// 334	                         | 1336  0x538	              | Reserved
// 335	                         | 1340  0x53C	              | Reserved
// 336	                         | 1344  0x540	              | S2mmCh0Control
// 337	                         | 1348  0x544	              | S2mmCh0Status
// 338	                         | 1352  0x548	              | S2mmCh0CurrentDescriptorLsw
// 339	                         | 1356  0x54C	              | S2mmCh0CurrentDescriptorMsw
// 340	                         | 1360  0x550	              | S2mmCh0TailDescriptorLsw
// 341	                         | 1364  0x554	              | S2mmCh0TailDescriptorMsw
// 342	                         | 1368  0x558	              | S2mmCh0PacketsDropped
// 343	                         | 1372  0x55C	              | S2mmCh0PacketCount
// 344	                         | 1376  0x560	              | Reserved
// 345	                         | 1380  0x564	              | Reserved
// 346	                         | 1384  0x568	              | Reserved
// 347	                         | 1388  0x56C	              | Reserved
// 348	                         | 1392  0x570	              | Reserved
// 349	                         | 1396  0x574	              | Reserved
// 350	                         | 1400  0x578	              | Reserved
// 351	                         | 1404  0x57C	              | Reserved
// 352	                         | 1408  0x580	              | S2mmCh1Control
// 353	                         | 1412  0x584	              | S2mmCh1Status
// 354	                         | 1416  0x588	              | S2mmCh1CurrentDescriptorLsw
// 355	                         | 1420  0x58C	              | S2mmCh1CurrentDescriptorMsw
// 356	                         | 1424  0x590	              | S2mmCh1TailDescriptorLsw
// 357	                         | 1428  0x594	              | S2mmCh1TailDescriptorMsw
// 358	                         | 1432  0x598	              | S2mmCh1PacketsDropped
// 359	                         | 1436  0x59C	              | S2mmCh1PacketCount
// 360	                         | 1440  0x5A0	              | Reserved
// 361	                         | 1444  0x5A4	              | Reserved
// 362	                         | 1448  0x5A8	              | Reserved
// 363	                         | 1452  0x5AC	              | Reserved
// 364	                         | 1456  0x5B0	              | Reserved
// 365	                         | 1460  0x5B4	              | Reserved
// 366	                         | 1464  0x5B8	              | Reserved
// 367	                         | 1468  0x5BC	              | Reserved
// 368	                         | 1472  0x5C0	              | S2mmCh2Control
// 369	                         | 1476  0x5C4	              | S2mmCh2Status
// 370	                         | 1480  0x5C8	              | S2mmCh2CurrentDescriptorLsw
// 371	                         | 1484  0x5CC	              | S2mmCh2CurrentDescriptorMsw
// 372	                         | 1488  0x5D0	              | S2mmCh2TailDescriptorLsw
// 373	                         | 1492  0x5D4	              | S2mmCh2TailDescriptorMsw
// 374	                         | 1496  0x5D8	              | S2mmCh2PacketsDropped
// 375	                         | 1500  0x5DC	              | S2mmCh2PacketCount
// 376	                         | 1504  0x5E0	              | Reserved
// 377	                         | 1508  0x5E4	              | Reserved
// 378	                         | 1512  0x5E8	              | Reserved
// 379	                         | 1516  0x5EC	              | Reserved
// 380	                         | 1520  0x5F0	              | Reserved
// 381	                         | 1524  0x5F4	              | Reserved
// 382	                         | 1528  0x5F8	              | Reserved
// 383	                         | 1532  0x5FC	              | Reserved
// 384	                         | 1536  0x600	              | S2mmCh3Control
// 385	                         | 1540  0x604	              | S2mmCh3Status
// 386	                         | 1544  0x608	              | S2mmCh3CurrentDescriptorLsw
// 387	                         | 1548  0x60C	              | S2mmCh3CurrentDescriptorMsw
// 388	                         | 1552  0x610	              | S2mmCh3TailDescriptorLsw
// 389	                         | 1556  0x614	              | S2mmCh3TailDescriptorMsw
// 390	                         | 1560  0x618	              | S2mmCh3PacketsDropped
// 391	                         | 1564  0x61C	              | S2mmCh3PacketCount
// 392	                         | 1568  0x620	              | Reserved
// 393	                         | 1572  0x624	              | Reserved
// 394	                         | 1576  0x628	              | Reserved
// 395	                         | 1580  0x62C	              | Reserved
// 396	                         | 1584  0x630	              | Reserved
// 397	                         | 1588  0x634	              | Reserved
// 398	                         | 1592  0x638	              | Reserved
// 399	                         | 1596  0x63C	              | Reserved
// 400	                         | 1600  0x640	              | S2mmCh4Control
// 401	                         | 1604  0x644	              | S2mmCh4Status
// 402	                         | 1608  0x648	              | S2mmCh4CurrentDescriptorLsw
// 403	                         | 1612  0x64C	              | S2mmCh4CurrentDescriptorMsw
// 404	                         | 1616  0x650	              | S2mmCh4TailDescriptorLsw
// 405	                         | 1620  0x654	              | S2mmCh4TailDescriptorMsw
// 406	                         | 1624  0x658	              | S2mmCh4PacketsDropped
// 407	                         | 1628  0x65C	              | S2mmCh4PacketCount
// 408	                         | 1632  0x660	              | Reserved
// 409	                         | 1636  0x664	              | Reserved
// 410	                         | 1640  0x668	              | Reserved
// 411	                         | 1644  0x66C	              | Reserved
// 412	                         | 1648  0x670	              | Reserved
// 413	                         | 1652  0x674	              | Reserved
// 414	                         | 1656  0x678	              | Reserved
// 415	                         | 1660  0x67C	              | Reserved
// 416	                         | 1664  0x680	              | S2mmCh5Control
// 417	                         | 1668  0x684	              | S2mmCh5Status
// 418	                         | 1672  0x688	              | S2mmCh5CurrentDescriptorLsw
// 419	                         | 1676  0x68C	              | S2mmCh5CurrentDescriptorMsw
// 420	                         | 1680  0x690	              | S2mmCh5TailDescriptorLsw
// 421	                         | 1684  0x694	              | S2mmCh5TailDescriptorMsw
// 422	                         | 1688  0x698	              | S2mmCh5PacketsDropped
// 423	                         | 1692  0x69C	              | S2mmCh5PacketCount
// 424	                         | 1696  0x6A0	              | Reserved
// 425	                         | 1700  0x6A4	              | Reserved
// 426	                         | 1704  0x6A8	              | Reserved
// 427	                         | 1708  0x6AC	              | Reserved
// 428	                         | 1712  0x6B0	              | Reserved
// 429	                         | 1716  0x6B4	              | Reserved
// 430	                         | 1720  0x6B8	              | Reserved
// 431	                         | 1724  0x6BC	              | Reserved
// 432	                         | 1728  0x6C0	              | S2mmCh6Control
// 433	                         | 1732  0x6C4	              | S2mmCh6Status
// 434	                         | 1736  0x6C8	              | S2mmCh6CurrentDescriptorLsw
// 435	                         | 1740  0x6CC	              | S2mmCh6CurrentDescriptorMsw
// 436	                         | 1744  0x6D0	              | S2mmCh6TailDescriptorLsw
// 437	                         | 1748  0x6D4	              | S2mmCh6TailDescriptorMsw
// 438	                         | 1752  0x6D8	              | S2mmCh6PacketsDropped
// 439	                         | 1756  0x6DC	              | S2mmCh6PacketCount
// 440	                         | 1760  0x6E0	              | Reserved
// 441	                         | 1764  0x6E4	              | Reserved
// 442	                         | 1768  0x6E8	              | Reserved
// 443	                         | 1772  0x6EC	              | Reserved
// 444	                         | 1776  0x6F0	              | Reserved
// 445	                         | 1780  0x6F4	              | Reserved
// 446	                         | 1784  0x6F8	              | Reserved
// 447	                         | 1788  0x6FC	              | Reserved
// 448	                         | 1792  0x700	              | S2mmCh7Control
// 449	                         | 1796  0x704	              | S2mmCh7Status
// 450	                         | 1800  0x708	              | S2mmCh7CurrentDescriptorLsw
// 451	                         | 1804  0x70C	              | S2mmCh7CurrentDescriptorMsw
// 452	                         | 1808  0x710	              | S2mmCh7TailDescriptorLsw
// 453	                         | 1812  0x714	              | S2mmCh7TailDescriptorMsw
// 454	                         | 1816  0x718	              | S2mmCh7PacketsDropped
// 455	                         | 1820  0x71C	              | S2mmCh7PacketCount
// 456	                         | 1824  0x720	              | Reserved
// 457	                         | 1828  0x724	              | Reserved
// 458	                         | 1832  0x728	              | Reserved
// 459	                         | 1836  0x72C	              | Reserved
// 460	                         | 1840  0x730	              | Reserved
// 461	                         | 1844  0x734	              | Reserved
// 462	                         | 1848  0x738	              | Reserved
// 463	                         | 1852  0x73C	              | Reserved
// 464	                         | 1856  0x740	              | S2mmCh8Control
// 465	                         | 1860  0x744	              | S2mmCh8Status
// 466	                         | 1864  0x748	              | S2mmCh8CurrentDescriptorLsw
// 467	                         | 1868  0x74C	              | S2mmCh8CurrentDescriptorMsw
// 468	                         | 1872  0x750	              | S2mmCh8TailDescriptorLsw
// 469	                         | 1876  0x754	              | S2mmCh8TailDescriptorMsw
// 470	                         | 1880  0x758	              | S2mmCh8PacketsDropped
// 471	                         | 1884  0x75C	              | S2mmCh8PacketCount
// 472	                         | 1888  0x760	              | Reserved
// 473	                         | 1892  0x764	              | Reserved
// 474	                         | 1896  0x768	              | Reserved
// 475	                         | 1900  0x76C	              | Reserved
// 476	                         | 1904  0x770	              | Reserved
// 477	                         | 1908  0x774	              | Reserved
// 478	                         | 1912  0x778	              | Reserved
// 479	                         | 1916  0x77C	              | Reserved
// 480	                         | 1920  0x780	              | S2mmCh9Control
// 481	                         | 1924  0x784	              | S2mmCh9Status
// 482	                         | 1928  0x788	              | S2mmCh9CurrentDescriptorLsw
// 483	                         | 1932  0x78C	              | S2mmCh9CurrentDescriptorMsw
// 484	                         | 1936  0x790	              | S2mmCh9TailDescriptorLsw
// 485	                         | 1940  0x794	              | S2mmCh9TailDescriptorMsw
// 486	                         | 1944  0x798	              | S2mmCh9PacketsDropped
// 487	                         | 1948  0x79C	              | S2mmCh9PacketCount
// 488	                         | 1952  0x7A0	              | Reserved
// 489	                         | 1956  0x7A4	              | Reserved
// 490	                         | 1960  0x7A8	              | Reserved
// 491	                         | 1964  0x7AC	              | Reserved
// 492	                         | 1968  0x7B0	              | Reserved
// 493	                         | 1972  0x7B4	              | Reserved
// 494	                         | 1976  0x7B8	              | Reserved
// 495	                         | 1980  0x7BC	              | Reserved
// 496	                         | 1984  0x7C0	              | S2mmCh10Control
// 497	                         | 1988  0x7C4	              | S2mmCh10Status
// 498	                         | 1992  0x7C8	              | S2mmCh10CurrentDescriptorLsw
// 499	                         | 1996  0x7CC	              | S2mmCh10CurrentDescriptorMsw
// 500	                         | 2000  0x7D0	              | S2mmCh10TailDescriptorLsw
// 501	                         | 2004  0x7D4	              | S2mmCh10TailDescriptorMsw
// 502	                         | 2008  0x7D8	              | S2mmCh10PacketsDropped
// 503	                         | 2012  0x7DC	              | S2mmCh10PacketCount
// 504	                         | 2016  0x7E0	              | Reserved
// 505	                         | 2020  0x7E4	              | Reserved
// 506	                         | 2024  0x7E8	              | Reserved
// 507	                         | 2028  0x7EC	              | Reserved
// 508	                         | 2032  0x7F0	              | Reserved
// 509	                         | 2036  0x7F4	              | Reserved
// 510	                         | 2040  0x7F8	              | Reserved
// 511	                         | 2044  0x7FC	              | Reserved
// 512	                         | 2048  0x800	              | S2mmCh11Control
// 513	                         | 2052  0x804	              | S2mmCh11Status
// 514	                         | 2056  0x808	              | S2mmCh11CurrentDescriptorLsw
// 515	                         | 2060  0x80C	              | S2mmCh11CurrentDescriptorMsw
// 516	                         | 2064  0x810	              | S2mmCh11TailDescriptorLsw
// 517	                         | 2068  0x814	              | S2mmCh11TailDescriptorMsw
// 518	                         | 2072  0x818	              | S2mmCh11PacketsDropped
// 519	                         | 2076  0x81C	              | S2mmCh11PacketCount
// 520	                         | 2080  0x820	              | Reserved
// 521	                         | 2084  0x824	              | Reserved
// 522	                         | 2088  0x828	              | Reserved
// 523	                         | 2092  0x82C	              | Reserved
// 524	                         | 2096  0x830	              | Reserved
// 525	                         | 2100  0x834	              | Reserved
// 526	                         | 2104  0x838	              | Reserved
// 527	                         | 2108  0x83C	              | Reserved
// 528	                         | 2112  0x840	              | S2mmCh12Control
// 529	                         | 2116  0x844	              | S2mmCh12Status
// 530	                         | 2120  0x848	              | S2mmCh12CurrentDescriptorLsw
// 531	                         | 2124  0x84C	              | S2mmCh12CurrentDescriptorMsw
// 532	                         | 2128  0x850	              | S2mmCh12TailDescriptorLsw
// 533	                         | 2132  0x854	              | S2mmCh12TailDescriptorMsw
// 534	                         | 2136  0x858	              | S2mmCh12PacketsDropped
// 535	                         | 2140  0x85C	              | S2mmCh12PacketCount
// 536	                         | 2144  0x860	              | Reserved
// 537	                         | 2148  0x864	              | Reserved
// 538	                         | 2152  0x868	              | Reserved
// 539	                         | 2156  0x86C	              | Reserved
// 540	                         | 2160  0x870	              | Reserved
// 541	                         | 2164  0x874	              | Reserved
// 542	                         | 2168  0x878	              | Reserved
// 543	                         | 2172  0x87C	              | Reserved
// 544	                         | 2176  0x880	              | S2mmCh13Control
// 545	                         | 2180  0x884	              | S2mmCh13Status
// 546	                         | 2184  0x888	              | S2mmCh13CurrentDescriptorLsw
// 547	                         | 2188  0x88C	              | S2mmCh13CurrentDescriptorMsw
// 548	                         | 2192  0x890	              | S2mmCh13TailDescriptorLsw
// 549	                         | 2196  0x894	              | S2mmCh13TailDescriptorMsw
// 550	                         | 2200  0x898	              | S2mmCh13PacketsDropped
// 551	                         | 2204  0x89C	              | S2mmCh13PacketCount
// 552	                         | 2208  0x8A0	              | Reserved
// 553	                         | 2212  0x8A4	              | Reserved
// 554	                         | 2216  0x8A8	              | Reserved
// 555	                         | 2220  0x8AC	              | Reserved
// 556	                         | 2224  0x8B0	              | Reserved
// 557	                         | 2228  0x8B4	              | Reserved
// 558	                         | 2232  0x8B8	              | Reserved
// 559	                         | 2236  0x8BC	              | Reserved
// 560	                         | 2240  0x8C0	              | S2mmCh14Control
// 561	                         | 2244  0x8C4	              | S2mmCh14Status
// 562	                         | 2248  0x8C8	              | S2mmCh14CurrentDescriptorLsw
// 563	                         | 2252  0x8CC	              | S2mmCh14CurrentDescriptorMsw
// 564	                         | 2256  0x8D0	              | S2mmCh14TailDescriptorLsw
// 565	                         | 2260  0x8D4	              | S2mmCh14TailDescriptorMsw
// 566	                         | 2264  0x8D8	              | S2mmCh14PacketsDropped
// 567	                         | 2268  0x8DC	              | S2mmCh14PacketCount
// 568	                         | 2272  0x8E0	              | Reserved
// 569	                         | 2276  0x8E4	              | Reserved
// 570	                         | 2280  0x8E8	              | Reserved
// 571	                         | 2284  0x8EC	              | Reserved
// 572	                         | 2288  0x8F0	              | Reserved
// 573	                         | 2292  0x8F4	              | Reserved
// 574	                         | 2296  0x8F8	              | Reserved
// 575	                         | 2300  0x8FC	              | Reserved
// 576	                         | 2304  0x900	              | S2mmCh15Control
// 577	                         | 2308  0x904	              | S2mmCh15Status
// 578	                         | 2312  0x908	              | S2mmCh15CurrentDescriptorLsw
// 579	                         | 2316  0x90C	              | S2mmCh15CurrentDescriptorMsw
// 580	                         | 2320  0x910	              | S2mmCh15TailDescriptorLsw
// 581	                         | 2324  0x914	              | S2mmCh15TailDescriptorMsw
// 582	                         | 2328  0x918	              | S2mmCh15PacketsDropped
// 583	                         | 2332  0x91C	              | S2mmCh15PacketCount
// 584	                         | 2336  0x920	              | Reserved
// 585	                         | 2340  0x924	              | Reserved
// 586	                         | 2344  0x928	              | Reserved
// 587	                         | 2348  0x92C	              | Reserved
// 588	                         | 2352  0x930	              | Reserved
// 589	                         | 2356  0x934	              | Reserved
// 590	                         | 2360  0x938	              | Reserved
// 591	                         | 2364  0x93C	              | Reserved
// 592	                         | 2368  0x940	              | S2mmChannelObserver1
// 593	                         | 2372  0x944	              | S2mmChannelObserver2
// 594	                         | 2376  0x948	              | S2mmChannelObserver3
// 595	                         | 2380  0x94C	              | S2mmChannelObserver4
// 596	                         | 2384  0x950	              | S2mmChannelObserver5
// 597	                         | 2388  0x954	              | S2mmChannelObserver6
//
// Since the channel registers are constant strides apart, we store only
// the base address and the stride between channels.
#[derive(Debug, Clone, EnumIter)]
pub enum Registers {
    Mm2sCommonControl,
    Mm2sCommonStatus,
    Mm2sChannelEnable,
    _Mm2sChannelInProgress,
    Mm2sError,
    _Mm2sChannelSchedulerType,
    _Mm2sLowerChannelsWeight,
    _Mm2sUpperChannelWeight,
    _Mm2sChannelsComplete,
    _Mm2sARCacheARUser,
    Mm2sInterruptStatus,
    Mm2sChControl(Channel),
    Mm2sChStatus(Channel),
    Mm2sChCurrentDescriptorLsw(Channel),
    Mm2sChCurrentDescriptorMsw(Channel),
    Mm2sChTailDescriptorLsw(Channel),
    Mm2sChTailDescriptorMsw(Channel),
    _Mm2sChPacketCount(Channel),
    _Mm2sChannelObserver1,
    _Mm2sChannelObserver2,
    _Mm2sChannelObserver3,
    _Mm2sChannelObserver4,
    _Mm2sChannelObserver5,
    _Mm2sChannelObserver6,
    S2mmCommonControl,
    S2mmCommonStatus,
    S2mmChannelEnable,
    _S2mmChannelInProgress,
    S2mmError,
    _S2mmPacketsDropped,
    _S2mmChannelsComplete,
    _S2mmAWCacheAWUser,
    S2mmInterruptStatus,
    S2mmChControl(Channel),
    S2mmChStatus(Channel),
    S2mmChCurrentDescriptorLsw(Channel),
    S2mmChCurrentDescriptorMsw(Channel),
    S2mmChTailDescriptorLsw(Channel),
    S2mmChTailDescriptorMsw(Channel),
    _S2mmChPacketsDropped(Channel),
    _S2mmChPacketCount(Channel),
    _S2mmChannelObserver1,
    _S2mmChannelObserver2,
    _S2mmChannelObserver3,
    _S2mmChannelObserver4,
    _S2mmChannelObserver5,
    _S2mmChannelObserver6,
}

impl Registers {
    pub(crate) const MM2S_CHANNEL_REGISTER_STRIDE: usize = 16;
    pub(crate) const S2MM_CHANNEL_REGISTER_STRIDE: usize = 16;

    pub(crate) const N_CHANNELS: u8 = 16;
}

impl RegisterLayout for Registers {
    type RegisterType = u32;

    #[inline]
    fn offset(&self) -> usize {
        match *self {
            Registers::Mm2sCommonControl => 0,
            Registers::Mm2sCommonStatus => 1,
            Registers::Mm2sChannelEnable => 2,
            Registers::_Mm2sChannelInProgress => 3,
            Registers::Mm2sError => 4,
            Registers::_Mm2sChannelSchedulerType => 5,
            Registers::_Mm2sLowerChannelsWeight => 6,
            Registers::_Mm2sUpperChannelWeight => 7,
            Registers::_Mm2sChannelsComplete => 8,
            Registers::_Mm2sARCacheARUser => 9,
            Registers::Mm2sInterruptStatus => 10,
            Registers::Mm2sChControl(channel) => {
                16 + Self::S2MM_CHANNEL_REGISTER_STRIDE * channel.channel_number as usize
            }
            Registers::Mm2sChStatus(channel) => {
                17 + Self::S2MM_CHANNEL_REGISTER_STRIDE * channel.channel_number as usize
            }
            Registers::Mm2sChCurrentDescriptorLsw(channel) => {
                18 + Self::MM2S_CHANNEL_REGISTER_STRIDE * channel.channel_number as usize
            }
            Registers::Mm2sChCurrentDescriptorMsw(channel) => {
                19 + Self::MM2S_CHANNEL_REGISTER_STRIDE * channel.channel_number as usize
            }
            Registers::Mm2sChTailDescriptorLsw(channel) => {
                20 + Self::MM2S_CHANNEL_REGISTER_STRIDE * channel.channel_number as usize
            }
            Registers::Mm2sChTailDescriptorMsw(channel) => {
                21 + Self::MM2S_CHANNEL_REGISTER_STRIDE * channel.channel_number as usize
            }
            Registers::_Mm2sChPacketCount(channel) => {
                22 + Self::MM2S_CHANNEL_REGISTER_STRIDE * channel.channel_number as usize
            }
            Registers::_Mm2sChannelObserver1 => 272,
            Registers::_Mm2sChannelObserver2 => 273,
            Registers::_Mm2sChannelObserver3 => 274,
            Registers::_Mm2sChannelObserver4 => 275,
            Registers::_Mm2sChannelObserver5 => 276,
            Registers::_Mm2sChannelObserver6 => 277,
            Registers::S2mmCommonControl => 320,
            Registers::S2mmCommonStatus => 321,
            Registers::S2mmChannelEnable => 322,
            Registers::_S2mmChannelInProgress => 323,
            Registers::S2mmError => 324,
            Registers::_S2mmPacketsDropped => 325,
            Registers::_S2mmChannelsComplete => 326,
            Registers::_S2mmAWCacheAWUser => 327,
            Registers::S2mmInterruptStatus => 328,
            Registers::S2mmChControl(channel) => {
                336 + Self::S2MM_CHANNEL_REGISTER_STRIDE * channel.channel_number as usize
            }
            Registers::S2mmChStatus(channel) => {
                337 + Self::S2MM_CHANNEL_REGISTER_STRIDE * channel.channel_number as usize
            }
            Registers::S2mmChCurrentDescriptorLsw(channel) => {
                338 + Self::S2MM_CHANNEL_REGISTER_STRIDE * channel.channel_number as usize
            }
            Registers::S2mmChCurrentDescriptorMsw(channel) => {
                339 + Self::S2MM_CHANNEL_REGISTER_STRIDE * channel.channel_number as usize
            }
            Registers::S2mmChTailDescriptorLsw(channel) => {
                340 + Self::S2MM_CHANNEL_REGISTER_STRIDE * channel.channel_number as usize
            }
            Registers::S2mmChTailDescriptorMsw(channel) => {
                341 + Self::S2MM_CHANNEL_REGISTER_STRIDE * channel.channel_number as usize
            }
            Registers::_S2mmChPacketsDropped(channel) => {
                342 + Self::S2MM_CHANNEL_REGISTER_STRIDE * channel.channel_number as usize
            }
            Registers::_S2mmChPacketCount(channel) => {
                343 + Self::S2MM_CHANNEL_REGISTER_STRIDE * channel.channel_number as usize
            }
            Registers::_S2mmChannelObserver1 => 592,
            Registers::_S2mmChannelObserver2 => 593,
            Registers::_S2mmChannelObserver3 => 594,
            Registers::_S2mmChannelObserver4 => 595,
            Registers::_S2mmChannelObserver5 => 596,
            Registers::_S2mmChannelObserver6 => 597,
        }
    }
}

impl RegisterCheck for Registers {}

pub enum CommonControlRegister {
    RunStop,
    Reset,
}

impl CommonControlRegister {
    #[inline]
    pub fn offset(&self) -> u32 {
        match *self {
            CommonControlRegister::RunStop => 0,
            CommonControlRegister::Reset => 2,
        }
    }
    #[inline]
    pub fn bitwidth(&self) -> u32 {
        match *self {
            CommonControlRegister::RunStop => 1,
            CommonControlRegister::Reset => 1,
        }
    }
    #[inline]
    pub fn bitmask(&self) -> u32 {
        (2_u32.pow(self.bitwidth()) - 1) << self.offset()
    }
}

pub enum CommonStatusRegister {
    Halted,
    Idle,
}

impl CommonStatusRegister {
    #[inline]
    pub fn offset(&self) -> u32 {
        match *self {
            CommonStatusRegister::Halted => 0,
            CommonStatusRegister::Idle => 1,
        }
    }
    #[inline]
    pub fn bitwidth(&self) -> u32 {
        match *self {
            CommonStatusRegister::Halted => 1,
            CommonStatusRegister::Idle => 1,
        }
    }
    #[inline]
    pub fn bitmask(&self) -> u32 {
        (2_u32.pow(self.bitwidth()) - 1) << self.offset()
    }
}

pub enum ErrorRegister {
    DmaIntErr, // RO 1 if DMA internal error
    DmaSlvErr, // RO 1 if DMA slave error
    DmaDecErr, // RO 1 if DMA decode error
    SGIntErr,  // RO 1 if scatter gather internal error
    SGSlvErr,  // RO 1 if scatter gather slave error
    SGDecErr,  // RO 1 if scatter gather decode error
}

impl ErrorRegister {
    #[inline]
    pub fn offset(&self) -> u32 {
        match *self {
            ErrorRegister::DmaIntErr => 0,
            ErrorRegister::DmaSlvErr => 1,
            ErrorRegister::DmaDecErr => 2,
            ErrorRegister::SGIntErr => 4,
            ErrorRegister::SGSlvErr => 5,
            ErrorRegister::SGDecErr => 6,
        }
    }
    #[inline]
    pub fn bitwidth(&self) -> u32 {
        match *self {
            ErrorRegister::DmaIntErr => 1,
            ErrorRegister::DmaSlvErr => 1,
            ErrorRegister::DmaDecErr => 1,
            ErrorRegister::SGIntErr => 1,
            ErrorRegister::SGSlvErr => 1,
            ErrorRegister::SGDecErr => 1,
        }
    }
    #[inline]
    pub fn bitmask(&self) -> u32 {
        (2_u32.pow(self.bitwidth()) - 1) << self.offset()
    }
}

pub enum Mm2sChannelControl {
    Fetch,           // Start/Stop the fetch of BDs for this channel
    OtherChErrIrqEn, // Interrupt on Error on other channel enable
    IocIrqEn,        // Interrupt on complete
    _DlyIrqEn,       // Interrupt on delay timer
    ErrIrqEn,        // Interrupt on error
    IrqThreshold,    // Interrupt threshold
    _IrqDelay,       // Interrupt delay timeout
}

impl Mm2sChannelControl {
    #[inline]
    pub fn offset(&self) -> u32 {
        match *self {
            Mm2sChannelControl::Fetch => 0,
            Mm2sChannelControl::OtherChErrIrqEn => 3,
            Mm2sChannelControl::IocIrqEn => 5,
            Mm2sChannelControl::_DlyIrqEn => 6,
            Mm2sChannelControl::ErrIrqEn => 7,
            Mm2sChannelControl::IrqThreshold => 16,
            Mm2sChannelControl::_IrqDelay => 24,
        }
    }
    #[inline]
    pub fn bitwidth(&self) -> u32 {
        match *self {
            Mm2sChannelControl::Fetch => 1,
            Mm2sChannelControl::OtherChErrIrqEn => 1,
            Mm2sChannelControl::IocIrqEn => 1,
            Mm2sChannelControl::_DlyIrqEn => 1,
            Mm2sChannelControl::ErrIrqEn => 1,
            Mm2sChannelControl::IrqThreshold => 8,
            Mm2sChannelControl::_IrqDelay => 8,
        }
    }
    #[inline]
    pub fn bitmask(&self) -> u32 {
        (2_u32.pow(self.bitwidth()) - 1) << self.offset()
    }
}

pub enum S2mmChannelControl {
    Fetch,                // Start/Stop the fetch of BDs for this channel
    OtherChErrIrqEn,      // Enable interrupt on Error on other channel enable
    _PktDropIrqEn,        // Enable interrupt on packet drop
    IocIrqEn,             // Enable interrupt on complete
    _DlyIrqEn,            // Enable interrupt on delay timer
    ErrIrqEn,             // Enable interrupt on error
    _IrqPktDropThreshold, // Packet drop interrupt threshold
    IrqThreshold,         // Interrupt threshold
    _IrqDelay,            // Interrupt delay timeout
}

impl S2mmChannelControl {
    #[inline]
    pub fn offset(&self) -> u32 {
        match *self {
            S2mmChannelControl::Fetch => 0,
            S2mmChannelControl::OtherChErrIrqEn => 3,
            S2mmChannelControl::_PktDropIrqEn => 4,
            S2mmChannelControl::IocIrqEn => 5,
            S2mmChannelControl::_DlyIrqEn => 6,
            S2mmChannelControl::ErrIrqEn => 7,
            S2mmChannelControl::_IrqPktDropThreshold => 8,
            S2mmChannelControl::IrqThreshold => 16,
            S2mmChannelControl::_IrqDelay => 24,
        }
    }
    #[inline]
    pub fn bitwidth(&self) -> u32 {
        match *self {
            S2mmChannelControl::Fetch => 1,
            S2mmChannelControl::OtherChErrIrqEn => 1,
            S2mmChannelControl::_PktDropIrqEn => 1,
            S2mmChannelControl::IocIrqEn => 1,
            S2mmChannelControl::_DlyIrqEn => 1,
            S2mmChannelControl::ErrIrqEn => 1,
            S2mmChannelControl::_IrqPktDropThreshold => 8,
            S2mmChannelControl::IrqThreshold => 8,
            S2mmChannelControl::_IrqDelay => 8,
        }
    }
    #[inline]
    pub fn bitmask(&self) -> u32 {
        (2_u32.pow(self.bitwidth()) - 1) << self.offset()
    }
}

pub enum Mm2sChannelStatus {
    _Idle,         // Channel is idle
    OtherChErrIrq, // Interrupt was generated on detection of an error on another channel
    IocIrq,        // Interrupt was generated on completion of a descriptor
    _DlyIrq,       // Interrupt was generated on delay timer timeout
    ErrIrq,        // Interrupt was generated on an error
    _IrqThreshold, // Interrupt threshold status
    _IrqDelay,     // Interrupt delay time status
}

impl Mm2sChannelStatus {
    #[inline]
    pub fn offset(&self) -> u32 {
        match *self {
            Mm2sChannelStatus::_Idle => 0,
            Mm2sChannelStatus::OtherChErrIrq => 3,
            Mm2sChannelStatus::IocIrq => 5,
            Mm2sChannelStatus::_DlyIrq => 6,
            Mm2sChannelStatus::ErrIrq => 7,
            Mm2sChannelStatus::_IrqThreshold => 16,
            Mm2sChannelStatus::_IrqDelay => 24,
        }
    }
    #[inline]
    pub fn bitwidth(&self) -> u32 {
        match *self {
            Mm2sChannelStatus::_Idle => 1,
            Mm2sChannelStatus::OtherChErrIrq => 1,
            Mm2sChannelStatus::IocIrq => 1,
            Mm2sChannelStatus::_DlyIrq => 1,
            Mm2sChannelStatus::ErrIrq => 1,
            Mm2sChannelStatus::_IrqThreshold => 8,
            Mm2sChannelStatus::_IrqDelay => 8,
        }
    }
    #[inline]
    pub fn bitmask(&self) -> u32 {
        (2_u32.pow(self.bitwidth()) - 1) << self.offset()
    }
}

pub enum S2mmChannelStatus {
    _Idle,                // Channel is idle
    BDShortfall,          // The BD queue is empty the packet has not completed
    OtherChErrIrq,        // Interrupt was generated on detection of an error on another channel
    _PktDropIrq,          // Interrupt was generated on packet drop
    IocIrq,               // Interrupt was generated on completion of a descriptor
    _DlyIrq,              // Interrupt was generated on delay timer timeout
    ErrIrq,               // Interrupt was generated on an error
    _IrqPktDropThreshold, // Interrupt packet drop threshold
    _IrqThreshold,        // Interrupt threshold status
    _IrqDelay,            // Interrupt delay time status
}

impl S2mmChannelStatus {
    #[inline]
    pub fn offset(&self) -> u32 {
        match *self {
            S2mmChannelStatus::_Idle => 0,
            S2mmChannelStatus::BDShortfall => 1,
            S2mmChannelStatus::OtherChErrIrq => 3,
            S2mmChannelStatus::_PktDropIrq => 4,
            S2mmChannelStatus::IocIrq => 5,
            S2mmChannelStatus::_DlyIrq => 6,
            S2mmChannelStatus::ErrIrq => 7,
            S2mmChannelStatus::_IrqPktDropThreshold => 8,
            S2mmChannelStatus::_IrqThreshold => 16,
            S2mmChannelStatus::_IrqDelay => 24,
        }
    }
    #[inline]
    pub fn bitwidth(&self) -> u32 {
        match *self {
            S2mmChannelStatus::_Idle => 1,
            S2mmChannelStatus::BDShortfall => 1,
            S2mmChannelStatus::OtherChErrIrq => 1,
            S2mmChannelStatus::_PktDropIrq => 1,
            S2mmChannelStatus::IocIrq => 1,
            S2mmChannelStatus::_DlyIrq => 1,
            S2mmChannelStatus::ErrIrq => 1,
            S2mmChannelStatus::_IrqPktDropThreshold => 8,
            S2mmChannelStatus::_IrqThreshold => 8,
            S2mmChannelStatus::_IrqDelay => 8,
        }
    }
    #[inline]
    pub fn bitmask(&self) -> u32 {
        (2_u32.pow(self.bitwidth()) - 1) << self.offset()
    }
}
