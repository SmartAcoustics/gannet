use std::cmp;
use std::collections::HashMap;
use std::error;
use std::fmt;
use std::ops;
use std::slice;

use nohash_hasher::IntMap;

use thiserror::Error;

use crate::descriptors::DescriptorPool;
use crate::multi_channel::MAX_CHANNELS;
use crate::operation::{Operation, OperationError, RefreshStaleOperation, ScatterGatherOperation};
use crate::MemBlockLayout;

/// Error in creating or handling a Multi-channel Gannet operation.
#[derive(Error, Debug)]
pub enum MultiChannelOperationError {
    /// Channel operations were not all from the same descriptor pool
    #[error("Channel operations were not all from the same descriptor pool.")]
    MismatchedDescriptorPool,
    /// Invalid channel in the channel operations
    #[error("Invalid channel (must be in range [0, MAX_CHANNELS)): {channel:?}")]
    InvalidChannel { channel: u8 },
    /// Mismatched transfer types.
    #[error("All the channels must be either MM2S or S2MM and cannot by mixed.")]
    MismatchedTransferType,
}

/// Errors detected on the descriptors during the refresh of the operation.
/// This implies there is a problem with the data. The descriptors are fully
/// refreshed so the operation should be usable again.
#[derive(Debug)]
pub struct MultiChannelOperationRefreshError {
    pub errors: Vec<(Channel, OperationError)>,
    pub refreshed_operation: MultiChannelOperation,
}

impl error::Error for MultiChannelOperationRefreshError {}

impl fmt::Display for MultiChannelOperationRefreshError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "On some or all channels, errors were recorded during the operation: {:?}",
            self.errors
        )
    }
}

/// Represents a single channel within a [MultiChannelOperation].
#[derive(Copy, Clone, Debug, Eq, PartialEq)]
pub struct Channel {
    pub(crate) channel_number: u8,
}

impl std::hash::Hash for Channel {
    fn hash<H: std::hash::Hasher>(&self, hasher: &mut H) {
        hasher.write_u8(self.channel_number)
    }
}

impl nohash_hasher::IsEnabled for Channel {}

impl Channel {
    pub fn new(channel_number: u8) -> Result<Channel, MultiChannelOperationError> {
        if channel_number >= MAX_CHANNELS {
            Err(MultiChannelOperationError::InvalidChannel {
                channel: channel_number,
            })
        } else {
            Ok(Channel { channel_number })
        }
    }
}

impl Into<usize> for Channel {
    fn into(self) -> usize {
        self.channel_number as usize
    }
}

impl Default for Channel {
    fn default() -> Channel {
        // Defensively use the `new` machinery. A panic will occur if for some
        // reason 0 is changed to be out of bounds.
        Channel::new(0).unwrap()
    }
}

impl TryFrom<u8> for Channel {
    type Error = MultiChannelOperationError;

    fn try_from(value: u8) -> Result<Self, Self::Error> {
        Channel::new(value)
    }
}

/// Represents a multi-channel scatter gather operation. It wraps multiple
/// [crate::operation::Operation] structures with suitable annotations and
/// augmentations.
#[derive(Debug)]
pub struct MultiChannelOperation {
    channel_operations: Vec<(Channel, Operation)>,
    descriptor_pool_id: Option<u64>,
    pub(crate) channels_enabled_mask: u32,
}

impl MultiChannelOperation {
    /// Construct a new MultiChannelOperation from a HashMap that relates
    /// single channel operations to each [Channel] of interest.
    pub fn new<H>(
        mut channel_operations: HashMap<Channel, Operation, H>,
    ) -> Result<MultiChannelOperation, MultiChannelOperationError> {
        let mut channel_operations_vec = Vec::new();
        let mut channels_enabled_mask = 0u32;

        let mut pool_id = None;

        for (channel, operation) in channel_operations.drain() {
            match pool_id {
                Some(id) => {
                    if operation.descriptor_block().pool_id() != id {
                        return Err(MultiChannelOperationError::MismatchedDescriptorPool);
                    }
                }
                None => {
                    pool_id = Some(operation.descriptor_block().pool_id());
                }
            }

            channel_operations_vec.push((channel, operation));
            channels_enabled_mask |= 1 << channel.channel_number;
        }

        Ok(MultiChannelOperation {
            channel_operations: channel_operations_vec,
            descriptor_pool_id: pool_id,
            channels_enabled_mask,
        })
    }

    pub(crate) fn iter(&self) -> slice::Iter<'_, (Channel, Operation)> {
        self.channel_operations.iter()
    }

    pub(crate) fn iter_mut(&mut self) -> slice::IterMut<'_, (Channel, Operation)> {
        self.channel_operations.iter_mut()
    }

    /// Returns the containing memory layout of this operation. That is, as a
    /// `MemBlockLayout` that can be used to claim the block of memory that
    /// contains within all accesses and reads for this operation.
    ///
    /// Precisely what the layout means is down to how the descriptors are
    /// defined, and knowledge of how the operation is created is necessary
    /// for making proper use of a claimed memory block.
    pub fn containing_memory(&self) -> MemBlockLayout {
        ScatterGatherOperation::containing_memory(self)
    }
}

impl ScatterGatherOperation for MultiChannelOperation {
    /// Returns whether the descriptors associated with this operation are
    /// from the passed in DescriptorPool.
    #[inline]
    fn descriptors_from(&self, pool: &DescriptorPool) -> bool {
        match self.descriptor_pool_id {
            Some(id) => id == pool.pool_id(),
            None => true,
        }
    }

    fn containing_memory(&self) -> MemBlockLayout {
        // This needs to happen when it's used since the contained operations
        // can be mutated.
        let mut containing_memory: Option<ops::Range<usize>> = None;

        for (_, operation) in self.iter() {
            containing_memory = match containing_memory {
                Some(range) => {
                    let other_range: ops::Range<usize> = operation.containing_memory().into();

                    let combined_range = cmp::min(range.start, other_range.start)
                        ..cmp::max(range.end, other_range.end);

                    Some(combined_range)
                }
                None => Some(operation.containing_memory().into()),
            };
        }

        match containing_memory {
            Some(range) => range.into(),
            None => MemBlockLayout { offset: 0, size: 0 },
        }
    }
}

impl TryFrom<HashMap<Channel, Operation>> for MultiChannelOperation {
    type Error = MultiChannelOperationError;

    fn try_from(value: HashMap<Channel, Operation>) -> Result<Self, Self::Error> {
        Self::new(value)
    }
}

impl TryFrom<nohash_hasher::IntMap<Channel, Operation>> for MultiChannelOperation {
    type Error = MultiChannelOperationError;

    fn try_from(value: IntMap<Channel, Operation>) -> Result<Self, Self::Error> {
        Self::new(value)
    }
}

// FIXME need test
impl IntoIterator for MultiChannelOperation {
    type Item = (Channel, Operation);
    type IntoIter = std::vec::IntoIter<Self::Item>;

    fn into_iter(self) -> Self::IntoIter {
        self.channel_operations.into_iter()
    }
}

/// An operation that has been run and should not be run again. To extract
/// the operation, call [check_and_refresh] on the `StaleMultiChannelOperation`,
/// which will always return a refreshed operation (even if it is encapsulated
/// in an error).
#[derive(Debug)]
pub struct StaleMultiChannelOperation {
    multi_channel_operation: MultiChannelOperation,
}

impl StaleMultiChannelOperation {
    /// Unwraps without refreshing a StaleMultiChannelOperation into the
    /// [MultiChannelOperation] from which it which it was derived.
    ///
    /// Strictly speaking, the process of unwrapping is not unsafe, but the
    /// returned [MultiChannelOperation] may not be in a usable state.
    pub unsafe fn unwrap(self) -> MultiChannelOperation {
        let StaleMultiChannelOperation {
            multi_channel_operation,
        } = self;

        multi_channel_operation
    }
}

impl RefreshStaleOperation for StaleMultiChannelOperation {
    type Output = MultiChannelOperation;
    type Error = MultiChannelOperationRefreshError;

    fn check_and_refresh(self) -> Result<Self::Output, Self::Error> {
        let StaleMultiChannelOperation {
            mut multi_channel_operation,
        } = self;

        let mut channel_errors: Option<Vec<(Channel, OperationError)>> = None;

        for (channel, operation) in multi_channel_operation.iter_mut() {
            let err = match operation.check_and_refresh() {
                Ok(_) => continue,
                Err(e) => e,
            };

            channel_errors = channel_errors.or(Some(Vec::new()));
            channel_errors.as_mut().map(|errs| {
                errs.push((*channel, err));
                errs
            });
        }

        match channel_errors {
            Some(errs) => {
                let ret_err = MultiChannelOperationRefreshError {
                    errors: errs,
                    refreshed_operation: multi_channel_operation,
                };
                Err(ret_err)
            }
            None => Ok(multi_channel_operation),
        }
    }
}

impl From<MultiChannelOperation> for StaleMultiChannelOperation {
    fn from(op: MultiChannelOperation) -> Self {
        StaleMultiChannelOperation {
            multi_channel_operation: op,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::convert::TryInto;

    #[test]
    fn test_channel_type() {
        Channel::new(0).unwrap();
        Channel::new(MAX_CHANNELS - 1).unwrap();

        let channel = MAX_CHANNELS;
        match Channel::new(channel) {
            Err(MultiChannelOperationError::InvalidChannel { channel: _ }) => (),
            _ => panic!("InvalidChannel not raised."),
        }

        let channel = MAX_CHANNELS + 1;
        match Channel::new(channel) {
            Err(MultiChannelOperationError::InvalidChannel { channel: _ }) => (),
            _ => panic!("InvalidChannel not raised."),
        }
    }

    #[test]
    fn test_channel_type_try_from() {
        Channel::try_from(0u8).unwrap();
        Channel::try_from(MAX_CHANNELS - 1).unwrap();

        let _a: Channel = 0u8.try_into().unwrap();
        let _a: Channel = (MAX_CHANNELS - 1).try_into().unwrap();

        let channel = MAX_CHANNELS;
        match Channel::try_from(channel) {
            Err(MultiChannelOperationError::InvalidChannel { channel: _ }) => (),
            _ => panic!("InvalidChannel not raised."),
        }

        let channel = MAX_CHANNELS + 1;
        match Channel::try_from(channel) {
            Err(MultiChannelOperationError::InvalidChannel { channel: _ }) => (),
            _ => panic!("InvalidChannel not raised."),
        }
    }
}

#[cfg(all(test, target_arch = "arm"))]
mod device_tests {

    use crate::multi_channel::{Channel, Device, MultiChannelOperation,};
    use crate::{MemBlockLayout, BusDataWidth};
    use serial_test::serial;
    use std::{collections::HashMap, convert::TryInto, path::PathBuf};

    /// It should be possible to construct a MultiChannelOperation from a
    /// HashMap of single channel operations.
    #[test]
    #[serial]
    fn test_multi_channel_operation_from_hashmap() {
        // Create device
        let device_path: PathBuf = ["/dev", "axi_dma_mc_mm2s"].iter().collect();
        let mut device = Device::new(&device_path, BusDataWidth::FourBytes).unwrap();

        let operation_0 = device.new_regular_blocks_operation(16, 16, 1, 0).unwrap();

        let operation_10 = device.new_regular_blocks_operation(16, 16, 1, 16).unwrap();

        // Create the operations hashmap
        let mut operations = HashMap::new();
        operations.insert(Channel::new(0_u8).unwrap(), operation_0);
        operations.insert(Channel::new(10_u8).unwrap(), operation_10);

        let _operations = MultiChannelOperation::new(operations).unwrap();
    }

    /// It should be possible to construct a MultiChannelOperation from a
    /// HashMap of single channel operations using try_into().
    #[test]
    #[serial]
    fn test_multi_channel_operation_from_hashmap_with_try_into() {
        // Create device
        let device_path: PathBuf = ["/dev", "axi_dma_mc_mm2s"].iter().collect();
        let mut device = Device::new(&device_path, BusDataWidth::FourBytes).unwrap();

        let operation_0 = device.new_regular_blocks_operation(16, 16, 1, 0).unwrap();

        let operation_10 = device.new_regular_blocks_operation(16, 16, 1, 16).unwrap();

        // Create the operations hashmap
        let mut operations = HashMap::new();
        operations.insert(Channel::new(0_u8).unwrap(), operation_0);
        operations.insert(Channel::new(10_u8).unwrap(), operation_10);

        let _operations: MultiChannelOperation = operations.try_into().unwrap();
    }

    /// The containing_memory method should return the maximum extents of
    /// the contained operations.
    #[test]
    #[serial]
    fn test_combined_containing_memory() {
        // Create device
        let device_path: PathBuf = ["/dev", "axi_dma_mc_mm2s"].iter().collect();
        let mut device = Device::new(&device_path, BusDataWidth::FourBytes).unwrap();

        let operation_10 = device
            .new_regular_blocks_operation(16, 32, 30, 200)
            .unwrap();

        let operation_0 = device
            .new_regular_blocks_operation(32, 64, 15, 2000)
            .unwrap();

        let operation_3 = device.new_regular_blocks_operation(8, 8, 3, 40).unwrap();

        let operation_8 = device.new_regular_blocks_operation(8, 8, 3, 1200).unwrap();

        // Create the operations hashmap
        let mut operations = HashMap::new();
        operations.insert(Channel::new(0_u8).unwrap(), operation_0);
        operations.insert(Channel::new(10_u8).unwrap(), operation_10);
        operations.insert(Channel::new(3_u8).unwrap(), operation_3);
        operations.insert(Channel::new(8_u8).unwrap(), operation_8);

        let operations = MultiChannelOperation::new(operations).unwrap();

        // upper = 2000 + 64 * 14 + 32 = 2928
        // lower = 40
        // => offset = 40, size = 2928 - 40 = 2888
        assert_eq!(
            operations.containing_memory(),
            MemBlockLayout {
                offset: 40,
                size: 2888
            }
        );
    }
}
