/// Defines the maximum number of bytes that can be transferred by each
/// descriptor. Note: This assumes that the Width of Buffer Length Register in
/// the DMA engine in the PL is set to 26 bits. If this is not the case then
/// this library will not behave as expected.
pub const MAX_BYTES_TRANSFER_PER_DESCRIPTOR: usize = (1 << 26) - 1;

pub(crate) enum Mm2sDescriptorFields {
    NextDescriptorLsw,
    NextDescriptorMsw,
    BufferAddrLsw,
    BufferAddrMsw,
    Control,
    _ControlSideband,
    Status,
    _App0,
    _App1,
    _App2,
    _App3,
    _App4,
}
impl Mm2sDescriptorFields {
    // offset gives the offset in bytes
    pub(crate) fn offset(&self) -> usize {
        match *self {
            Mm2sDescriptorFields::NextDescriptorLsw => 0x00,
            Mm2sDescriptorFields::NextDescriptorMsw => 0x04,
            Mm2sDescriptorFields::BufferAddrLsw => 0x08,
            Mm2sDescriptorFields::BufferAddrMsw => 0x0C,
            Mm2sDescriptorFields::Control => 0x14,
            Mm2sDescriptorFields::_ControlSideband => 0x18,
            Mm2sDescriptorFields::Status => 0x1C,
            Mm2sDescriptorFields::_App0 => 0x20,
            Mm2sDescriptorFields::_App1 => 0x24,
            Mm2sDescriptorFields::_App2 => 0x28,
            Mm2sDescriptorFields::_App3 => 0x2C,
            Mm2sDescriptorFields::_App4 => 0x30,
        }
    }
}

pub(crate) enum S2mmDescriptorFields {
    NextDescriptorLsw,
    NextDescriptorMsw,
    BufferAddrLsw,
    BufferAddrMsw,
    Control,
    Status,
    _SidebandStatus,
    _App0,
    _App1,
    _App2,
    _App3,
    _App4,
}
impl S2mmDescriptorFields {
    // offset gives the offset in bytes
    pub(crate) fn offset(&self) -> usize {
        match *self {
            S2mmDescriptorFields::NextDescriptorLsw => 0x00,
            S2mmDescriptorFields::NextDescriptorMsw => 0x04,
            S2mmDescriptorFields::BufferAddrLsw => 0x08,
            S2mmDescriptorFields::BufferAddrMsw => 0x0C,
            S2mmDescriptorFields::Control => 0x14,
            S2mmDescriptorFields::Status => 0x18,
            S2mmDescriptorFields::_SidebandStatus => 0x1C,
            S2mmDescriptorFields::_App0 => 0x20,
            S2mmDescriptorFields::_App1 => 0x24,
            S2mmDescriptorFields::_App2 => 0x28,
            S2mmDescriptorFields::_App3 => 0x2C,
            S2mmDescriptorFields::_App4 => 0x30,
        }
    }
}

pub(crate) enum Mm2sDescriptorControlField {
    _BufferLength, // MM2S: Transfer length. S2MM: Available memory
    EndOfFrame,    // Indicates end of packet
    StartOfFrame,  // Indicated start of packet
}

impl Mm2sDescriptorControlField {
    pub(crate) fn bitmask(&self) -> u32 {
        match *self {
            Mm2sDescriptorControlField::_BufferLength => 2_u32.pow(25) - 1,
            Mm2sDescriptorControlField::EndOfFrame => 1 << 30,
            Mm2sDescriptorControlField::StartOfFrame => 1 << 31,
        }
    }
}

enum _S2mmDescriptorControlField {
    BufferLength, // MM2S: Transfer length. S2MM: Available memory
}

impl _S2mmDescriptorControlField {
    fn _bitmask(&self) -> u32 {
        match *self {
            _S2mmDescriptorControlField::BufferLength => 2_u32.pow(25) - 1,
        }
    }
}

pub(crate) enum Mm2sDescriptorStatusField {
    TransferredBytes, // The number of bytes transferred by this descriptor
    InternalError,    // DMA internal error
    SlaveError,       // DMA slave error
    DecodeError,      // DMA decode error
    Complete,         // DMA engine has completed transfer described by this descriptor
}

impl Mm2sDescriptorStatusField {
    pub(crate) fn bitmask(&self) -> u32 {
        match *self {
            Mm2sDescriptorStatusField::TransferredBytes => 2_u32.pow(25) - 1,
            Mm2sDescriptorStatusField::InternalError => 1 << 28,
            Mm2sDescriptorStatusField::SlaveError => 1 << 29,
            Mm2sDescriptorStatusField::DecodeError => 1 << 30,
            Mm2sDescriptorStatusField::Complete => 1 << 31,
        }
    }
}

pub(crate) enum S2mmDescriptorStatusField {
    TransferredBytes, // The number of bytes transferred by this descriptor
    _EndOfFrame,      // Indicates end of packet
    _StartOfFrame,    // Indicated start of packet
    InternalError,    // DMA internal error
    SlaveError,       // DMA slave error
    DecodeError,      // DMA decode error
    Complete,         // DMA engine has completed transfer described by this descriptor
}

impl S2mmDescriptorStatusField {
    pub(crate) fn bitmask(&self) -> u32 {
        match *self {
            S2mmDescriptorStatusField::TransferredBytes => 2_u32.pow(25) - 1,
            S2mmDescriptorStatusField::_EndOfFrame => 1 << 26,
            S2mmDescriptorStatusField::_StartOfFrame => 1 << 27,
            S2mmDescriptorStatusField::InternalError => 1 << 28,
            S2mmDescriptorStatusField::SlaveError => 1 << 29,
            S2mmDescriptorStatusField::DecodeError => 1 << 30,
            S2mmDescriptorStatusField::Complete => 1 << 31,
        }
    }
}
