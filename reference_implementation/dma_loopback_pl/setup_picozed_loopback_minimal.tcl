set list_projs [get_projects -quiet]
if { $list_projs eq "" } {
   create_project picozed_loopback_minimal picozed_loopback_minimal -part xc7z030sbg485-1
}

source picozed_loopback_minimal.tcl
make_wrapper -files [get_files picozed_loopback_minimal/picozed_loopback_minimal.srcs/sources_1/bd/picozed_loopback/picozed_loopback.bd] -top
add_files -norecurse picozed_loopback_minimal/picozed_loopback_minimal.srcs/sources_1/bd/picozed_loopback/hdl/picozed_loopback_wrapper.v
set_property STEPS.WRITE_BITSTREAM.ARGS.BIN_FILE true [get_runs impl_1]
