/dts-v1/;
/include/ "zynq-7000.dtsi"
/ {
	model = "Red Pitaya Board";
	compatible = "xlnx,zynq-red-pitaya", "xlnx,zynq-7000";

	aliases {
		ethernet0 = &gem0;
		serial0 = &uart0;
	};

	memory {
		device_type = "memory";
		reg = <0x0 0x20000000>;
	};

	chosen {
		bootargs = "earlyprintk";
		stdout-path = "serial0:115200n8";
	};

	usb_phy0: phy0 {
		compatible = "usb-nop-xceiv";
		#phy-cells = <0>;
	};

	amba {
        clattering_registers: clattering_registers@43C00000 {
            compatible = "smartacoustics,axi-control";
            reg = < 0x43C00000 0x1000 >;
        };
        //hardware_control_registers: hardware_control_registers@43C20000 {
        //    compatible = "smartacoustics,axi-control";
        //    /* If this address changes you will need to modify the nboard and
        //    nboard_status addresses in the u-boot uEnv.txt */
        //    reg = < 0x43C20000 0x1000 >;
        //};
        axi_dma_interleaved_data: axi_dma_interleaved_data@43C14000 {
            compatible = "smartacoustics,axi-irq-mem";
            reg = < 0x43C14000 0x1000 >;
            interrupt-parent = <&intc>;
		    interrupt-names = "interleaved_data_interrupt";
            interrupts = <0 29 1>;
            dma-data-mem-size = <0x02000000>; /* Big Endian */
    	};
        axi_dma_tgc: axi_dma_tgc@43C11000 {
            compatible = "smartacoustics,axi-irq-mem";
            reg = < 0x43C11000 0x1000 >;
            interrupt-parent = <&intc>;
		    interrupt-names = "tgc_interrupt";
            interrupts = <0 30 1>;
            dma-data-mem-size = <0x80000>; /* Big Endian */
    	};
        axi_dma_downmix: axi_dma_downmix@43C12000 {
            compatible = "smartacoustics,axi-irq-mem";
            reg = < 0x43C12000 0x1000 >;
            interrupt-parent = <&intc>;
		    interrupt-names = "downmix_interrupt";
            interrupts = <0 31 1>;
            dma-data-mem-size = <0x80000>; /* Big Endian */
    	};
        axi_dma_waveform_data: axi_dma_waveform_data@43C10000 {
            compatible = "smartacoustics,axi-irq-mem";
            reg = < 0x43C10000 0x1000 >;
            interrupt-parent = <&intc>;
		    interrupt-names = "waveform_data_interrupt";
            interrupts = <0 32 1>;
            dma-data-mem-size = <0x00200000>; /* Big Endian */
            dma-descriptors-mem-size = <0x40000>; /* Big Endian */
            /* Reserving up to 32Mb <0x02000000> for the dma-data-mem-size
            seems to work OK, but increasing to <0x04000000> (64Mb) gives
            memory errors. Could be linked somehow to 128MB that is reserved
            as the kernel boots - perhaps we're running out, but not confirmed
            */
    	};
        axi_dma_operation_dispatcher: axi_dma_operation_dispatcher@43C13000 {
            compatible = "smartacoustics,axi-irq-mem";
            reg = < 0x43C13000 0x1000 >;
            interrupt-parent = <&intc>;
		    interrupt-names = "operation_dispatcher_interrupt";
            interrupts = <0 33 1>;
            dma-data-mem-size = <0x80000>; /* Big Endian */
    	};

        clattering_ps_sync: clattering_ps_sync {
            compatible = "smartacoustics,simple-irq";
            reg = < 0x43C00000 0x0 >;
            interrupt-parent = <&intc>;
            interrupt-names = "ps_sync_interrupt";
            interrupts = <0 34 1>;
        };

        clattering_power_down: clattering_power_down {
            compatible = "smartacoustics,simple-irq";
            reg = < 0x43C00000 0x0 >;
            interrupt-parent = <&intc>;
            interrupt-names = "power_down_interrupt";
            interrupts = <0 35 1>;
        };

        clattering_external_trigger_received: clattering_external_trigger_received {
            compatible = "smartacoustics,simple-irq";
            reg = < 0x43C00000 0x0 >;
            interrupt-parent = <&intc>;
            interrupt-names = "external_trigger_received";
            interrupts = <0 36 1>;
        };

        clattering_probe_info: clattering_probe_info {
            compatible = "smartacoustics,simple-irq";
            reg = < 0x43C00000 0x0 >;
            interrupt-parent = <&intc>;
            interrupt-names = "probe_info";
            interrupts = <0 52 1>;
        };
	};
};

&gem0 {
	status = "okay";
	phy-mode = "rgmii-id";
	phy-handle = <&ethernet_phy0>;

	ethernet_phy0: ethernet-phy@0 {
		reg = <1>;
	};
};

&sdhci0 {
	status = "okay";
};

&uart0 {
	status = "okay";
};

&usb0 {
	status = "okay";
	dr_mode = "host";
	usb-phy = <&usb_phy0>;
};

&spi0 {
    status = "okay";
    num-cs = <1>;

    flash_chip: flash_chip@0 {
        compatible = "smartacoustics,spigeneric";
        reg = <0>;  //Chip select 0
        spi-max-frequency = <10000000>;
    };
};

&spi1 {
    status = "okay";
    num-cs = <1>;

    switches: switches@0 {
        compatible = "smartacoustics,spigeneric";
        reg = <0>;  //Chip select 0
        spi-max-frequency = <10000000>;
    };
};

&qspi {
	status = "okay";
	is-dual = <0>;
	num-cs = <1>;
	flash@0 {
		compatible = "n25q128a11";
		reg = <0x0>;
		spi-tx-bus-width = <1>;
		spi-rx-bus-width = <4>;
		spi-max-frequency = <50000000>;
		#address-cells = <1>;
		#size-cells = <1>;
		partition@qspi-fsbl-uboot {
			label = "qspi-fsbl-uboot";
			reg = <0x0 0x100000>;
		};
		partition@qspi-linux {
			label = "qspi-linux";
			reg = <0x100000 0x500000>;
		};
		partition@qspi-device-tree {
			label = "qspi-device-tree";
			reg = <0x600000 0x20000>;
		};
		partition@qspi-rootfs {
			label = "qspi-rootfs";
			reg = <0x620000 0x5E0000>;
		};
		partition@qspi-bitstream {
			label = "qspi-bitstream";
			reg = <0xC00000 0x400000>;
		};
	};
};
