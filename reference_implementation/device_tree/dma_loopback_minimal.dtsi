// A reduced example device tree with suitable libgannet compatible devices
// created.

/ {
    // We want to restrict the space of memory the DMA system can write to.
    // We do this by setting up a block of reserved memory that the CMA
    // uses as its memory pool.
    // This address pool should be the pool that is set up in Vivado as the
    // available range it can access. The result is that there is some
    // protection against the DMA engine being able to overwrite arbitrary
    // blocks of memory.
    // There is still a risk that other drivers might use the CMA and there
    // could be some conflict at that point (by a malicious or erroneous
    // coder), but the major risk of a userspace program stepping on running
    // kernel memory should be gone.
    // This reserved-memory block is not necessary for the device driver,
    // merely a helpful tool to restrict the DMA memory space.
    reserved-memory {
		#address-cells = <1>;
		#size-cells = <1>;
		ranges;
		/* global autoconfigured region for contiguous allocations */
		linux,cma@10000000 {
			compatible = "shared-dma-pool";
			reusable;
            reg = <0x10000000 0x8000000>; // Set 128M region at 256M boundary
			linux,cma-default;
		};
	};


    // There are 2 devices:
    // * one MM2S direct DMA enginee
    // * one S2MM direct DMA engine.
    // In the associated PL block diagram, there is a FIFO between the
    // the two direct DMA engines. Non-SPI level-high interrupts
    // have the interrupt property <0 N 4> where N is computed as (PL
    // interrupt number - 32).
    // Interrupt numbers can be in the ranges:
    //     61 - 68 (incl)
    //     84 - 91 (incl)
	amba {
        axi_dma_loop_direct_mm2s: axi_dma_loop_direct_mm2s@40402000 {
            compatible = "smartacoustics,axi-irq-mem";
            reg = < 0x40402000 0x1000 >;
            interrupt-parent = <&intc>;
		    interrupt-names = "axi_dma_mm2s";
            interrupts = <0 29 4>; // PL interrupt 63
            dma-data-mem-size = <0x1000000>; /* Big Endian */
    	};
        axi_dma_loop_direct_s2mm: axi_dma_loop_direct_s2mm@40403000 {
            compatible = "smartacoustics,axi-irq-mem";
            reg = < 0x40403000 0x1000 >;
            interrupt-parent = <&intc>;
		    interrupt-names = "axi_dma_s2mm";
            interrupts = <0 30 4>; // PL interrupt 64
            dma-data-mem-size = <0x1000000>; /* Big Endian */
    	};
	};
};

